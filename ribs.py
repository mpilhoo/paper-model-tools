import bmesh
import bpy

from .helpers import (
    ensure_lookup,
    VERTS, EDGES, FACES, ALL_MESH
)


def reduce_former(bm, thickness):
    t = {}

    ensure_lookup(bm, FACES)

    for v in bm.verts:
        le = []
        for e in v.link_edges:
            if e.is_boundary:
                le.append(e)
        if len(le) != 2:
            continue
        # translation vector
        if abs(math.pi - (le[0].other_vert(v).co - v.co).angle(le[1].other_vert(v).co - v.co)) > 0.01:
            tv = (
                (le[0].other_vert(v).co - v.co).normalized() +
                (le[1].other_vert(v).co - v.co).normalized()
            ).normalized()
            angle = 0.
            for l in v.link_loops:
                if l.is_convex:
                    angle += l.calc_angle()
                else:
                    angle += 2. * math.pi - l.calc_angle()
            angle /= 2.
            if angle > math.pi/2.:
                angle = -angle
            t[v.index] = (tv / math.sin(angle)) * thickness
        else:
            l = [l for l in v.link_loops if l.edge in le][0]
            tv = (l.link_loop_prev.vert.co-v.co).normalized().cross(l.face.normal)
            t[v.index] = tv * thickness

    for v in bm.verts:
        if v.index in t:
            v.co += t[v.index]


class ReduceFormerOperator(bpy.types.Operator):
    bl_idname = "object.reduce_former"
    bl_label = "Reduce former"
    bl_options = {'REGISTER', 'UNDO'}

    thickness = bpy.props.FloatProperty(
        name="Thickness (mm)",
        description="thickness of covering paper",
        default=0.25,
        min=0.05, max=0.5,
        subtype='FACTOR'
    )

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def execute(self, context):
        for obj in context.selected_objects:
            bm = bmesh.new()
            bm.from_mesh(obj.data)
            bm.transform(obj.matrix_world)
            bm.normal_update()

            reduce_former(bm, 0.001*self.thickness*context.scene.paper_model.scale)

            bm.transform(obj.matrix_world.inverted())
            bm.to_mesh(obj.data)
            bm.free()

        return {'FINISHED'}


class CreateRibsOperator(bpy.types.Operator):
    """ Creates ribs from intersections of mesh and planes """
    bl_idname = "object.create_ribs_operator"
    bl_label = "Create Ribs"
    bl_options = {'REGISTER', 'UNDO'}

    threshold = bpy.props.FloatProperty(name="Threshold",
                                        description="Minimal distance to another vertex to create new one",
                                        default=0.05, soft_min=0., subtype='FACTOR')

    reduce_on = bpy.props.BoolProperty(name="reduce",
                                       description="When option is selected created ribs will be reduced by thickness of covering paper",
                                       default=True)

    thickness = bpy.props.FloatProperty(name="Thickness",
                                        description="thickness of covering paper",
                                        default=0.25,
                                        min=0.05, max=0.5,
                                        subtype='FACTOR')

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT' and len(context.selected_objects) > 1

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "threshold", text="Threshold (mm)")
        layout.prop(self.properties, "reduce", text="Reduce")
        layout.prop(self.properties, "thickness", text="Thickness (mm)")

    def execute(self, context):
        bm = bmesh.new()
        bm.from_mesh(context.active_object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        planes = []
        for plane in context.selected_objects:
            if plane.name == context.active_object.name:
                continue
            pl_bm = bmesh.new()
            pl_bm.from_mesh(plane.data)
            pl_bm.transform(plane.matrix_world)
            pl_bm.normal_update()
            ensure_lookup(pl_bm, FACES)
            planes.append((pl_bm.faces[0].calc_center_bounds(), Vector(pl_bm.faces[0].normal)))
            pl_bm.free()

        make_ribs(bm, planes, threshold=scaled_length(self.threshold))

        if self.reduce_on:
            reduce_former(bm, scaled_length(self.thickness))

        me = bpy.data.meshes.new(context.active_object.name+"ribs datablock")
        ribs = bpy.data.objects.new(name="Ribs of "+context.active_object.name, object_data=me)
        bpy.context.scene.objects.link(ribs)
        bm.to_mesh(ribs.data)
        bm.free()
        return {'FINISHED'}


class IntersectRibsWithStrut(bpy.types.Operator):
    """ Creates cuts on intersections of ribs and strut """
    bl_idname = "object.intersect_strut_ribs"
    bl_label = "Intersect Strut with Ribs"
    bl_options = {'REGISTER', 'UNDO'}

    thickness = bpy.props.FloatProperty(name="Thickness (mm)",
                                        description="Thickness of cardboard (width of cuts in ribs)",
                                        default=1.15, soft_min=0.5, soft_max=3., subtype='FACTOR')

    reverse = bpy.props.BoolProperty(name="Reversed", description="Reverse direction of cutting", default=False)

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT' and len(context.selected_objects) == 2

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "thickness", text="Thickness (mm)")
        layout.prop(self.properties, "reverse", text="Reverse cuts")

    def execute(self, context):
        ribs = []
        zero_is_active = context.selected_objects[0].name != context.active_object.name
        ribs_obj = context.selected_objects[0 if zero_is_active else 1]
        ribs_mesh = bmesh.new()
        ribs_mesh.from_mesh(ribs_obj.data)
        ribs_mesh.transform(ribs_obj.matrix_world)
        ribs_mesh.normal_update()
        rib_face = ribs_mesh.faces[0]
        ribs.append((rib_face.calc_center_bounds(), copy(rib_face.normal)))
        ribs_mesh.free()

        strut = bmesh.new()
        strut.from_mesh(context.active_object.data)
        strut.transform(context.active_object.matrix_world)
        strut.normal_update()
        ensure_lookup(strut, FACES)
        strut_face = strut.faces[0]
        strut_co = strut_face.calc_center_bounds()
        strut_no = copy(strut_face.normal)

        strut_cuts(strut, ribs,
                   cut_direciton=-1. if self.reverse else 1.,
                   thickness=scaled_length(self.thickness, 'mm'))
        strut.transform(context.active_object.matrix_world.inverted())
        strut.to_mesh(context.active_object.data)
        strut.free()

        ribs_mesh = bmesh.new()
        ribs_mesh.from_mesh(ribs_obj.data)
        ribs_mesh.transform(ribs_obj.matrix_world)
        ribs_mesh.normal_update()
        strut_cuts(ribs_mesh, [(strut_co, strut_no)],
                   cut_direciton=1. if self.reverse else -1.,
                   thickness=scaled_length(self.thickness, 'mm'))
        ribs_mesh.transform(ribs_obj.matrix_world.inverted())
        ribs_mesh.to_mesh(ribs_obj.data)
        ribs_mesh.free()

        return {'FINISHED'}
