import bmesh
import bpy
from .common.selection import (
    selected_edges,
    selected_verts,
    # find_selected_cycle_and_path,
)
from .common.helpers import (
    ensure_lookup,
    VERTS, EDGES, FACES, ALL_MESH,
    get_separated_object,
)
from .common.algorithm import (
    find_shortest_paths,
)
from .common.values import (
    scaled_length,
)
from .common.uv import (
    clean_uv_map,
    fit_uv,
)
from .common.baking import bake_textures
from .common.algorithm import select_from_face_to_border
from .ribbons_old import generate_ribbon_geometry

class RadialCutsOperator(bpy.types.Operator):
    """
    Cuts mesh by rotating mid-planes and simplify parts between cuts.
    """
    bl_idname = "edit_mesh.radial_cuts"
    bl_label = "Radial cuts"
    bl_options = {'REGISTER', 'UNDO'}

    cut_count = bpy.props.IntProperty(
        name="Number of Cuts",
        default=6
    )

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "cut_count", text="Number of cuts")

    @classmethod
    def poll(cls, context):
        return context.mode == 'EDIT_MESH'

    def execute(self, context):
        threshold = scaled_length(.3, 'mm')
        uv_index = len(context.object.data.uv_textures)
        bpy.ops.mesh.uv_texture_add()
        uv_name = context.object.data.uv_textures[uv_index].name

        bm = bmesh.from_edit_mesh(context.object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        uv_layer = bm.loops.layers.uv[uv_name]
        clean_uv_map(bm, uv_layer)

        reset_tags(bm)

        basis, cut, focus, fork = find_selected_cycle_and_path(bm)
        source_geom = select_from_face_to_border(basis, next(f for f in focus.link_faces))
        basis_length = sum(e.calc_length() for e in basis)
        part_width = basis_length / self.cut_count

        next_cut_length = part_width
        traveled_length = 0.
        cuts = [cut]
        v = fork

        for ce in cut:
            ce.seam = True

        def new_cut(v):
            cuts.append(bmesh.ops.connect_vert_pair(bm, verts=[v, focus])['edges'])
            for ce in cuts[-1]:
                if v in ce.verts:
                    continue
                ce.seam = True

        basis_verts = set()
        for e in basis:
            for vv in e.verts:
                basis_verts.add(vv)
            v = e.other_vert(v)
            traveled_length += e.calc_length()
            if abs(traveled_length - next_cut_length) <= threshold:
                if v is fork:
                    break
                new_cut(v)
                next_cut_length += part_width
            elif traveled_length > next_cut_length:
                _, nv = bmesh.utils.edge_split(e, v, (traveled_length - next_cut_length) / e.calc_length())
                new_cut(nv)
                next_cut_length += part_width

        parts_geom = [None] * self.cut_count
        target_geom = { 'faces': [], 'edges': [], 'verts': [] }
        dupl_maps = [None] * self.cut_count
        parts_connection_edges = []
        for i in range(self.cut_count):
            cut1 = [e for e in cuts[i-1] if all(v is not focus for v in e.verts)]
            cut2 = cuts[i]
            parts_geom[i], dupl_maps[i] = generate_ribbon_geometry(bm, set(cut1), set(cut2))
            for k, v in target_geom.items():
                v.extend(parts_geom[i][k])

            if i > 0:
                for cut in (cut1, cut2):
                    import pdb; pdb.set_trace()
                    parts_connection_edges.append(
                        next(
                            dupl_maps[i]['edges'][e]
                                for e in cut 
                                if any(v in basis_verts for v in e.verts)
                        )
                    )


        doubled_verts = []
        for e in parts_connection_edges:
            for v in e.verts:
                doubled_verts.append(v)
        print([v.index for v in doubled_verts])

        bmesh.ops.remove_doubles(bm, verts=doubled_verts, dist=0.0001)
        for elems in target_geom.values():
            elems[:] = [el for el in elems if el.is_valid]
        #dupl_map = {k: {} for k in dupl_maps[0].keys()}
        #for dm in dupl_maps:
            #for elems_key, elems in dm.items():
                #for el, mapped_el in elems.items():
                    #if el.is_valid and mapped_el.is_valid:
                        #dupl_map[elems_key][el] = mapped_el

        for f in target_geom['faces']:
            f.select_set(True)
        bpy.ops.uv.unwrap()
        return {'FINISHED'}
        for f in target_geom['faces']:
            f.select_set(False)

        # FIXME removed duplicates crashes mapping structure
        for dupl_map in dupl_maps:
            fit_uv(source_geom, target_geom, dupl_map['verts'], uv_layer)

        bm.transform(context.active_object.matrix_world.inverted())
        bmesh.update_edit_mesh(context.active_object.data)

        target = get_separated_object() # TODO NameError: name 'get_separated_object' is not defined
        bake_textures(bm, target, context, suffix='_radialcuts')

        context.object.select = False
        ribbon.select = True
        bpy.ops.object.shade_smooth()


        return {'FINISHED'}
