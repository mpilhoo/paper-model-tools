import bpy
import bmesh
from mathutils import Vector

from .source_mesh import SourceMesh
from ..common.submeshes import MeshPart

from .target_mesh import TargetMesh


class RibbonateMeshProcessor(object):
    def __init__(self,
                 context: bpy.types.Context,
                 bm: bmesh.types.BMesh,
                 selection: MeshPart,
                 uv_layer_name: str = None,
                 closed: bool = None):
        self._uv_layer = None
        self._frames = None
        self._bounds = (set(), set())
        self._source_mesh = None
        self._target_mesh = None
        self.context = context
        self.bm = bm
        self.uv_layer_name = uv_layer_name or context.object.data.uv_textures.active.name
        self.closed = closed
        self._selection = selection
        self._seam = set()
        self._reset_uvs_on_layer()

    def _reset_uvs_on_layer(self):
        for f in self.bm.faces:
            for l in f.loops:
                l[self.uv_layer].uv = Vector((-1, -1))

    @property
    def uv_layer(self):
        return self.bm.loops.layers.uv[self.uv_layer_name]

    @property
    def selection(self):
        if self._selection is None:
            self._selection = MeshPart.from_selection(self.bm)
            self._selection.select_set(False)
        return self._selection

    @property
    def source_mesh(self):
        if not self._source_mesh:
            self._source_mesh = SourceMesh.build_from_selection(self.bm, self.closed, self.selection)
        return self._source_mesh

    @property
    def target_mesh(self):
        if not self._target_mesh:
            self._target_mesh = TargetMesh.generate_base(self.bm, self.source_mesh, self.uv_layer)
        return self._target_mesh

    def process(self):
        # self.mark_seams()     # FIXME delete?
        self.unwrap_form()
        self.adjust_geometry_to_form()
        if not self.source_mesh.closed:
            self.target_mesh.form_side_paths(self.source_mesh.side_paths)
        # TODO resize uv before baking
        # def resize_uv(self):
        #     max_uv = 0.
        #     for v in bm.verts:
        #         l = next(l for l in v.link_loops)
        #         uv = l[uv_layer].uv
        #         max_uv = max(max_uv, *uv)
        #
        #     for v in bm.verts:
        #         for l in v.link_loops:
        #             l[uv_layer].uv /= max_uv
        # TODO BAKE TEXTURE

    def clean(self):
        self.closed = None
        self._selection = None
        self._source_mesh = None
        self._target_mesh = None
        self._uv_layer = None

    def unwrap_form(self):
        self.context.object.data.uv_textures.active = self.context.object.data.uv_textures[self.uv_layer_name]
        self.target_mesh.select_set(True)
        bpy.ops.uv.unwrap()
        self.target_mesh.select_set(False)

        if self.source_mesh.closed:
            v1, v2 = tuple(self.target_mesh.map[v] for v in self.source_mesh.bridge.ends)
            bridge_edge = next(e for e in v1.link_edges if e.other_vert(v1) is v2)
            self.target_mesh.bridge_edge = bridge_edge
            for l in bridge_edge.link_loops:
                if l.vert is v1:
                    self.source_mesh.bridge.left_uvs = l[self.uv_layer].uv, l.link_loop_next[self.uv_layer].uv
                else:
                    self.source_mesh.bridge.right_uvs = l.link_loop_next[self.uv_layer].uv, l[self.uv_layer].uv

    def _set_pinned_uv(self, l, uv):
        l[self.uv_layer].uv = uv
        l[self.uv_layer].pin_uv = True

    @staticmethod
    def _calc_line_uv(uvs, dist, path):
        return uvs[1] + (uvs[0] - uvs[1]) * (dist / path.distance)

    def _adjust_bridge_uvs_to_form(self):
        start, end = self.bridge.ends
        next_loop_anticlockwise = lambda l: l.link_loop_prev.link_loop_radial_next

        l = next(l for l in start.link_loops if (l.edge in self.frames[0] and l.face in self.source_mesh))
        uv = self.bridge.right_uvs[0]
        while l.face in self.source_mesh:
            self._set_pinned_uv(l, uv)
            l = next_loop_anticlockwise(l)
            if l.edge in self.bridge.iter_edges():
                uv = self.bridge.left_uvs[0]
            if l.edge in self.frames[0]:
                break

        l = next(l for l in end.link_loops if (l.edge in self.frames[1] and l.face in self.source_mesh))
        uv = self.bridge.left_uvs[1]
        while l.face in self.source_mesh:
            self._set_pinned_uv(l, uv)
            l = next_loop_anticlockwise(l)
            if l.edge in self.bridge.iter_edges():
                uv = self.bridge.right_uvs[1]
            if l.edge in self.frames[0]:
                break

        next_e = self.bridge.history[-1].edge
        dist = next_e.calc_length()

        bridge = self.bridge.history[-2::-1]
        for s in bridge:
            v = s.vert
            prev_e = s.edge
            l = next(l for l in v.link_loops if l.edge is prev_e)
            uv = self._calc_line_uv(self.bridge.right_uvs, dist, self.bridge)
            while not l[self.uv_layer].pin_uv:
                self._set_pinned_uv(l, uv)
                l = next_loop_anticlockwise(l)
                if l.edge is next_e:
                    uv = self._calc_line_uv(self.bridge.left_uvs, dist, self.bridge)
            dist += prev_e.calc_length()
            next_e = prev_e

    def _adjust_side_paths_to_form(self):
        for path in self.source_mesh.side_paths:
            if path.is_cut:
                dist = 0.
                uvs = tuple(self.ribbon_geometry.map[v].link_loops[0][self.uv_layer].uv for v in path.ends[::-1])
                for s in path:
                    v = s.vert
                    dist += s.edge.calc_length()
                    uv = self._calc_line_uv(uvs, dist, path)
                    for l in v.link_loops:
                        self._set_pinned_uv(l, uv)
            else:
                path_link_faces = MeshPart(faces=sum((list(s.vert.link_faces) for s in path.history[:-1]), []))
                path_link_faces.select_set(True)
                bpy.ops.uv.unwrap()
                for s in path:
                    for l in s.vert.link_loops[:-1]:
                        l[self.uv_layer].pin_uv = True

    def adjust_geometry_to_form(self):
        if self.closed:
            self._adjust_bridge_uvs_to_form()

        for v in self.target_mesh.verts:
            uv = v.link_loops[0][self.uv_layer].uv
            for l in self.target_mesh.map[v].link_loops:
                if l[self.uv_layer].pin_uv:
                    continue
                if l.face in self.source_mesh:
                    self._set_pinned_uv(l, uv)

        if self.source_mesh.closed is False:
            self._adjust_side_paths_to_form()

        self.source_mesh.select_set(True)
        bpy.ops.uv.unwrap()
        # self.source_mesh.select_set(False)
