from itertools import chain

import bmesh
from bmesh.ops import remove_doubles
from bmesh.types import BMFace, BMEdge, BMVert

from ..common.algorithm import find_shortest_paths
from ..common.helpers import ensure_lookup, VERTS, EDGES, FACES
from ..common.submeshes import MeshPart
from .side_former import SideFormer


class TargetMesh(MeshPart):
    @classmethod
    def generate_base(cls, bm, source_mesh, uv_layer):
        predef_conn = [path.ends for path in source_mesh.side_paths]

        frame1, frame2 = source_mesh.frames

        dupl_dict = bmesh.ops.duplicate(bm, geom=list(frame1 | frame2))
        geometry = TargetMesh(
            mixed=dupl_dict['geom'],
            map=dupl_dict['vert_map']
        )
        geometry.map.update(dupl_dict['edge_map'])
        ensure_lookup(bm, VERTS | EDGES)

        for conn in predef_conn:
            new_edge = bm.edges.new([geometry.map[v] for v in conn])
            geometry.edges.add(new_edge)
        ensure_lookup(bm, EDGES)

        def on_finish(branch):
            try:
                new_edge = bm.edges.new([geometry.map[branch.current], geometry.map[branch.start]])
                ensure_lookup(bm, EDGES)
                geometry.edges.add(new_edge)
            except Exception as e:
                pass

        find_shortest_paths(frame1.verts, frame2.verts, on_finish=on_finish)
        geometry.faces.update(bmesh.ops.edgenet_fill(bm, edges=list(geometry.edges))['faces'])
        ensure_lookup(bm, EDGES | FACES)

        is_frame = lambda f: all(geometry.map.get(e) in frame1 for e in f.edges) or \
                             all(geometry.map.get(e) in frame2 for e in f.edges)

        frame_faces = [f for f in geometry.faces if is_frame(f)]
        for f in frame_faces:
            geometry.faces.remove(f)
            bm.faces.remove(f)

        geometry.source = source_mesh
        geometry.bm = bm
        geometry.uv_layer = uv_layer
        geometry._triangulate_geometry()
        geometry._adjust_normals()

        return geometry

    def remove(self, obj):
        if isinstance(obj, BMFace):
            self.faces.remove(obj)
            self.bm.faces.remove(obj)
        elif isinstance(obj, BMEdge):
            self.edges.remove(obj)
            self.bm.edges.remove(obj)
        elif isinstance(obj, BMVert):
            self.verts.remove(obj)
            self.bm.verts.remove(obj)

    def update_with_new_mesh_elements(self):
        for collection in (self.bm.verts, self.bm.edges, self.bm.faces):
            objs = tuple(o for o in collection)
            for o in objs[::-1]:
                if o.index == -1:
                    self.add(o)
                else:
                    break

    def remove_doubles(self):
        remove_doubles(self.bm, verts=list(self.verts), dist=0.0001)

    def _adjust_normals(self):
        se = next(e for e in self.map.keys() if (isinstance(e, bmesh.types.BMEdge) and e in self.source))
        te = self.map[se]
        tl = te.link_loops[0]
        try:
            sl = next(l for l in se.link_loops if self.map[l.vert] is tl.vert)
        except StopIteration:
            sl = None
            pass

        if not sl or sl.face.normal * tl.face.normal < 0.:
            for f in self.faces:
                f.normal_flip()

    def _triangulate_geometry(self):
        def dist(v1, v2):
            if not v1 or not v2:
                return float('Inf')
            return (v1.co-v2.co).length

        to_conn = []

        class NoneVert:
            def index(self):
                return -1

            def __bool__(self):
                return False

        class LoopCut:
            def __init__(self, loop):
                self.nloop = loop.link_loop_next
                self.ploop = loop
                self.pair = None

            def pair_cuts(self, other):
                self.pair = other
                other.pair = self

            def verts1(self):
                return (self.nloop.vert,
                        self.ploop.link_loop_prev.vert if self.ploop != self.pair.nloop else NoneVert())

            def verts2(self):
                return (self.nloop.link_loop_next.vert if self.nloop != self.pair.ploop else NoneVert(),
                        self.ploop.vert)

            def cut_lenght(self):
                return min(dist(*self.verts1()), dist(*self.verts2()))

            def make_cut(self):
                if dist(*self.verts1()) < dist(*self.verts2()):
                    to_conn.append(self.verts1())
                    self.ploop = self.ploop.link_loop_prev
                else:
                    to_conn.append(self.verts2())
                    self.nloop = self.nloop.link_loop_next

            def collides(self):
                return (self.nloop == self.pair.ploop and self.ploop == self.pair.nloop.link_loop_next) or \
                    (self.ploop == self.pair.nloop and self.nloop == self.pair.ploop.link_loop_prev)

        set1 = set(self.map[v] for v in self.source.frames[0].verts)
        set2 = set(self.map[v] for v in self.source.frames[1].verts)

        def connects_frames(edge):
            return any((
                edge.verts[0] in set1 and edge.verts[1] in set2,
                edge.verts[0] in set2 and edge.verts[1] in set1,
            ))

        for face in self.faces:
            cut1 = cut2 = None
            for loop in face.loops:
                if connects_frames(loop.edge):
                    if not cut1:
                        cut1 = LoopCut(loop)
                    else:
                        cut2 = LoopCut(loop)
            if not cut1 or not cut2:
                continue
            cut1.pair_cuts(cut2)
            while not cut1.collides():
                if cut1.cut_lenght() < cut2.cut_lenght():
                    cut1.make_cut()
                else:
                    cut2.make_cut()

        for v1, v2 in to_conn:
            new_edge = bmesh.ops.connect_vert_pair(self.bm, verts=[v1, v2])['edges'][0]
            self.edges.add(new_edge)
            ensure_lookup(self.bm, FACES)
            self.faces.update(f for f in new_edge.link_faces if f not in self)

    def form_side_paths(self, side_paths):
        for side_path in side_paths:
            former = SideFormer(self, side_path)
            former.form_side_path()
