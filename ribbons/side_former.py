import bmesh
from bmesh.utils import face_split
from mathutils.geometry import intersect_line_line_2d

from ..common.helpers import ensure_lookup, ALL_MESH
from ..common.submeshes import Path
from ..common.uv import calculate_co_by_uv_base, get_vert_uv


class DividableEdge:
    def __init__(self,
                 bm_edge: bmesh.types.BMEdge,
                 uv_layer: bmesh.types.BMLayerItem):
        self.edges = [bm_edge]
        self.uv_layer = uv_layer
        self.ends_uvs = self._edge_uvs(bm_edge)

        diffs = tuple(abs(self.ends_uvs[0][i] - self.ends_uvs[1][i]) for i in range(2))
        self.longer_coord = 0 if diffs[0] > diffs[1] else 1

    def _edge_uvs(self, edge):
        return tuple(self._get_vert_uv(v) for v in edge.verts)

    def _get_vert_uv(self, vert):
        return next(l for l in vert.link_loops)[self.uv_layer].uv

    @staticmethod
    def _between_uv(a, ends):
        if ends[0] < ends[1]:
            return ends[0] <= a <= ends[1]
        else:
            return ends[0] >= a >= ends[1]

    def _longer_coords(self, edge):
        return tuple(self._get_vert_uv(v)[self.longer_coord] for v in edge.verts)

    def try_cross(self, step):
        step_uvs = self._edge_uvs(step.edge)
        cross = intersect_line_line_2d(*step_uvs, *self.ends_uvs)

        if cross:
            cross_co = cross[self.longer_coord]
            for i, edge in enumerate(self.edges):
                own_uvs = self._edge_uvs(edge)
                if self._between_uv(cross_co, self._longer_coords(edge)):
                    own_length = (own_uvs[1] - own_uvs[0]).length
                    fac = (cross - own_uvs[0]).length / (own_uvs[1] - own_uvs[0]).length
                    if fac * own_length > 0.01:
                        new_edge, new_vert = bmesh.utils.edge_split(edge, edge.verts[0], fac)
                        self.edges.append(new_edge)
                        return new_vert

        return None


class AbstractEdge:
    @staticmethod
    def try_cross(*args):
        return None


class RibbonFace:
    next_edge = None


class ExtraFace(RibbonFace):
    def __init__(self, edge: DividableEdge):
        self.face = edge.edges[0].link_faces[0]
        self.next_edge = edge
        self.prev_edge = AbstractEdge()
        self.next_face = ProjectionFace(self.next_edge.edges[0].link_faces[0], self)

    def iter_neighbors(self):
        yield 'next', self.next_edge, self.next_face


class ProjectionFace(RibbonFace):
    def __init__(self,
                 face: bmesh.types.BMFace,
                 prev: RibbonFace):
        self.face = face
        self.prev_edge = prev.next_edge

        next_bm_edge = next(e for e in face.edges if any(not(f is face or f is prev.face) for f in e.link_faces))
        self.next_edge = DividableEdge(next_bm_edge, self.prev_edge.uv_layer)

        self.prev_face = prev
        self._next_face = None

    def iter_neighbors(self):
        yield 'next', self.next_edge, self.next_face
        yield 'prev', self.prev_edge, self.prev_face

    @property
    def next_face(self):
        if not self._next_face:
            nxt_bmface = next(f for f in self.next_edge.edges[0].link_faces if f is not self.face)
            self._next_face = ProjectionFace(nxt_bmface, self)
        return self._next_face


class Split:
    def __init__(self, start, uv_layer, face):
        self.start = start
        self.finish = None
        self.points = []
        self.face = face
        self.uv_layer = uv_layer
        self.base, self.base_uv = self.find_base()

    def _get_vert_uv(self, v):
        return next(l for l in v.link_loops if l.face is self.face)[self.uv_layer].uv

    def find_base(self):
        e1 = self.face.edges[0]

        def angle(edge1, edge2):
            vec1 = edge1.verts[1].co - edge1.verts[0].co
            vec2 = edge2.verts[1].co - edge2.verts[0].co
            return vec1.angle(vec2)

        e2 = sorted([e for e in self.face.edges if e is not e1], key=lambda e: 90. - angle(e1, e))[0]
        v11, v12 = e1.verts
        vb = v11
        v21, v22 = e2.verts

        uvb = self._get_vert_uv(vb)
        uv11 = self._get_vert_uv(v11)
        uv12 = self._get_vert_uv(v12)
        uv21 = self._get_vert_uv(v21)
        uv22 = self._get_vert_uv(v22)

        return (vb.co, v12.co-v11.co, v22.co-v21.co), (uvb, uv12-uv11, uv22-uv21)

    def calculate_co_with_uv_base(self, uv):
        a, b = calculate_co_by_uv_base(uv, self.base_uv)
        return self.base[0] + a * self.base[1] + b * self.base[2]

    def add_point(self, uv_point):
        self.points.append(self.calculate_co_with_uv_base(uv_point))

    def make(self):
        if not self.finish:
            raise Exception("no finish defined")

        if not self.points:
            try:
                common_edge = next(e for e in self.start.link_edges if e.other_vert(self.start) is self.finish)
                return tuple((common_edge, ))
            except StopIteration:
                pass

        try:
            face = next(f for f in self.start.link_faces if f in self.finish.link_faces)
        except StopIteration:
            raise
        new_face, loop = face_split(face, self.start, self.finish, self.points)

        new_edges = []
        for e in face.edges:
            if e in new_face.edges:
                new_edges.append(e)
        return tuple(new_edges)


class SideFormer:
    def __init__(self, target_mesh, path):
        self.target_mesh = target_mesh
        self.uv_layer = target_mesh.uv_layer
        self.path = path
        self.ribbon_face = self._adjust_ribbon_string_and_path()
        self.splits = []
        self.cut_path = None
        self.cut_path_edges = []

    def _adjust_ribbon_string_and_path(self):
        mapped_ends = set(self.target_mesh.map[v] for v in self.path.ends)
        try:
            side_edge = next(e for e in self.target_mesh.edges if len(set(e.verts) & mapped_ends) == 2)
        except StopIteration:
            raise Exception("Path does not fit to ribbon side")

        l = side_edge.link_loops[0]

        if l.vert is self.target_mesh.map[self.path.end]:
            self.path = self.path.reversed()

        return ExtraFace(DividableEdge(side_edge, self.uv_layer))

    def form_side_path(self):
        step = self.path.history[0]
        start_v = self.target_mesh.map[self.path.start]

        self._find_start_face(start_v, step)
        self._go_by_the_path(start_v)

        for s in self.splits:
            print(s.start, s.finish, s.points)
            self.cut_path_edges.extend(s.make())

        start_v = self.target_mesh.map[self.path.start]
        self.cut_path = Path.build(start_v, self.cut_path_edges)

        self.target_mesh.update_with_new_mesh_elements()
        self._remove_right_side_of_cut_path()
        self.target_mesh.remove_doubles()

    @staticmethod
    def _has_no_common_vert(start_v: bmesh.types.BMVert, edge: DividableEdge):
        return start_v not in edge.edges[0].verts

    def _is_counter_clockwise(self, start_v, step_dir, edge: DividableEdge):
        other_v = edge.edges[0].other_vert(start_v)
        edge_dir = get_vert_uv(self.uv_layer, other_v) - get_vert_uv(self.uv_layer, start_v)
        return step_dir.angle_signed(edge_dir) < 0.

    def _find_start_face(self, start_v, step):
        step_dir = get_vert_uv(self.uv_layer, step.vert) - get_vert_uv(self.uv_layer, self.path.start)

        while True:
            if self._has_no_common_vert(start_v, self.ribbon_face.next_edge) or \
                    self._is_counter_clockwise(start_v, step_dir, self.ribbon_face.next_edge):
                break
            self.ribbon_face = self.ribbon_face.next_face

    def _go_by_the_path(self, start_v):
        last_v = start_v
        split = Split(last_v, self.uv_layer, self.ribbon_face.face)

        split.add_point(self.path.history[0].vert.link_loops[0][self.uv_layer].uv)

        for step in self.path.history[1:-1]:
            print("step {} {}".format(step.vert, step.edge))
            v = None
            for direction, edge, face in self.ribbon_face.iter_neighbors():
                v = edge.try_cross(step)
                if v:
                    ensure_lookup(self.target_mesh.bm, ALL_MESH)
                    split.finish = v
                    self.splits.append(split)
                    self.ribbon_face = face
                    split = Split(v, self.uv_layer, face.face)
                    while True:
                        v = getattr(self.ribbon_face, '{}_edge'.format(direction)).try_cross(step)
                        if v:
                            ensure_lookup(self.target_mesh.bm, ALL_MESH)
                            split.finish = v
                            self.splits.append(split)
                            self.ribbon_face = getattr(self.ribbon_face, '{}_face'.format(direction))
                            split = Split(v, self.uv_layer, self.ribbon_face.face)
                        else:
                            break
                    break
            if not v:
                split.add_point(step.vert.link_loops[0][self.uv_layer].uv)

        split.finish = self.target_mesh.map[self.path.end]
        self.splits.append(split)

    def _has_same_direction(self, step1, step2):
        dir1 = get_vert_uv(self.uv_layer, step1.vert) - get_vert_uv(self.uv_layer, step1.edge.other_vert(step1.vert))
        dir2 = get_vert_uv(self.uv_layer, step2.vert) - get_vert_uv(self.uv_layer, step2.edge.other_vert(step2.vert))
        return dir1.angle(dir2) < 0.0001

    def _has_the_same_uv_location(self, v1, v2):
        uv1 = get_vert_uv(self.uv_layer, v1)
        uv2 = get_vert_uv(self.uv_layer, v2)
        dist = (uv1 - uv2).length
        return dist > 0.0001

    def _remove_right_side_of_cut_path(self):
        to_remove = set()
        for step in self.cut_path:
            step.edge.select_set(True)
            try:
                l = next(l for l in step.vert.link_loops if l.edge is step.edge)
                to_remove.add(l.face)
            except StopIteration:
                pass

        for f in to_remove:
            self.target_mesh.remove(f)

        to_remove = []
        for e in self.target_mesh.edges:
            if len(e.link_faces) == 0:
                to_remove.append(e)

        for e in to_remove:
            self.target_mesh.remove(e)
