import bpy
import bmesh

from .processor import RibbonateMeshProcessor
from ..common.structures import MeshPart


class RibbonateMeshOperator(bpy.types.Operator):
    """
    Transformates segment for ribbons where folds are always
    connecting different ribs
    """

    bl_idname = "edit_mesh.ribbonate_mesh_operator"
    bl_label = "Ribbonate mesh"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.mode == 'EDIT_MESH'

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        uv_layer = context.object.data.uv_textures.new("__ribbon__")

        processor = RibbonateMeshProcessor(context, bm, MeshPart.from_selection(bm), uv_layer.name)
        processor.process()

        bm.transform(context.active_object.matrix_world.inverted())
        bmesh.update_edit_mesh(context.active_object.data)
        # context.object.data.uv_textures.remove(uv_layer)

        return {'FINISHED'}
