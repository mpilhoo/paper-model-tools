import bmesh
from ..common.algorithm import (
    mesh_between_cycles,
    mesh_inside_borders,
    find_shortest_paths
)
from ..common.structures import (
    MeshStructureException,
    find_selected_cycles_with_bridge,
    find_selected_two_paths
)
from ..common.submeshes import MeshPart, Path


class SourceMesh(MeshPart):
    def __init__(self, mesh_part: MeshPart):
        self._side_paths = None
        self.verts = mesh_part.verts
        self.edges = mesh_part.edges
        self.faces = mesh_part.faces

    @classmethod
    def build_from_selection(cls, bm, closed, selection):
        if closed is not False:
            try:
                frames, bridge = find_selected_cycles_with_bridge(selection)
                source_mesh = SourceMesh(mesh_between_cycles(*frames, bridge))
                source_mesh.frames = frames
                source_mesh.closed = closed = True
                source_mesh._bridge = bridge
            except MeshStructureException as e:
                if closed is True:
                    raise e

        if closed is not True:
            frames = find_selected_two_paths(selection)
            paths = cls._connect_frames_with_side_paths(frames)

            source_mesh = SourceMesh(mesh_inside_borders(frames[0] | frames[1] | paths[0] | paths[1]))

            # TODO optimize:
            final_paths = []
            for path in paths:
                if all(end.is_boundary for end in path.ends):
                    final_paths.append(source_mesh.find_alternative_boundary_path(path))
                else:
                    conn = bmesh.ops.connect_vert_pair(bm, verts=path.ends)
                    final_path = Path.build(
                        start=path.start,
                        edges=conn['edges'],
                    )
                    final_path.is_cut = True
                    final_paths.append(final_path)

            source_mesh = SourceMesh(mesh_inside_borders(frames[0] | frames[1] | final_paths[0] | final_paths[1]))
            source_mesh._side_paths = tuple(final_paths)
            source_mesh.frames = frames
            source_mesh.closed = False

        return source_mesh

    @staticmethod
    def _connect_frames_with_side_paths(frames):
        v1, v2 = tuple(frames[0].ends)
        paths = {}
        exclude = set()
        for frame in frames:
            exclude |= frame.edges

        paths.update(find_shortest_paths(set((v1,)), frames[1].ends, excluded=exclude))
        paths.update(find_shortest_paths(set((v2,)), frames[1].ends, excluded=exclude))

        def paths_with_none_intersection():
            paths1 = paths[v1]
            paths2 = paths[v2]

            for path1 in paths1:
                for path2 in paths2:
                    if not path1.intersect(path2):
                        return path1, path2
            raise Exception("All paths are crossing")

        return paths_with_none_intersection()

    def find_alternative_boundary_path(self, path):
        e = path.history[0].edge
        faces = set(f for f in path.start.link_faces if f in self)
        while not e.is_boundary:
            f = next(f for f in e.link_faces if f not in faces)
            faces.add(f)
            e = next(ne for ne in f.edges if (path.start in ne.verts and ne is not e))

        p = Path(path.start).next_step(e)
        while p.current not in path.ends:
            e = next(nv for nv in p.current.link_edges if (nv.is_boundary and nv is not e))
            p = p.next_step(e)

        boundary_path = p if p.end is path.end else path
        boundary_path.is_cut = False
        return boundary_path

    @property
    def bridge(self):
        if self.closed:
            return self._bridge
        raise Exception("no bridge in unclosed ribbons")

    @property
    def side_paths(self):
        if self.closed:
            return tuple()
        return self._side_paths
