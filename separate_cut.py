import bmesh
import bpy
import math


selected_verts = lambda bm: (v for v in bm.verts if v.select)
selected_edges = lambda bm: (e for e in bm.edges if e.select)


def signed_angle_3d(v1, v2, no):
    return v1.angle(v2) if v1.cross(v2) * no >= 0. else 2.*math.pi-v1.angle(v2)


def edge_signed_angle(e1, e2, v):
    return signed_angle_3d(e1.other_vert(v).co-v.co, e2.other_vert(v).co-v.co, v.normal)


def side_division(vert, prev_edge, next_edge):
    sorted_loops = radial_sorted_loops(vert, next_edge)
    angles = [l.calc_angle() for l in sorted_loops]
    div_index = next(i for i, l in enumerate(sorted_loops, 1) if l.edge is prev_edge)
    left_loops, right_loops = sorted_loops[div_index:], sorted_loops[:div_index]
    right_angle = sum([a for a in angles[:div_index]])
    left_angle = sum([a for a in angles[div_index:]])
    return {
        'right': {'angle': right_angle, 'loops': right_loops},
        'left': {'angle': left_angle, 'loops': left_loops}
    }


def radial_sorted_loops(vert, start_from=None):
    wire_edges = [e for e in vert.link_edges if e.is_wire]
    opening_edges = set([l.edge for l in vert.link_loops if l.edge.is_boundary]) | set(wire_edges)
    closing_edges = [e for e in vert.link_edges if e.is_boundary and e not in opening_edges] + wire_edges

    class FakeLoop:
        def __init__(self, edge, angle):
            self.edge = edge
            self.angle = angle
            self.face = None

        def index(self):
            return -1

        def calc_angle(self):
            return self.angle

    def find_closing_egde(edge):
        min_angle = -100.
        next_edge = None
        for e in closing_edges:
            angle = edge_signed_angle(edge, e, vert)
            if angle > min_angle:
                min_angle = angle
                next_edge = e
        return next_edge, 2*math.pi-min_angle

    if not start_from:
        start_from = vert.link_edges[0]
    l = FakeLoop(*find_closing_egde(start_from)) \
        if start_from in closing_edges \
        else [l for l in start_from.link_loops if l.vert == vert][0]

    loops = []
    condition = True
    while condition:
        loops.append(l)
        if l.edge in opening_edges:
            l = FakeLoop(*find_closing_egde(l.edge))
        elif l.edge.is_boundary:
            l = l.edge.link_loops[0].link_loop_next
        else:
            l = l.link_loop_radial_next.link_loop_next
        condition = (l.edge != start_from)
    loops.append(loops.pop(0))
    return loops



def make_separating_cut(bm, edges, leave_faces=set()):
    selected_link_edges = lambda v: (e for e in v.link_edges if e in edges)

    ends = []
    for e in edges:
        for v in e.verts:
            if len(tuple(selected_link_edges(v))) == 1:
                ends.append(v)
    if len(ends) != 2:
        raise Exception("Wrong selection (ends: %s)" % str(ends))

    v1, v2 = ends

    ne = next(selected_link_edges(v1))
    visited = set((ne, ))
    v = ne.other_vert(v1)

    new_verts = []

    while v is not v2:
        pe = ne
        ne = next(e for e in selected_link_edges(v) if e not in visited)
        visited.add(ne)

        sd = side_division(v, pe, ne)
        for l in sd['right']['loops']:
            if l.face is None:
                continue
            new_verts.append(bmesh.utils.loop_separate(l))

        v = ne.other_vert(v)

    bmesh.ops.remove_doubles(bm, verts=new_verts, dist=0.001)



class SeparatingCutOperator(bpy.types.Operator):
    bl_idname = "edit_mesh.separating_cut"
    bl_label = "Separating cut"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.mode == 'EDIT_MESH'

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.object.data)
        make_separating_cut(bm, set(selected_edges(bm)))
        return {'FINISHED'}

if __name__ == "__main__":
    try:
        bpy.utils.unregister_class(SeparatingCutOperator)
    except RuntimeError:
        pass
    bpy.utils.register_class(SeparatingCutOperator)
