import bmesh
import bpy

from .common.values import scaled_length
from .common.uv import clean_uv_map
from ribbons import RibbonateMeshProcessor


class RadialCutsProcessor(object):
    """
    Cuts mesh by rotating mid-planes and simplify parts between cuts with ribbons tool.
    """

    def __init__(self, context, bm, selection, cut_count, 
                 closed=None, threshold=None, 
                 uv_layer=None):
        self._selection = None
        self._threshold = None
        self.context = context
        self.bm = bm
        self._uv_layer = uv_layer
        self._closed = closed
        self.cut_count = cut_count

    @property
    def threshold(self):
        if self._threshold is None:
            self._threshold = scaled_length(.3, 'mm')
        return self._threshold

    @property
    def uv_layer(self):
        if not self._uv_layer:
            self._uv_layer = self.bm.loops.layers.uv.new('__ribbon__')
            clean_uv_map(self.bm, self._uv_layer)
        return self._uv_layer

    def find_selected_cycle_and_path(self):
        # TODO basis, cut, focus, fork = find_selected_cycle_and_path(bm)
        pass

    def clean(self):
        # TODO ?
        for ribbon in self.ribbons:
            ribbon.clean()

    def process(self):
        clean_uv_map(self.bm, self.uv_layer)
        self.find_selected_cycle_and_path()
        # TODO
        self.clean()
