import bpy
import bmesh
from math import cos
from mathutils import Vector
from mathutils.geometry import intersect_line_line_2d as intersect_line_segments_2d
from .math import intersect_lines_2d
from .helpers import (
    ensure_lookup,
    VERTS, EDGES, FACES, ALL_MESH
)


def clean_uv_map(bm, uv_layer):
    for v in bm.verts:
        for l in v.link_loops:
            l[uv_layer].uv = Vector((0., 0.))
            l[uv_layer].pin_uv = False


def get_vert_uv(uv_layer, vert):
    return next(l for l in vert.link_loops)[uv_layer].uv


def setup_cut_uv(bridge, source, target, v_map, uv_layer):
    begin, end = (v_map[v] for v in bridge['ends'])
    rss = (end.co - begin.co).cross(begin.normal)  # rigt side specifier

    def get_left_right_faces(e):
        return sorted(e.link_faces, key=lambda f: rss.dot(f.calc_center_median() - begin.co))

    cut_edge = next(e for e in begin.link_edges if e.other_vert(begin) == end)
    clf, crf = get_left_right_faces(cut_edge)   # cut left/right faces
    lcb = next(l for l in begin.link_loops if l.face == clf)[uv_layer].uv   # left cut begin
    lce = next(l for l in end.link_loops if l.face == clf)[uv_layer].uv     # left cut end
    lcv = lce - lcb                                                         # left cut vector

    rcb = next(l for l in begin.link_loops if l.face == crf)[uv_layer].uv   # right cut as above...
    rce = next(l for l in end.link_loops if l.face == crf)[uv_layer].uv
    rcv = rce - rcb

    path_len = sum(e.calc_length() for e in bridge['edges'])

    def linked_edges(v):
        return [le for le in v.link_edges if le in bridge['edges']]

    conn = {}
    for e in bridge['edges']:
        for v in e.verts:
            if v not in conn:
                conn[v] = linked_edges(v)

    def get_vert_left_right_faces(v):
        return {f for f in v.link_faces if (f in source['faces'] and rss.dot(f.calc_center_median() - begin.co) < 0)}, \
               {f for f in v.link_faces if (f in source['faces'] and rss.dot(f.calc_center_median() - begin.co) > 0)}

    prev_v = bridge['ends'][0]
    prev_e = conn[prev_v][0]
    it = prev_e.other_vert(prev_v)
    lf, rf = get_vert_left_right_faces(prev_v)

    for l in prev_v.link_loops:
        if l.face in lf:
            l[uv_layer].uv = lcb
            l[uv_layer].pin_uv = True
        if l.face in rf:
            l[uv_layer].uv = rcb
            l[uv_layer].pin_uv = True

    curr_dist = 0.

    while True:
        curr_dist += prev_e.calc_length()
        lf, rf = get_vert_left_right_faces(it)
        for l in it.link_loops:
            if l.face in lf:
                l[uv_layer].uv = lcb + lcv * (curr_dist / path_len)
                l[uv_layer].pin_uv = True
            if l.face in rf:
                l[uv_layer].uv = rcb + rcv * (curr_dist / path_len)
                l[uv_layer].pin_uv = True

        try:
            prev_e = next(e for e in conn[it] if e is not prev_e)
        except StopIteration:
            break
        prev_v = it
        it = prev_e.other_vert(it)


def setup_straith_path_uv(path, source, target, v_map, uv_layer):
    begin, end = (v_map[v] for v in path['ends'])
    begin_uv, end_uv  = next(l for l in begin.link_loops)[uv_layer].uv, next(l for l in end.link_loops)[uv_layer].uv
    path_vector = end_uv - begin_uv

    path_len = sum(e.calc_length() for e in path['edges'])

    def linked_edges(v):
        return [le for le in v.link_edges if le in path['edges']]

    conn = {}
    for e in path['edges']:
        for v in e.verts:
            if v not in conn:
                conn[v] = linked_edges(v)

    prev_v = path['ends'][0]
    prev_e = conn[prev_v][0]
    it = prev_e.other_vert(prev_v)

    for l in prev_v.link_loops:
        if l.face in source['faces']:
            l[uv_layer].uv = begin_uv
            l[uv_layer].pin_uv = True

    curr_dist = 0.
    while True:
        curr_dist += prev_e.calc_length()
        for l in it.link_loops:
            if l.face in source['faces']:
                l[uv_layer].uv = begin_uv + path_vector * (curr_dist / path_len)
                l[uv_layer].pin_uv = True
        try:
            prev_e = next(e for e in conn[it] if e is not prev_e)
        except StopIteration:
            break
        prev_v = it
        it = prev_e.other_vert(it)


def setup_boundary_path_uv(path, source, target, v_map, uv_layer):
    for v in path['ends']:
        mapped_v = v_map[v]
        uv = next(l for l in mapped_v.link_loops)[uv_layer].uv

        for l in v.link_loops:
            if l.face in source['faces']:
                l[uv_layer].uv = uv
                l[uv_layer].pin_uv = True

    done = set()
    faces_to_unwrap = []
    for e in path['edges']:
        for v in e.verts:
            if v in done:
                continue
            for f in v.link_faces:
                if f in source['faces']:
                    faces_to_unwrap.append(f)
            done.add(v)

    for f in faces_to_unwrap:
        f.select_set(True)
    bpy.ops.uv.unwrap()
    for f in faces_to_unwrap:
        f.select_set(False)

    for v in done:
        for l in v.link_loops:
            if l.face in source['faces']:
                l[uv_layer].pin_uv = True


def fit_uv(source, target, v_map, uv_layer):
    for f in source['faces']:
        f.select_set(True)
    for v, mv in v_map.items():
        if v.select:
            uv = mv.link_loops[0][uv_layer].uv
            for l in v.link_loops:
                if l[uv_layer].pin_uv:
                    continue
                if l.face in source['faces']:
                    l[uv_layer].uv = uv
                    l[uv_layer].pin_uv = True

    bpy.ops.uv.unwrap()

    for f in source['faces']:
        f.select_set(False)


def connect_path_uv(bm, v1, v2, uv_layer, faces, threshold=0.0001):
    def edge_uvs(e):
        for l in e.link_loops:
            v = l.vert
            ov = e.other_vert(v)
            f = l.face
            ol = next(ll for ll in ov.link_loops if ll.face is f)
            if v is e.verts[0]:
                yield (l[uv_layer].uv, ol[uv_layer].uv)
            else:
                yield (ol[uv_layer].uv, l[uv_layer].uv)


    old_edges = [e for e in bm.edges if any((f in faces) for f in e.link_faces)]

    vert_uvs = lambda v: tuple(loop[uv_layer].uv for loop in v.link_loops if loop.face in faces)

    # there can be multiple uv coordinates for vertice so the nearest pair is chosen
    def best_pair(v1_uvs, v2_uvs):
        return min(min(((uv2 - uv1).length, uv1, uv2) for uv2 in v2_uvs) for uv1 in v1_uvs)

    _, p1, p2 = best_pair(vert_uvs(v1), vert_uvs(v2))

    verts = {}
    for e in old_edges:
        for ev1, ev2 in edge_uvs(e):
            intersection = intersect_line_segments_2d(ev1, ev2, p1, p2)
            if intersection:
                if (ev2-intersection).length < threshold:
                    verts[e.verts[1]] = intersection
                elif (ev1-intersection).length < threshold:
                    verts[e.verts[0]] = intersection
                else:
                    ne, nv = bmesh.utils.edge_split(e, e.verts[0], (ev1-intersection).length / (ev1-ev2).length)
                    ensure_lookup(bm, VERTS | EDGES)
                    verts[nv] = intersection
            break

    path = []
    sorted_verts = sorted(((((p2 - verts[v]).length) / (p2-p1).length, v)
                          for v in verts.keys()), key=lambda el: (el[0], el[1].index))
    for i in range(len(sorted_verts)-1):
        try:
            new_edge = next(iter(bmesh.ops.connect_verts(bm, verts=tuple(v[1] for v in sorted_verts[i:i+2]))['edges']))
            ensure_lookup(EDGES | FACES)
            path.append(new_edge)
        except StopIteration:
            path.append(None)   # FIXME may not be needed

    return path, sorted_verts


def get_base(f, uv_layer): 
    e1, e2 = sorted((e for e in f.edges), key=lambda e: e.calc_length())[1:]
    bv = [next(iter(set(e1.verts) & set(e2.verts)))]
    bv.extend((e1.other_vert(bv[0]), e2.other_vert(bv[0])))
    face_loop = lambda v: next(iter(l for l in v.link_loops if l.face is f))
    b = [face_loop(v)[uv_layer].uv for v in bv]
    for i in range(1,3):
        b[i] = b[i]-b[0]

    return b, bv


def calculate_co_by_uv_base(uv_co, base):
    ill1 = intersect_lines_2d(base[0], base[0] + base[1], uv_co,   uv_co + base[2])
    ill2 = intersect_lines_2d(base[0], base[0] + base[2], uv_co,   uv_co + base[1])

    v1 = ill1 - base[0]
    cos1 = cos(v1.angle(base[1])) if v1.length > 0. else 1.
    v2 = ill2 - base[0]
    cos2 = cos(v2.angle(base[2])) if v2.length > 0. else 1.

    a, b = cos1 * v1.length / base[1].length, \
           cos2 * v2.length / base[2].length
    return a, b
