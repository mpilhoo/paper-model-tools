import bpy


# checking version is over 2.72
ENSURE_LOOKUP = (int(bpy.app.version_string.split()[0].split('.')[1]) > 72)
VERTS = 1
EDGES = 2
FACES = 4
ALL_MESH = 7


def ensure_lookup(bm, flags):
    if ENSURE_LOOKUP:
        if flags & VERTS:
            bm.verts.ensure_lookup_table()
        if flags & EDGES:
            bm.edges.ensure_lookup_table()
        if flags & FACES:
            bm.faces.ensure_lookup_table()


def get_separated_object():
    old_meshes = set(ob for ob in bpy.data.objects if ob.type == 'MESH')
    bpy.ops.mesh.separate()
    return next(ob for ob in bpy.data.objects if (ob.type == 'MESH' and ob not in old_meshes))



