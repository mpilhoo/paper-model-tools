import bpy

INF = float("inf")

DEL_VERTS = 1
DEL_EDGES = 2
DEL_ONLYFACES = 3
DEL_EDGESFACES = 4
DEL_FACES = 5
DEL_ALL = 6
DEL_ONLYTAGGED = 7

unit_multiplier = {
    'm': 1.,
    'cm': 0.01,
    'mm': 0.001
}


def scaled_length(length, unit='mm'):
    return length * unit_multiplier[unit] * bpy.context.scene.paper_model.scale
