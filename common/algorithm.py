import collections
import math
from queue import Queue, PriorityQueue

from .submeshes import MeshPart, Path


def select_set(selected: MeshPart):
    for el in selected:
        el.select_set(True)


def find_shortest_paths(begin_set: set,
                        end_set: set,
                        on_each: collections.Callable=None,
                        on_finish: collections.Callable=None,
                        dist_func: collections.Callable=None,
                        excluded: set=None):
    q = PriorityQueue()
    for v in begin_set:
        q.put(Path(v))

    paths = {}

    finished = 0
    visited = set()
    if excluded:
        visited |= excluded
    while not q.empty() and finished < len(end_set):
        branch = q.get()
        if branch.distance == float('inf'):
            break
        if branch.current in visited:
            continue

        visited.add(branch.current)

        if on_each:
            on_each(branch)
        if branch.current in end_set:
            finished += 1
            if on_finish:
                on_finish(branch)
            if branch.start not in paths:
                paths[branch.start] = []
            paths[branch.start].append(branch)
            continue

        for e in branch.current.link_edges:
            if e not in visited:
                visited.add(e)
                q.put(branch.next_step(e, dist_func))
    return paths


def vert_bfs_select(start_verts, border_verts, border_edges, select=False) -> MeshPart:
    selected = MeshPart(border_verts, border_edges)

    queue = Queue()
    for v in start_verts:
        queue.put(v)
        selected.verts.add(v)

    for v in border_verts:
        for e in v.link_edges:
            if e.other_vert in border_verts and e not in border_edges:
                selected.edges.add(e)
                for f in e.link_faces:
                    selected.faces.add(f)

    while not queue.empty():
        v = queue.get()
        selected.faces.update(set([f for f in v.link_faces]))
        for e in v.link_edges:
            selected.edges.add(e)
            vc = e.other_vert(v)
            if vc in selected.verts:
                continue
            selected.verts.add(vc)
            queue.put(vc)

    if select:
        select_set(selected)

    return selected


def face_bfs(queue: Queue, border_edges: set):
    selected = MeshPart(edges=border_edges)

    visited = set()

    while not queue.empty():
        f = queue.get()
        visited.add(f)
        selected.faces.add(f)
        for e in f.edges:
            if e in border_edges:
                continue
            selected.edges.add(e)
            for cf in e.link_faces:
                if cf not in visited:
                    queue.put(cf)
                    visited.add(cf)

    selected.add_linked()
    return selected


def mesh_inside_borders(border: MeshPart):
    right_sum = left_sum = 0.
    right_verts, left_verts = set(), set()
    border_verts = []
    v = next(e.verts[0] for e in border.edges)
    he = []
    for e in v.link_edges:
        if e in border.edges:
            he.append(e)
    if len(he) != 2:
        raise Exception("BM_INSIDE: INVALID-BORDER -- %s" % [e.index for e in border.edges])
    prev_edge, next_edge = he
    start_v = v

    while True:
        border_verts.append(v)
        sd = side_division(v, prev_edge, next_edge)
        left_sum += sd['left']['angle']
        left_verts.update([l.edge.other_vert(v) for l in sd['left']['loops'] if l.edge not in border.edges])

        right_sum += sd['right']['angle']
        right_verts.update([l.edge.other_vert(v) for l in sd['right']['loops'] if l.edge not in border.edges])

        v = next_edge.other_vert(v)
        prev_edge = next_edge
        for e in v.link_edges:
            if e in border.edges and e != prev_edge:
                next_edge = e
                break
        if prev_edge == next_edge:
            raise Exception("BM_INSIDE: INVALID-BORDER -- %s" % [e.index for e in border.edges])
        if v == start_v:
            break

    right_verts.difference_update(border_verts)
    left_verts.difference_update(border_verts)
    selection = vert_bfs_select(
        list(right_verts) if right_sum < left_sum else list(left_verts), 
        border_verts, border.edges
    )

    for e in border.edges:
        for f in e.link_faces:
            if all(v in border_verts for v in f.verts):
                selection.faces.add(f)

    return selection


def mesh_between_cycles(cycle1: MeshPart, cycle2: MeshPart, bridge: Path=None):
    queue = Queue()
    if not bridge:
        raise NotImplemented()
        # TODO #798844 find bridge
        # find_shortest_paths(cycle1.verts, cycle2.verts)

    for e in bridge.iter_edges():
        for f in e.link_faces:
            queue.put(f)

    return face_bfs(queue, cycle1.edges | cycle2.edges)


def select_from_face_to_border(border, start, select=False):
    queue = Queue()
    queue.put(start)
    return face_bfs(queue, border, select)


AUTO = 0
SINGLE_CYCLE_BORDER = 1
BETWEEN_TWO_CYCLES = 2

def select_inside(border_edges, border_type=AUTO):  # TODO
    def generate_queue(border_edges, border_type):
        def find_cycle():
            pass
        
        if border_type is AUTO:
            e = next(border_edges)
            start_v = next(e.verts)
            v = e.other_vert(start_v)
            
        # TODO
        # 1. inside edge cycle
        # 2. between two edge cycles
        raise Exception("Invalid border")

    inside_geom = MeshPart(edges=border_edges)
    q = generate_queue(border_edges)

    while not q.empty():
        f = q.get()
        inside_geom.faces.add(f)
        for e in f.edges:
            if e in border_edges:
                continue
            inside_geom.edges.add(e)
            for lf in e.link_faces:
                if lf in inside_geom.faces:
                    continue
                inside_geom.faces.add(lf)
                q.put(lf)

    for e in inside_geom.edges:
        for v in e.verts:
            inside_geom.verts.add(v)

    return inside_geom


def signed_angle_3d(v1, v2, no):
    return v1.angle(v2) if v1.cross(v2) * no >= 0. else 2.*math.pi-v1.angle(v2)


def edge_signed_angle(e1, e2, v):
    return signed_angle_3d(e1.other_vert(v).co-v.co, e2.other_vert(v).co-v.co, v.normal)


def side_division(vert, prev_edge, next_edge):
    sorted_loops = radial_sorted_loops(vert, next_edge)
    angles = [l.calc_angle() for l in sorted_loops]
    div_index = next(i for i, l in enumerate(sorted_loops, 1) if l.edge is prev_edge)
    left_loops, right_loops = sorted_loops[div_index:], sorted_loops[:div_index]
    right_angle = sum([a for a in angles[:div_index]])
    left_angle = sum([a for a in angles[div_index:]])
    return {
        'right': {'angle': right_angle, 'loops': right_loops},
        'left': {'angle': left_angle, 'loops': left_loops}
    }


def radial_sorted_loops(vert, start_from=None):
    wire_edges = [e for e in vert.link_edges if e.is_wire]
    opening_edges = set([l.edge for l in vert.link_loops if l.edge.is_boundary]) | set(wire_edges)
    closing_edges = [e for e in vert.link_edges if e.is_boundary and e not in opening_edges] + wire_edges

    class FakeLoop:
        def __init__(self, edge, angle):
            self.edge = edge
            self.angle = angle
            self.face = None

        def index(self):
            return -1

        def calc_angle(self):
            return self.angle

    def find_closing_egde(edge):
        min_angle = -100.
        next_edge = None
        for e in closing_edges:
            angle = edge_signed_angle(edge, e, vert)
            if angle > min_angle:
                min_angle = angle
                next_edge = e
        return next_edge, 2*math.pi-min_angle

    if not start_from:
        start_from = vert.link_edges[0]
    l = FakeLoop(*find_closing_egde(start_from)) \
        if start_from in closing_edges \
        else [l for l in start_from.link_loops if l.vert == vert][0]

    loops = []
    condition = True
    while condition:
        loops.append(l)
        if l.edge in opening_edges:
            l = FakeLoop(*find_closing_egde(l.edge))
        elif l.edge.is_boundary:
            l = l.edge.link_loops[0].link_loop_next
        else:
            l = l.link_loop_radial_next.link_loop_next
        condition = (l.edge != start_from)
    loops.append(loops.pop(0))
    return loops
