import bpy


def bake_textures(bm, target, context, suffix):
    target.name = context.object.name + suffix
    target.data.materials.clear()
    for tex_name, tex in target.data.uv_textures.items():
        if tex_name != uv_layer.name:
            target.data.uv_textures.remove(tex)
    uv_name = uv_layer.name

    bpy.ops.object.editmode_toggle()
    # TODO  calculate image size for new texture
    img = bpy.data.images.new('%s_texture' % target.name, 1024, 1024, alpha=True)
    texfaces = context.object.data.uv_textures[uv_name].data
    for index, tf in texfaces.items():
        tf.image = img if index in fids else None

    context.scene.render.bake_margin = 1
    context.scene.render.bake_type = 'TEXTURE'
    bpy.ops.object.bake_image()

    mat = bpy.data.materials.new(target.name)
    target.data.materials.append(mat)
    tex = bpy.data.textures.new(target.name, 'IMAGE')
    tex.image = bpy.data.images[img.name]
    slot = mat.texture_slots.add()
    slot.texture = tex
    slot.uv_layer = uv_name
