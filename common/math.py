from mathutils import Vector


def intersect_lines_2d(a1, a2, b1, b2):
    da = a2-a1
    db = b2-b1
    dp = a1-b1
    dap = Vector((-da[1], da[0]))
    denom = dap.dot(db)
    num = dap.dot(dp)

    return (num / denom)*db + b1


def co_middle(co1, co2):
    return co1 + (co2-co1) / 2.


def edge_middle(e):
    return co_middle(e.verts[0].co, e.verts[1].co)


def edge_direction(e, v):
    return (e.other_vert(v).co - v.co).normalized()
