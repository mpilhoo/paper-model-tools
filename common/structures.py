import bmesh
import collections
from copy import copy
from queue import Queue

from .submeshes import MeshPart, Path


linked_edges_len = lambda v, edges: len(tuple(e for e in v.link_edges if e in edges))


class MeshStructureException(Exception):
    pass


def find_verts_with_level(mesh_part: MeshPart, level: int):
    if not mesh_part:
        mesh_part = MeshPart.from_selection()

    return (v for v in mesh_part.verts if linked_edges_len(v, mesh_part.edges) == level)


def find_forks(mesh_part: MeshPart):
    return find_verts_with_level(mesh_part, 3)


def find_ends(mesh_part: MeshPart):
    return find_verts_with_level(mesh_part, 1)


def find_path(start: bmesh.types.BMVert, ends, edges: collections.Iterable) -> Path:
    if isinstance(ends, bmesh.types.BMVert):
        ends = {ends, }
    path = Path(start)

    q = Queue()
    q.put(path)
    visited = {start, }
    while not q.empty():
        path = q.get()
        if path.current in ends:
            break
        for e in path.current.link_edges:
            if e not in edges:
                continue
            cv = e.other_vert(path.current)
            if cv in visited:
                continue
            visited.add(cv)
            q.put(path.next_step(e));

    return path


def find_cycle(v, edges):
    edges = set(edges)
    cycle = Path(v)
    visited = set()
    while cycle.current not in visited:
        visited.add(cycle.current)
        edge = next(e for e in cycle.current.link_edges if e in edges)
        edges.remove(edge)
        cycle = cycle.next_step(edge)

    return cycle


def find_selected_cycle_and_path(bm: bmesh.types.BMesh):
    selection = MeshPart.from_selection(bm)

    forks = find_forks(selection)
    fork = next(forks)
    try:
        next(forks)
        raise MeshStructureException('Too many potential fork vertices')
    except StopIteration:
        pass
    ends = find_ends(selection)
    focus = next(ends)
    try:
        next(ends)
        raise MeshStructureException('Too many potential top vertices')
    except StopIteration:
        pass
    for v in selection.verts:
        if v is focus or v is fork:
            continue
        if linked_edges_len(v, selection.edges) != 2:
            raise MeshStructureException("Invalid mesh part given. Vertice {} has "
                                         "wrong number of edges linked".format(v))

    cut = find_path(focus, {fork, }, selection.edges)
    selection.edges -= set(cut.edges)
    cycle = find_cycle(fork, selection.edges)
    redundance = selection.edges.difference(cycle)
    if redundance:
        raise MeshStructureException("Input redundance: {}".format(redundance))

    return cycle, cut


def find_selected_cycles_with_bridge(selection: MeshPart):
    selection = copy(selection)
    bridge_ends = tuple(find_forks(selection))
    if len(bridge_ends) != 2:
        raise MeshStructureException("Potential bridge ends should count two, found {}".format(len(bridge_ends)))
    for v in selection.verts:
        le = linked_edges_len(v, selection.edges)
        if v in bridge_ends:
            continue
        if le != 2:
            raise MeshStructureException("Invalid mesh part given. Vertice {} has wrong "
                                         "number of edges linked".format(v))

    v1, v2 = bridge_ends
    bridge = find_path(v1, v2, selection.edges)
    selection.edges -= set(bridge.iter_edges())

    cycle1 = MeshPart.from_path(find_cycle(v1, selection.edges))
    cycle2 = MeshPart.from_path(find_cycle(v2, selection.edges))

    redundance = selection.edges.difference(cycle1.edges | cycle2.edges)
    if redundance:
        raise MeshStructureException("Input redundance: {}".format(redundance))

    return (cycle1, cycle2), bridge


def find_selected_two_paths(selection: MeshPart):
    selection = copy(selection)
    ends = tuple(find_ends(selection))
    if len(ends) != 4:
        raise MeshStructureException("Potential path ends should be four, found {}".format(len(ends)))
    tmp = set(ends)

    try:
        f1s = tmp.pop()
        frame1 = find_path(f1s, tmp, selection.edges)
        tmp.remove(frame1.end)

        f2s = tmp.pop()
        frame2 = find_path(f2s, tmp, selection.edges)
    except Exception as e:
        raise MeshStructureException("Exception catched: {}".format(e))  # TODO

    return (
        MeshPart.from_path(frame1),
        MeshPart.from_path(frame2),
    )
