import bmesh
from collections import namedtuple, Iterable, Callable
from copy import copy

from .selection import (
    selected_verts,
    selected_edges,
    selected_faces
)


class Path:
    pass


class MeshPart:
    def __init__(self,
                 verts: Iterable=tuple(),
                 edges: Iterable=tuple(),
                 faces: Iterable=tuple(),
                 mixed: Iterable=None,
                 **kwargs):
        self.verts = set(verts)
        self.edges = set(edges)
        self.faces = set(faces)

        if mixed:
            for item in mixed:
                self.add(item)

        for name, val in kwargs.items():
            setattr(self, name, val)

    def add_linked(self):
        for f in self.faces:
            self.edges.update(e for e in f.edges)
        for e in self.edges:
            self.verts.update(v for v in e.verts)

    def __iter__(self):
        for v in self.verts:
            yield v
        for e in self.edges:
            yield e
        for f in self.faces:
            yield f

    def add(self, item):
        if isinstance(item, bmesh.types.BMVert):
            self.verts.add(item)
        elif isinstance(item, bmesh.types.BMEdge):
            self.edges.add(item)
        elif isinstance(item, bmesh.types.BMFace):
            self.faces.add(item)
        else:
            raise TypeError("Item {} is not bmesh part type".format(item))

    def select_set(self, select):
        for el in self:
            el.select_set(select)

    @classmethod
    def from_selection(cls, bm: bmesh.types.BMesh):
        return MeshPart(selected_verts(bm), selected_edges(bm),  selected_faces(bm))

    @classmethod
    def from_path(cls, path: Path):
        path = MeshPart(edges=[*path.iter_edges()], ends=(path.start, path.end))
        path.add_linked()
        return path

    def __contains__(self, item):
        if isinstance(item, bmesh.types.BMVert):
            return item in self.verts
        if isinstance(item, bmesh.types.BMEdge):
            return item in self.edges
        if isinstance(item, bmesh.types.BMFace):
            return item in self.faces
        if isinstance(item, MeshPart):
            return all((i in self) for i in item)
        if isinstance(item, Path):
            return all(e in self.edges for e in item.iter_edges()) or \
                   all(v in self.vert for v in item.iter_verts())
        return False

    def __or__(self, other):
        if isinstance(other, MeshPart):
            return MeshPart(self.verts | other.verts,
                            self.edges | other.edges,
                            self.faces | other.faces)
        elif isinstance(other, Path):
            return MeshPart(self.verts, 
                            self.edges | set(e for e in other.iter_edges()),
                            self.faces)
        elif isinstance(other, set):
            return self | MeshPart(mixed=other)
        else:
            raise TypeError("Unsupported type ({}) for MeeshPart bool operation.".format(type(other)))


PathStep = namedtuple('PathStep', ('vert', 'edge'))


class Path:
    def __init__(self,
                 start: bmesh.types.BMVert,
                 distance: float = 0.,
                 history: tuple = tuple()):
        self.start = start
        self.distance = distance
        self.history = history
        self._reversed = None

    @classmethod
    def build(cls, start: bmesh.types.BMVert, edges: Iterable) -> Path:
        history = []
        if isinstance(edges, set):
            _edges = copy(edges)
        else:
            _edges = set(edges)

        v = start
        distance = 0.
        while len(_edges):
            e = (set(v.link_edges) & _edges).pop()
            _edges.remove(e)
            nv = e.other_vert(v)
            history.append(PathStep(nv, e))
            distance += e.calc_length()
            v = nv

        return Path(start, distance, tuple(history))

    def reversed(self):
        if not self._reversed:
            history = tuple(
                PathStep(self.history[i-1].vert, self.history[i].edge)
                for i in range(len(self.history)-1, 0, -1)
            ) + (PathStep(self.start, self.history[0].edge), )
            self._reversed = Path(start=self.history[-1].vert,
                                  distance=self.distance,
                                  history=history)
            self._reversed._reversed = self
        return self._reversed

    @property
    def current(self):
        return self.history[-1].vert if len(self.history) > 0 else self.start
    end = current

    @property
    def ends(self):
        return self.start, self.end

    @property
    def last_edge(self):
        return self.history[-1].edge if len(self.history) > 0 else None

    def next_step(self, edge: bmesh.types.BMEdge, dist_func: Callable=None) -> Path:
        if edge not in self.current.link_edges:
            raise Exception("edge is not linked to current vertice")
        if dist_func is None:
            dist_func = lambda branch, e: branch.distance+e.calc_length()

        return Path(
            self.start,
            dist_func(self, edge),
            self.history + (PathStep(edge.other_vert(self.current), edge), )
        )

    def iter_verts(self):
        yield  self.start
        for step in self.history:
            yield step.vert

    def iter_edges(self):
        for step in self.history:
            yield step.edge

    def __iter__(self):
        for step in self.history:
            yield step

    def intersect(self, other : Path) -> bool:
        return bool(set(self.iter_verts()) & set(other.iter_verts()))

    def __lt__(self, other):
        return (self.distance, 0) < (other.distance, 1)

    def __repr__(self):
        return "<Path %s ==> %s;\n\t  %s>" % (self.start, self.end, '\n\t->'.join(e.__repr__() for e in self.iter_edges()))
