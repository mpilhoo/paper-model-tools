
active_element = lambda bm: bm.select_history[-1]

selected_verts = lambda bm: (v for v in bm.verts if v.select)
selected_edges = lambda bm: (e for e in bm.edges if e.select)
selected_faces = lambda bm: (f for f in bm.faces if f.select)
