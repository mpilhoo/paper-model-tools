# <pep8-120 compliant>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

bl_info = {
    "name": "Paper Models Tools",
    "author": "Michał Andrzej Pilch",
    "version": (0, 2),
    "blender": (2, 6, 0),
    "location": "",
    "warning": "",
    "description": "Tools for simplifying meshes to generate paper model parts",
    "category": "Mesh",
    "wiki_url": "",
    "tracker_url": ""
}

import bpy
from .ribbons import RibbonateMeshOperator
#from .radial_cuts import RadialCutsOperator


class PaperModelPreparingView(bpy.types.Panel):
    bl_label = "Preparing"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "Paper Model"

    def draw(self, context):
        layout = self.layout
        #box = layout.box()
        #box.label("Ribs (Skeleton)")
        #box.operator("object.suggest_rib_placement")
        #box.operator("object.create_ribs_operator")
        #box.operator("object.intersect_strut_ribs")
        #box.operator("object.reduce_former")

        box = layout.box()
        box.label("transfrorming parts")
        box.operator("edit_mesh.ribbonate_mesh_operator")
        #box.operator("edit_mesh.radial_cuts")
        #box.operator("object.slice_mesh")
        #box.operator("object.roll_mesh")

        #box = layout.box()
        #box.label("Helpers")
        #box.operator("object.intersect_mesh_plane")
        #box.operator("edit_mesh.string_of_springs")
        #box.operator("edit_mesh.flatten_selection")

        #layout.operator("edit_mesh.special_triangulation")
        #layout.operator("edit_mesh.string_connection")
        #layout.operator("edit_mesh.path_normal")
        #layout.operator("edit_mesh.connect_on_direction")

def register():
    bpy.utils.register_class(RibbonateMeshOperator)
    bpy.utils.register_module(__name__)


def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.utils.unregister_class(RibbonateMeshOperator)


if __name__ == "__main__":
    register()
