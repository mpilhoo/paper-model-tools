# import bpy
# import bmesh
# from mathutils import Vector
# from queue import Queue
#
# from .common.structures import (
#     find_selected_cycles_with_bridge,
#     find_selected_two_paths,
#     MeshStructureException
# )
# from .common.algorithm import (
#     find_shortest_paths,
#     mesh_between_cycles,
#     mesh_inside_borders,
# )
# from .common.types import MeshPart, Path
# from .common.helpers import ensure_lookup, VERTS, EDGES, FACES
#
#
# class RibbonException(Exception):
#     pass
#
#
# class RibbonateMeshProcessor(object):
#     _uv_layer = None
#     _frames = None
#     _bounds = (set(), set())
#     _side_paths = None
#     _bridge = None
#     _source_geom = None
#     _selection = None
#
#     def __init__(self,
#                  context: bpy.types.Context,
#                  bm: bmesh.types.BMesh,
#                  selection: MeshPart,
#                  uv_layer_name: str = None,
#                  closed: bool = None):
#         self.context = context
#         self.bm = bm
#         self.uv_layer_name = uv_layer_name or context.object.data.uv_textures.active.name
#         self.closed = closed
#         self._selection = selection
#         self._seam = set()
#         self._reset_uvs_on_layer()
#
#     def _reset_uvs_on_layer(self):
#         for f in self.bm.faces:
#             for l in f.loops:
#                 l[self.uv_layer].uv = Vector((-1, -1))
#
#     def find_source_geom(self):
#         if self.closed is not False:
#             try:
#                 frame1, frame2, self._bridge = find_selected_cycles_with_bridge(self.selection)
#                 self._frames = (frame1, frame2)
#                 self.closed = True
#                 self._source_geom = mesh_between_cycles(frame1, frame2, self._bridge)
#                 self._side_paths = tuple()
#             except MeshStructureException as e:
#                 if self.closed is True:
#                     raise e
#
#         if self.closed is not True:
#             self._frames = frames = find_selected_two_paths(self.selection)
#             self.closed = False
#             paths = self.connect_frames_with_paths()
#
#             inside = mesh_inside_borders(frames[0]| frames[1] | paths[0] | paths[1])
#
#             # TODO optimize:
#             final_paths = []
#             for path in paths:
#                 if all(end.is_boundary for end in path.ends):
#                     e = path.history[0].edge
#                     faces = set(f for f in path.start.link_faces if f in inside)
#                     while not e.is_boundary:
#                         f = next(f for f in e.link_faces if f not in faces)
#                         faces.add(f)
#                         e = next(ne for ne in f.edges if (path.start in ne.verts and ne is not e))
#
#                     p = Path(path.start).next_step(e)
#                     while p.current not in path.ends:
#                         e = next(nv for nv in p.current.link_edges if (nv.is_boundary and nv is not e))
#                         p = p.next_step(e)
#
#                     final_path = p if p.end is path.end else path
#                     final_path.is_cut = False
#                     final_paths.append(final_path)
#                 else:
#                     conn = bmesh.ops.connect_vert_pair(self.bm, verts=path.ends)
#                     final_path = Path.build(
#                         start=path.start,
#                         edges=conn['edges'],
#                     )
#                     final_path.is_cut = True
#                     final_paths.append(final_path)
#
#             self._source_geom = mesh_inside_borders(frames[0] | frames[1] | final_paths[0] | final_paths[1])
#             self._side_paths = tuple(final_paths)
#
#     def connect_frames_with_paths(self):
#         v1, v2 = tuple(self.frames[0].ends)
#         paths = {}
#         exclude = set()
#         for frame in self.frames:
#             exclude |= frame.edges
#
#         paths.update(find_shortest_paths(set((v1,)), self.frames[1].ends, excluded=exclude))
#         paths.update(find_shortest_paths(set((v2,)), self.frames[1].ends, excluded=exclude))
#
#         def paths_with_none_intersection():
#             paths1 = paths[v1]
#             paths2 = paths[v2]
#
#             for path1 in paths1:
#                 for path2 in paths2:
#                     if not path1.intersect(path2):
#                         return path1, path2
#             raise RibbonException("All paths are crossing")
#
#         return paths_with_none_intersection()
#
#     def mark_seams(self):
#         for e in (self.frames[0].edges | self.frames[1].edges):
#             e.seam = True
#
#         if self.closed is True:
#             for e in self.bridge.iter_edges():
#                 e.seam = True
#                 self._seam.add(e)
#             b1, b2 = (self.ribbon_geometry.map[v] for v in self.bridge.ends)
#             for e in b1.link_edges:
#                 if e.other_vert(b1) is b2:
#                     e.seam = True
#                     break
#
#     def generate_base_geometry(self):
#         predef_conn = [path.ends for path in self.side_paths]
#
#         frame1, frame2 = self.frames
#
#         dupl_dict = bmesh.ops.duplicate(self.bm, geom=list(frame1 | frame2))
#         geometry = MeshPart(
#             mixed=dupl_dict['geom'],
#             map=dupl_dict['vert_map']
#         )
#         geometry.map.update(dupl_dict['edge_map'])
#         ensure_lookup(self.bm, VERTS | EDGES)
#
#         for conn in predef_conn:
#             new_edge = self.bm.edges.new([geometry.map[v] for v in conn])
#             geometry.edges.add(new_edge)
#         ensure_lookup(self.bm, EDGES)
#
#         def on_finish(branch):
#             try:
#                 new_edge = self.bm.edges.new([geometry.map[branch.current], geometry.map[branch.start]])
#                 ensure_lookup(self.bm, EDGES)
#                 geometry.edges.add(new_edge)
#             except Exception as e:
#                 pass
#
#         find_shortest_paths(frame1.verts, frame2.verts, on_finish=on_finish)
#         geometry.faces.update(bmesh.ops.edgenet_fill(self.bm, edges=list(geometry.edges))['faces'])
#         ensure_lookup(self.bm, EDGES | FACES)
#
#         is_frame = lambda f: all(geometry.map.get(e) in frame1 for e in f.edges) or \
#                              all(geometry.map.get(e) in frame2 for e in f.edges)
#
#         frame_faces = [f for f in geometry.faces if is_frame(f)]
#         for f in frame_faces:
#             geometry.faces.remove(f)
#             self.bm.faces.remove(f)
#
#         self.ribbon_geometry = geometry
#         self.triangulate_geometry()
#
#     def triangulate_geometry(self):
#         def dist(v1, v2):
#             if not v1 or not v2:
#                 return float('Inf')
#             return (v1.co-v2.co).length
#
#         to_conn = []
#
#         class NoneVert:
#             def index(self):
#                 return -1
#
#             def __bool__(self):
#                 return False
#
#         class LoopCut:
#             def __init__(self, loop):
#                 self.nloop = loop.link_loop_next
#                 self.ploop = loop
#                 self.pair = None
#
#             def pair_cuts(self, other):
#                 self.pair = other
#                 other.pair = self
#
#             def verts1(self):
#                 return (self.nloop.vert,
#                         self.ploop.link_loop_prev.vert if self.ploop != self.pair.nloop else NoneVert())
#
#             def verts2(self):
#                 return (self.nloop.link_loop_next.vert if self.nloop != self.pair.ploop else NoneVert(),
#                         self.ploop.vert)
#
#             def cut_lenght(self):
#                 return min(dist(*self.verts1()), dist(*self.verts2()))
#
#             def make_cut(self):
#                 if dist(*self.verts1()) < dist(*self.verts2()):
#                     to_conn.append(self.verts1())
#                     self.ploop = self.ploop.link_loop_prev
#                 else:
#                     to_conn.append(self.verts2())
#                     self.nloop = self.nloop.link_loop_next
#
#             def collides(self):
#                 return (self.nloop == self.pair.ploop and self.ploop == self.pair.nloop.link_loop_next) or \
#                     (self.ploop == self.pair.nloop and self.nloop == self.pair.ploop.link_loop_prev)
#
#         set1 = set(self.ribbon_geometry.map[v] for v in self.frames[0].verts)
#         set2 = set(self.ribbon_geometry.map[v] for v in self.frames[1].verts)
#
#         def connects_frames(edge):
#             return any((
#                 edge.verts[0] in set1 and edge.verts[1] in set2,
#                 edge.verts[0] in set2 and edge.verts[1] in set1,
#             ))
#
#         ribbon_faces = set(self.ribbon_geometry.faces)
#
#         for face in ribbon_faces:
#             cut1 = cut2 = None
#             for loop in face.loops:
#                 if connects_frames(loop.edge):
#                     if not cut1:
#                         cut1 = LoopCut(loop)
#                     else:
#                         cut2 = LoopCut(loop)
#             if not cut1 or not cut2:
#                 continue
#             cut1.pair_cuts(cut2)
#             while not cut1.collides():
#                 if cut1.cut_lenght() < cut2.cut_lenght():
#                     cut1.make_cut()
#                 else:
#                     cut2.make_cut()
#
#         for v1, v2 in to_conn:
#             new_edge = bmesh.ops.connect_vert_pair(self.bm, verts=[v1, v2])['edges'][0]
#             self.ribbon_geometry.edges.add(new_edge)
#             ensure_lookup(self.bm, FACES)
#             self.ribbon_geometry.faces.update(f for f in new_edge.link_faces if f not in ribbon_faces)
#
#     def unwrap_form(self):
#         self.context.object.data.uv_textures.active = self.context.object.data.uv_textures[self.uv_layer_name]
#         self.ribbon_geometry.select_set(True)
#         bpy.ops.uv.unwrap()
#         self.ribbon_geometry.select_set(False)
#
#         if self.closed:
#             v1, v2 = tuple(self.ribbon_geometry.map[v] for v in self.bridge.ends)
#             bridge_edge = next(e for e in v1.link_edges if e.other_vert(v1) is v2)
#             self.ribbon_geometry.bridge_edge = bridge_edge
#             for l in bridge_edge.link_loops:
#                 if l.vert is v1:
#                     self.bridge.left_uvs = l[self.uv_layer].uv, l.link_loop_next[self.uv_layer].uv
#                 else:
#                     self.bridge.right_uvs = l.link_loop_next[self.uv_layer].uv, l[self.uv_layer].uv
#
#     def _set_pinned_uv(self, l, uv):
#         l[self.uv_layer].uv = uv
#         l[self.uv_layer].pin_uv = True
#
#     def _calc_line_uv(self, uvs, dist, path):
#         return uvs[1] + (uvs[0] - uvs[1]) * (dist / path.distance)
#
#     def _adjust_bridge_uvs_to_form(self):
#         start, end = self.bridge.ends
#         next_loop_anticlockwise = lambda l: l.link_loop_prev.link_loop_radial_next
#
#         l = next(l for l in start.link_loops if (l.edge in self.frames[0] and l.face in self.source_geom))
#         uv = self.bridge.right_uvs[0]
#         while l.face in self.source_geom:
#             self._set_pinned_uv(l, uv)
#             l = next_loop_anticlockwise(l)
#             if l.edge in self.bridge.iter_edges():
#                 uv = self.bridge.left_uvs[0]
#             if l.edge in self.frames[0]:
#                 break
#
#         l = next(l for l in end.link_loops if (l.edge in self.frames[1] and l.face in self.source_geom))
#         uv = self.bridge.left_uvs[1]
#         while l.face in self.source_geom:
#             self._set_pinned_uv(l, uv)
#             l = next_loop_anticlockwise(l)
#             if l.edge in self.bridge.iter_edges():
#                 uv = self.bridge.right_uvs[1]
#             if l.edge in self.frames[0]:
#                 break
#
#         next_e = self.bridge.history[-1].edge
#         dist = next_e.calc_length()
#
#         bridge = self.bridge.history[-2::-1]
#         for s in bridge:
#             v = s.vert
#             prev_e = s.edge
#             l = next(l for l in v.link_loops if l.edge is prev_e)
#             uv = self._calc_line_uv(self.bridge.right_uvs, dist, self.bridge)
#             while not l[self.uv_layer].pin_uv:
#                 self._set_pinned_uv(l, uv)
#                 l = next_loop_anticlockwise(l)
#                 if l.edge is next_e:
#                     uv = self._calc_line_uv(self.bridge.left_uvs, dist, self.bridge)
#             dist += prev_e.calc_length()
#             next_e = prev_e
#
#     def _adjust_side_paths_to_form(self):
#         for path in self.side_paths:
#             if path.is_cut:
#                 dist = 0.
#                 uvs = tuple(self.ribbon_geometry.map[v].link_loops[0][self.uv_layer].uv for v in path.ends[::-1])
#                 for s in path:
#                     v = s.vert
#                     dist += s.edge.calc_length()
#                     uv = self._calc_line_uv(uvs, dist, path)
#                     for l in v.link_loops:
#                         self._set_pinned_uv(l, uv)
#             else:
#                 path_link_faces = MeshPart(faces=sum((list(s.vert.link_faces) for s in path.history[:-1]), []))
#                 path_link_faces.select_set(True)
#                 bpy.ops.uv.unwrap()
#                 for s in path:
#                     for l in s.vert.link_loops[:-1]:
#                         l[self.uv_layer].pin_uv = True
#
#     def adjust_geometry_to_form(self):
#         if self.closed:
#             self._adjust_bridge_uvs()
#
#         for v in self.ribbon_geometry.verts:
#             uv = v.link_loops[0][self.uv_layer].uv
#             for l in self.ribbon_geometry.map[v].link_loops:
#                 if l[self.uv_layer].pin_uv:
#                     continue
#                 if l.face in self.source_geom:
#                     self._set_pinned_uv(l, uv)
#
#         if self.closed is False:
#             self._adjust_side_paths_to_form()
#
#         self.source_geom.select_set(True)
#         bpy.ops.uv.unwrap()
#         self.source_geom.select_set(False)
#
#     def render_preview_texture(self):
#         # TODO
#         # bmesh.utils.face_split(face, vert_a, vert_b, coords=(), use_exist=True, example=None)
#         pass
#
#     def project_form_cuts_on_original_mesh(self):
#         next_queue = Queue()
#         if self.bridge:
#             start = (self.ribbon_geometry.bridge_edge, self.ribbon_geometry.map[self.bridge.start])
#
#             v = self.bridge.start
#             e = self.bridge.history[0].edge
#             l = next(l for l in e.link_loops if l.vert is v).link_loop_prev.link_loop_radial_prev
#             while l.edge in self.source_geom:
#                 next_queue.put((l.edge, v))
#                 print(f" put ({l.edge}, {v}")
#                 l = l.link_loop_prev.link_loop_radial_prev
#
#             for i in range(len(self.bridge.history) - 1):
#                 stp, nxt = self.bridge.history[i:i+2]
#                 l = next(l for l in stp.edge.link_loops if l.vert is not stp.vert).link_loop_next
#                 while l.edge is not nxt.edge:
#                     next_queue.put((l.edge, stp.vert))
#                     print(f" put ({l.edge}, {stp.vert}")
#                     l = l.link_loop_radial_next.link_loop_next
#
#             v = self.bridge.history[-1].vert
#             e = self.bridge.history[-1].edge
#             l = next(l for l in e.link_loops if l.vert is not v).link_loop_next
#             while l.edge in self.source_geom:
#                 next_queue.put((l.edge, v))
#                 print(f" put ({l.edge}, {v}")
#                 l = l.link_loop_radial_next.link_loop_next
#         else:
#             start = (self.side_paths,)  # TODO
#
#         visited = set()
#         done_faces = 0
#         for rf, re in self.iter_ribbon(*start):
#             tf = TargetRibbonFace(self.bm, rf, re, self.uv_layer)
#             queue = next_queue
#             next_queue = Queue()
#
#             while not queue.empty():
#                 e, v1 = queue.get()
#                 tf.place_vert_on_my_plane(v1)
#
#                 v2 = e.other_vert(v1)
#
#                 if e in visited:
#                     continue
#
#                 print(f"  get: v1: {v1}, e: {e}")
#                 visited.add(e)
#                 visited.add(v1)
#
#                 v1_uv = tf.get_nearest_uv(v1)
#                 v2_uv = tf.get_nearest_uv(v2)
#
#                 cross = tf.cross_with_line(e)
#                 print(f"{cross}, {v1_uv} -- {v2_uv}")
#                 if cross:
#                     if cross == v1_uv:   # FIXME ustalić próg błędu
#                         tf.add_cross_vert(v1)
#                         next_queue.put((e, v1))
#                         print(f" put next ({e}, {v1} cross on v1")
#                     elif cross == v2_uv:
#                         tf.add_cross_vert(v2)
#                         for le in v2.link_edges:
#                             v3 = le.other_vert(v2)
#                             if v3 in self.source_geom and le not in visited and v3 not in tf:
#                                 next_queue.put((le, v2))
#                                 print(f" put next ({le}, {v2} cross on v2")
#                     else:
#                         fac = (cross - v2_uv).length / (v2_uv - v1_uv).length
#                         ne, nv = bmesh.utils.edge_split(e, v2, fac)
#                         self.source_geom.add(nv)
#                         self.source_geom.add(ne)
#                         print(f"divided ne {ne}, nv: {nv}")
#                         tf.add_cross_vert(nv)
#                         next_queue.put((ne, nv))
#                         print(f" put next ({ne}, {nv} new cross")
#                 else:
#                     for le in v2.link_edges:
#                         if le in self.source_geom and le not in visited:
#                             queue.put((le, v2))
#                             print(f" put ({le}, {v2}")
#
#             tf.connect_crosses()
#             print("NEXT")
#             done_faces += 1
#             if done_faces == 18:
#                 break
#
#         if self.side_paths:
#             # TODO
#             pass
#         # TODO #798847
#         # 1. constuct first queue
#         # 2.
#
#     def project_verts_to_form(self):
#         # TODO #798848
#         pass
#
#     @property
#     def uv_layer(self):
#         return self.bm.loops.layers.uv[self.uv_layer_name]
#
#     @property
#     def selection(self):
#         if self._selection is None:
#             self._selection = MeshPart.from_selection(self.bm)
#             self._selection.select_set(False)
#         return self._selection
#
#     @property
#     def frames(self):
#         if not self._frames:
#             self.find_source_geom()
#         return self._frames
#
#     @property
#     def side_paths(self):
#         if not self._side_paths and self.closed is None:
#             self.find_source_geom()
#         return self._side_paths
#
#     @property
#     def source_geom(self):
#         if not self._source_geom:
#             self.find_source_geom()
#         return self._source_geom
#
#     @property
#     def bridge(self):
#         if self.closed is False:
#             raise RibbonException("no bridge in unclosed ribbons")
#         if not self._bridge:
#             self.find_source_geom()
#         return self._bridge
#
#     @staticmethod
#     def iter_ribbon(e, v):
#         l = next(l for l in e.link_loops if l.vert is v)
#         visited = set()
#
#         while True:
#             visited.add(l)
#             l = l.link_loop_next
#
#             while len(l.edge.link_faces) != 2:
#                 l = l.link_loop_next
#                 if l in visited:
#                     raise StopIteration()
#
#             yield (l.face, l.edge)
#
#             l = l.link_loop_radial_next
#             if l in visited:
#                 raise StopIteration()
#
#     def clean(self):
#         self.closed = None
#         self._selection = None
#         self._frames = None
#         self._side_paths = None
#         self._bridge = None
#         self._source_geom = None
#         self.ribbon_geometry = None
#         for e in self._seam:
#             e.seam = False
#         self._seam = set()
#         print("CLEANED")
#         # bmesh.ops.delete(self.bm, geom=[*self.ribbon_geometry], context=values.DEL_ALL)    # TODO uncomment it
#         # self._uv_layer = None
#
#     def process(self):
#         self.generate_base_geometry()
#         self.mark_seams()
#         self.unwrap_form()
#         self.adjust_geometry_to_form()
#         self.project_form_cuts_on_original_mesh()
#         # self.project_verts_to_form()
#         # TODO
#         # self.ribbon_geometry.select_set(True)
#         # self.clean()
#
#
# class RibbonateMeshOperator(bpy.types.Operator):
#     """
#     Transformates segment for ribbons where folds are always
#     connecting different ribs
#     """
#
#     bl_idname = "edit_mesh.ribbonate_mesh_operator"
#     bl_label = "Ribbonate mesh"
#     bl_options = {'REGISTER', 'UNDO'}
#
#     @classmethod
#     def poll(cls, context):
#         return context.mode == 'EDIT_MESH'
#
#     def execute(self, context):
#         bm = bmesh.from_edit_mesh(context.object.data)
#         bm.transform(context.active_object.matrix_world)
#         bm.normal_update()
#
#         uv_layer = context.object.data.uv_textures.new("__ribbon__")
#
#         processor = RibbonateMeshProcessor(context, bm, MeshPart.from_selection(bm), uv_layer.name)
#         processor.process()
#
#         bm.transform(context.active_object.matrix_world.inverted())
#         bmesh.update_edit_mesh(context.active_object.data)
#         # context.object.data.uv_textures.remove(uv_layer)
#
#         return {'FINISHED'}
