import bmesh
import bpy
from helpers import scaled_length


def slice_mesh(bm, dir_v, thickness=None, single_side=False):
    if not thickness:
        thickness = scaled_length(1.125, 'mm')
    dir_v.normalize()
    sorted_verts = sorted([v for v in bm.verts], key=lambda v: (v.co*dir_v, v.index))
    co_min, co_max = sorted_verts[0].co, sorted_verts[-1].co
    d_min, d_max = co_min*dir_v, co_max*dir_v
    slice_count = round((d_max-d_min)/thickness)
    thickness = (d_max-d_min) / slice_count

    next_pl_co = co_min
    it = 0

    slices = []

    for i in range(slice_count):
        pl_co = next_pl_co
        pl_dist = pl_co * dir_v
        next_pl_co = co_min + dir_v*(thickness*(i+1))
        next_pl_dist = next_pl_co * dir_v

        verts = intersect_mesh_with_plane(bm, next_pl_co, dir_v)['verts']
        slices.append((verts, pl_co, pl_dist, next_pl_dist, next_pl_co))

    bm.faces.index_update()

    for s in slices:
        verts, pl_co, pl_dist, next_pl_dist, next_pl_co = s
        reset_tags(bm)
        for v in verts:
            for l in v.link_loops:
                if l.face.calc_center_median() * dir_v > next_pl_dist:
                    bmesh.utils.loop_separate(l)
        for v in verts:
            v.co -= dir_v * thickness

        def move_vert_and_connected(start):
            q = [start]
            start.tag = True
            while len(q):
                v = q.pop(0)
                v.co -= dir_v * dist_pt_pl(v.co, pl_co, dir_v)
                for e in v.link_edges:
                    cv = e.other_vert(v)
                    if cv.tag:
                        continue
                    cv.tag = True
                    q.append(cv)

        while it < len(sorted_verts) and sorted_verts[it].co * dir_v < next_pl_dist:
            v = sorted_verts[it]
            it += 1
            if v.tag:
                continue
            move_vert_and_connected(v)

        bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=0.0001)

    while it < len(sorted_verts):
        v = sorted_verts[it]
        it += 1
        if v.tag:
            continue
        move_vert_and_connected(v)
    bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=0.0001)


class SliceMeshOperator(bpy.types.Operator):
    """ divides mesh into slices """
    bl_idname = "object.slice_mesh"
    bl_label = "Slice Mesh"
    bl_options = {'REGISTER', 'UNDO'}

    thickness = bpy.props.FloatProperty(name="Thickness",
                                        default=1.15,
                                        min=0.1,
                                        max=2.,
                                        step=0.1,
                                        subtype='FACTOR')

    single_side = bpy.props.BoolProperty(name="Single side", default=False)

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "thickness", text="Thickness (mm)")

    def execute(self, context):
        bm = bmesh.new()
        bm.from_mesh(context.active_object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        slice_mesh(bm, context.scene.cursor_location - context.active_object.location,
                   scaled_length(self.thickness, 'mm'),
                   self.single_side)

        bm.transform(context.active_object.matrix_world.inverted())
        bm.to_mesh(context.active_object.data)
        bm.free()
        return {'FINISHED'}
