import bmesh
import bpy


class RollMeshOperator(bpy.types.Operator):
    """ makes mesh rolled """
    bl_idname = "object.roll_mesh"
    bl_label = "Roll Mesh"
    bl_options = {'REGISTER', 'UNDO'}

    thickness = bpy.props.FloatProperty(name="Thickness",
                                        default=0.13,
                                        min=0.05,
                                        step=0.01,
                                        max=1.,
                                        subtype='FACTOR')

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "thickness", text="Thickness (mm)")

    def execute(self, context):
        bm = bmesh.new()
        bm.from_mesh(context.active_object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        collapse_mesh(bm,
                      context.active_object.location,
                      context.scene.cursor_location - context.active_object.location,
                      thickness=scaled_length(self.thickness))

        bm.transform(context.active_object.matrix_world.inverted())
        bm.to_mesh(context.active_object.data)
        bm.free()
        return {'FINISHED'}
