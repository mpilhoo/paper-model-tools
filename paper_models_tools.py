# <pep8-120 compliant>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

bl_info = {
    "name": "Paper Models Tools",
    "author": "Michał Andrzej Pilch",
    "version": (0, 1),
    "blender": (2, 6, 0),
    "location": "",
    "warning": "",
    "description": "Tools for simplifying meshes to generate paper model parts",
    "category": "",
    "wiki_url": "",
    "tracker_url": ""
}

import bpy
import bgl
import blf
import bmesh
import mathutils
import math
import time

from copy import copy
from datetime import datetime
from numpy.linalg import solve
from pprint import pprint
from queue import Queue, PriorityQueue
# from operator import itemgetter, attrgetter
from object_print3d_utils.report import info as volume_info
from mathutils import Vector, Matrix
from mathutils.geometry import normal
from mathutils.geometry import distance_point_to_plane as dist_pt_pl
from mathutils.geometry import intersect_line_plane as insct_ln_pl
from mathutils.geometry import normal as pl_normal
from mathutils.geometry import intersect_plane_plane as insct_pl_pl
from mathutils.geometry import intersect_line_line as insct_ln_ln
from mathutils.geometry import intersect_point_line as insct_pt_ln
from bmesh.geometry import intersect_face_point as insct_fc_pt

# checking version is over 2.72
ENSURE_LOOKUP = (int(bpy.app.version_string.split()[0].split('.')[1]) > 72)

ANALIZIS = False

DEL_VERTS = 1
DEL_EDGES = 2
DEL_ONLYFACES = 3
DEL_EDGESFACES = 4
DEL_FACES = 5
DEL_ALL = 6
DEL_ONLYTAGGED = 7

# commonly used short variable names:
# - v(ertice)
# - e(dge)
# - f(ace)
# - l(oop)
# - pl(ane)
# - no(rmal)
# - co(ordinates)
# - d(istance)
# - p(oint)
# - dir(ection)
# - bm - bmesh


def reset_tags(bm, verts=True, edges=True, faces=True):
    if verts:
        for v in bm.verts:
            v.tag = False
    if edges:
        for e in bm.edges:
            e.tag = False
    if faces:
        for f in bm.faces:
            f.tag = False


active_element = lambda bm: bm.select_history[-1]
selected_verts = lambda bm: (v for v in bm.verts if v.select)
selected_edges = lambda bm: (e for e in bm.edges if e.select)
selected_faces = lambda bm: (f for f in bm.faces if f.select)

unit_multiplier = {
    'm': 1.,
    'cm': 0.01,
    'mm': 0.001
}


def scaled_length(length, unit='mm'):
    return length * unit_multiplier[unit] * bpy.context.scene.paper_model.scale


def co_middle(co1, co2):
    return co1 + (co2-co1) / 2.


def edge_middle(e):
    return co_middle(e.verts[0].co, e.verts[1].co)


def intersect_mesh_with_plane(bm, pl_co, pl_no, side=None, threshold=None):
    if not threshold:
        threshold = scaled_length(.05, 'mm')
    v_dist = {}
    e_set = set()
    v_set = set()
    new_verts = set()
    to_dissolve = []

    def on_side(co):
        if side is None:
            return True
        return (co-pl_co) * side >= -threshold**2

    for v in bm.verts:
        v_dist[v] = dist_pt_pl(v.co, pl_co, pl_no)

    def cut_edges(e):
        v1, v2 = e.verts
        tmp_v_set = set()
        if abs(v_dist[v1]) < threshold:
            tmp_v_set.add(v1)
        if abs(v_dist[v2]) < threshold:
            tmp_v_set.add(v2)
        v_set.update(tmp_v_set)

        if len(tmp_v_set) == 0 and v_dist[v1] * v_dist[v2] < 0.:
            d1, d2 = abs(v_dist[v1]), abs(v_dist[v2])
            ne, nv = bmesh.utils.edge_split(e, v1, d1 / (d1+d2))
            v_dist[nv] = 0.
            v_set.add(nv)
            new_verts.add(nv)

    def check_cross(e):
        v1, v2 = e.verts
        if not(on_side(v1.co) or on_side(v2.co)):
            return False
        if any((
            abs(v_dist[v1]) < threshold,
            abs(v_dist[v2]) < threshold
        )):
            return True
        return all((
            v_dist[v1] * v_dist[v2] < 0.,
            on_side(v1.co + ((v2.co - v1.co) * abs(v_dist[v1] / (abs(v_dist[v1])+abs(v_dist[v2]))))),
        ))

    for f in bm.faces:
        go = False
        for e in f.edges:
            go = check_cross(e) or go
        if go:
            for e in f.edges:
                cut_edges(e)

            sort_dir = pl_normal(f.verts[0].co, f.verts[0].co+f.normal, f.verts[0].co+pl_no)
            cv = []
            i = 0
            l = f.loops[0]
            while i < len(f.loops):
                if l.vert in v_set:
                    cv.append((l.vert.co * sort_dir, l.vert))
                    if v_dist[l.link_loop_next.vert] * v_dist[l.link_loop_prev.vert] > 0.:
                        # plane intersects face only in one point (vertice)
                        cv.append((l.vert.co * sort_dir, l.vert))
                l = l.link_loop_next
                i += 1
            cv.sort(key=lambda v: v[0])
            while len(cv) > 1:
                d, v1 = cv.pop()
                d, v2 = cv.pop()
                is_con = v1 is v2
                for e in v1.link_edges:
                    if e.other_vert(v1) is v2:
                        e_set.add(e)
                        is_con = True
                if not is_con:
                    ne = bmesh.ops.connect_vert_pair(bm, verts=[v1, v2])['edges'][0]
                    e_set.add(ne)

    if ENSURE_LOOKUP:
        bm.edges.ensure_lookup_table()

    if side:
        for e in e_set:
            v1, v2 = e.verts
            if on_side(v1.co) ^ on_side(v2.co):
                v_os, v_ot = (v1, v2) if on_side(v1.co) else (v2, v1)  # vertices OnSide and OTher
                d1, d2 = abs(dist_pt_pl(v_os.co, pl_co, side)), abs(dist_pt_pl(v_ot.co, pl_co, side))
                ne, nv = bmesh.utils.edge_split(e, v_os, d1 / (d1+d2))
                v_dist[nv] = 0.

    if ENSURE_LOOKUP:
        bm.edges.ensure_lookup_table()

    v_set.clear()
    for e in e_set:
        for v in e.verts:
            if on_side(v.co):
                v_set.add(v)

    bmesh.ops.dissolve_verts(bm, verts=to_dissolve)

    return dict(
        verts=list(v_set),
        edges=list(e_set)
    )


def strut_cuts(strut, ribs, thickness, cut_direciton=1.):

    def insct_near(edge, pt):
        d = insct_pt_ln(pt, edge.verts[0].co, edge.verts[1].co)[1]
        return bmesh.utils.edge_split(edge, edge.verts[0], d)[1]

    def comp_zyx(co1, co2, direction=1.):
        d = co2 - co1
        if d.z != 0.:
            return d.z*direction > 0.
        if d.y != 0.:
            return d.y*direction > 0.
        if d.x != 0.:
            return d.x*direction > 0.
        raise Exception()

    if ENSURE_LOOKUP:
        strut.faces.ensure_lookup_table()
    vh = Vector(strut.faces[0].normal)

    for rib in ribs:
        cut_centers = []
        rib_co, rib_no = rib
        mid_cut = intersect_mesh_with_plane(strut, rib_co, rib_no)
        for edge in mid_cut['edges']:
            cut_centers.append(edge_middle(edge))
        cut_centers.sort(key=lambda c: (c*vh, c))
        bmesh.ops.dissolve_edges(strut, edges=mid_cut['edges'])

        cut1 = intersect_mesh_with_plane(strut, rib_co+rib_no*thickness/2., rib_no)
        cut1_edges = sorted(cut1['edges'], key=lambda e: (edge_middle(e)*vh, edge_middle(e)))
        cut2 = intersect_mesh_with_plane(strut, rib_co-rib_no*thickness/2., rib_no)
        cut2_edges = sorted(cut2['edges'], key=lambda e: (edge_middle(e)*vh, edge_middle(e)))

        for edge1, edge2, center in zip(cut1_edges, cut2_edges, cut_centers):
            v1 = insct_near(edge1, center)
            v2 = insct_near(edge2, center)
            new_e = bmesh.ops.connect_vert_pair(strut, verts=[v1, v2])['edges'][0]
            f1, f2 = new_e.link_faces

            comp_res = comp_zyx(f1.calc_center_bounds(), f2.calc_center_bounds(), cut_direciton)
            to_del, to_flip = (f1, f2) if comp_res else (f2, f1)
            to_flip.normal_flip()
            bmesh.ops.delete(strut, geom=[to_del], context=DEL_FACES)


def make_ribs(bm, planes, threshold=None):
    if not threshold:
        threshold = scaled_length(.05, 'mm')
    intersections = []

    for v in bm.verts:
        v.tag = False

    for pl_co, pl_dir in planes:
        intersection = intersect_mesh_with_plane(bm, pl_co, pl_dir, threshold=threshold)
        for v in intersection['verts']:
            v.tag = True
        intersections.append(intersection)

    bmesh.ops.delete(bm, geom=[v for v in bm.verts if not v.tag], context=DEL_VERTS)

    for intersection in intersections:
        bmesh.ops.contextual_create(bm, geom=[v for v in intersection['verts']]+[e for e in intersection['edges']])


def points_are_collinear(p1, p2, p3, threshold):
    if not threshold:
        threshold = scaled_length(.02, 'mm')
    return 1. - abs((p2 - p1).normalized() * (p3-p1).normalized()) < threshold


def points_in_list_are_collinear(pl, threshold=None):
    for p in pl[2:]:
        if not points_are_collinear(pl[0], pl[1], p, threshold):
            return False
    return True


def points_in_a_plane(pl, threshold=None):
    if not threshold:
        threshold = scaled_length(.02, 'mm')
    p0 = (pl[1]-pl[0])
    no = p0.cross(pl[2]-pl[0]).normalized()
    for p in pl[2:]:
        if p0.cross(p-pl[0]).normalized() - no > threshold:
            return False
    return True


def vertex_collapse(v, dir_v):
    cp_no = pl_normal(v.co, v.co+v.normal, v.co+dir_v)   # cut plane normal
    av = []
    for l in v.link_loops:
        dist_prev = dist_pt_pl(l.link_loop_prev.vert.co, v.co, cp_no)
        dist_next = dist_pt_pl(l.link_loop_next.vert.co, v.co, cp_no)

        if dist_prev == 0. and not l.link_loop_prev.vert.tag:
            av.append(l.link_loop_prev.vert.co)
            l.link_loop_prev.vert.tag = True
        elif dist_next == 0. and not l.link_loop_next.vert.tag:
            av.append(l.link_loop_next.vert.co)
            l.link_loop_next.vert.tag = True
        elif dist_prev * dist_next < 0.:
            line = insct_pl_pl(v.co, l.face.normal, v.co, cp_no)
            lh = l.link_loop_prev
            while points_are_collinear(lh.vert.co, v.co, l.link_loop_next.vert.co):
                lh = lh.link_loop_prev
            pts = insct_ln_ln(line[0], line[0]+line[1], lh.vert.co, l.link_loop_next.vert.co)
            if pts is not None:
                av.append(pts[0])
            else:
                av.append(insct_pt_ln(lh.vert.co, v.co, l.link_loop_next.vert.co)[0])

    for l in v.link_loops:
        l.link_loop_prev.vert.tag = False

    if len(av) < 2:
        return 0.
    v1 = av.pop()
    v2 = av.pop()
    return abs(math.sin((v1-v.co).angle(v2-v.co)))


def find_surface_collapse(obj, dir_v, max_deviation, threshold=0.1):
    dir_v.normalize()

    bm = bmesh.new()
    bm.from_mesh(obj.data)
    bm.transform(obj.matrix_world)
    bm.normal_update()

    verts = sorted([v for v in bm.verts], key=lambda v: (v.co * dir_v, v.index))
    prev_front = [-1] * len(verts)
    prev_back = [-1] * len(verts)
    first_cuts = []
    cut_indices = []

    if ENSURE_LOOKUP:
        bm.verts.ensure_lookup_table()

    i = 0
    while i < len(verts):
        if vertex_collapse(verts[i], dir_v) < max_deviation:
            i += 1
            continue
        j = k = i
        while j < len(verts) and (verts[j].co*dir_v - verts[i].co*dir_v) < 2.*threshold:
            if vertex_collapse(verts[j], dir_v) > max_deviation:
                k = j
            j += 1
        if i != j:
            first_cuts.append(verts[i].co*dir_v + (verts[k].co*dir_v - verts[i].co*dir_v) / 2.)
            cut_indices.append((i, j-1))
        i = j

    if first_cuts[0] - verts[0].co*dir_v > threshold:
        first_cuts.insert(0, verts[0].co*dir_v)
        i = 0
        while verts[i].co * dir_v < threshold:
            i += 1
        cut_indices.insert(0, (0, i-1))
    if first_cuts[-1]+threshold < verts[-1].co*dir_v:
        first_cuts.append(verts[-1].co*dir_v)
        i = len(verts)-1
        while first_cuts[-1] - verts[i].co*dir_v < threshold:
            i -= 1
        cut_indices.append((i+1, len(verts)-1))

    def has_connections_back(v, dir_v):
        for e in v.link_edges:
            vc = e.other_vert(v)
            if (vc.co - v.co) * dir_v < -.1:
                return True
        return False

    for v in verts:
        v.tag = False

    q = PriorityQueue()
    i = 0
    while not has_connections_back(verts[i], dir_v):
        q.put((0., verts[i].index, -1))
        i += 1

    while not q.empty():
        dist, v_i, prev = q.get()
        v = bm.verts[v_i]
        if v.tag:
            continue
        v.tag = True
        prev_front[v_i] = prev
        for e in v.link_edges:
            vc = e.other_vert(v)
            if not vc.tag:
                q.put((dist+e.calc_length(), vc.index, v_i))

    for v in verts:
        v.tag = False

    q = PriorityQueue()
    i = len(verts)-1
    while not has_connections_back(verts[i], -dir_v):
        q.put((0., verts[i].index, -1))
        i -= 1

    while not q.empty():
        dist, v_i, prev = q.get()
        v = bm.verts[v_i]
        if v.tag:
            continue
        v.tag = True
        prev_back[v_i] = prev
        for e in v.link_edges:
            vc = e.other_vert(v)
            if not vc.tag:
                q.put((dist+e.calc_length(), vc.index, v_i))

    second_cuts = []
    cut_i = 0

    while cut_i < len(first_cuts)-1:
        front_cuts = []
        back_cuts = []

        f = cut_indices[cut_i][1]+1
        v_f = verts[f]
        dev_front = {}

        b = cut_indices[cut_i+1][0]-1
        v_b = verts[b]
        dev_back = {}

        while f < b:
            if v_f.co*dir_v - first_cuts[cut_i] < first_cuts[cut_i+1] - v_b.co*dir_v:
                dev_front[v_f.index] = vertex_collapse(v_f, dir_v)
                if prev_front[v_f.index] in dev_front:
                    dev_front[v_f.index] += dev_front[prev_front[v_f.index]]
                if dev_front[v_f.index] > max_deviation:
                    front_cuts.append(v_f.co * dir_v)
                    dev_front = {}
                f += 1
                v_f = verts[f]
            else:
                dev_back[v_b.index] = vertex_collapse(v_b, dir_v)
                if prev_back[v_b.index] in dev_back:
                    dev_back[v_b.index] += dev_back[prev_back[v_b.index]]
                if dev_back[v_b.index] > max_deviation:
                    back_cuts.append(v_b.co * dir_v)
                    dev_back = {}
                b -= 1
                v_b = verts[b]

        if len(front_cuts) == 0:
            second_cuts += back_cuts
        elif len(back_cuts) == 0:
            second_cuts += front_cuts
        else:
            average = (first_cuts[cut_i+1] - first_cuts[cut_i]) / (len(back_cuts) + len(front_cuts) + 1)
            diff = (average - (back_cuts[-1] - front_cuts[-1])) / (len(back_cuts) + len(front_cuts))

            for fc in front_cuts:
                second_cuts.append(fc - diff)
            for bc in back_cuts:
                second_cuts.append(bc + diff)

        cut_i += 1

    return sorted(first_cuts+second_cuts)


def choose_shorter(seq1, seq2):
    s1_len = s2_len = 0.
    for e in seq1:
        s1_len += e.calc_length()
    for e in seq2:
        s2_len += e.calc_length()

    return (seq1, seq2) if s1_len < s2_len else (seq2, seq1)


def dijkstra(bm, begin_set, end_set,
             on_each=None, on_finish=None):
    q = PriorityQueue()
    for v in begin_set:
        q.put((0., v.index, -1, v.index))

    start_v = {}
    prev_v = {}

    finished = 0
    while not q.empty() and finished < len(end_set):
        dist, v_i, prev_i, sv_i = q.get()
        v = bm.verts[v_i]
        if v.tag:
            continue
        v.tag = True
        sv = bm.verts[sv_i]
        start_v[v] = sv
        prev = None
        if prev_i >= 0:
            prev = bm.verts[prev_i]
        prev_v[v] = prev

        if on_each:
            on_each(v, sv, prev, dist)
        if v in end_set:
            finished += 1
            if on_finish:
                on_finish(v, sv, prev, dist)
            continue

        for e in v.link_edges:
            vc = e.other_vert(v)
            q.put((dist+e.calc_length(), vc.index, v.index, sv_i))
    return start_v, prev_v


def find_frame_ends(frame):
    """searching for extreme vertices in frames if they are not loops"""
    for v in frame['verts']:
        v.tag = False
    v = next(iter(frame['verts']))
    frame_edges = set(frame['edges'])
    ends = []
    for e in v.link_edges:
        if e in frame_edges:
            ends.append(e.other_vert(v))
    end1 = ends[0] if ends else v
    end2 = ends[1] if len(ends) > 1 else v
    v.tag = end1.tag = end2.tag = True

    def find_end(v):
        prev = None
        while v != prev:
            prev = v
            for e in v.link_edges:
                other = e.other_vert(v)
                if e in frame_edges and not other.tag:
                    v = other
                    v.tag = True
                    break
        return v

    end1 = find_end(end1)
    end2 = find_end(end2)

    if len(frame['verts']) > 2:
        for e in end1.link_edges:
            if e.other_vert(end1) is end2:
                return None, None   # frame is looped

    for v in frame['verts']:
        v.tag = False
    return end1, end2


def signed_angle_3d(v1, v2, no):
    return v1.angle(v2) if v1.cross(v2) * no >= 0. else 2.*math.pi-v1.angle(v2)


def edge_signed_angle(e1, e2, v):
    return signed_angle_3d(e1.other_vert(v).co-v.co, e2.other_vert(v).co-v.co, v.normal)


def radial_sorted_edges(vert, start_from=None):
    wire_edges = [e for e in vert.link_edges if e.is_wire]
    opening_edges = set([l.edge for l in vert.link_loops if l.edge.is_boundary]) | set(wire_edges)
    closing_edges = [e for e in vert.link_edges if e.is_boundary and e not in opening_edges] + wire_edges

    class FakeLoop:
        def __init__(self, edge, angle):
            self.edge = edge
            self.angle = angle

        def index(self):
            return -1

        def calc_angle(self):
            return self.angle

    def find_closing_egde(edge):
        min_angle = -100.
        next_edge = None
        for e in closing_edges:
            angle = edge_signed_angle(edge, e, vert)
            if angle > min_angle:
                min_angle = angle
                next_edge = e
        return next_edge, 2*math.pi-min_angle

    if not start_from:
        start_from = vert.link_edges[0]
    l = FakeLoop(*find_closing_egde(start_from)) \
        if start_from in closing_edges \
        else [l for l in start_from.link_loops if l.vert == vert][0]

    loops = []
    condition = True
    while condition:
        loops.append(l)
        if l.edge in opening_edges:
            l = FakeLoop(*find_closing_egde(l.edge))
        elif l.edge.is_boundary:
            l = l.edge.link_loops[0].link_loop_next
        else:
            l = l.link_loop_radial_next.link_loop_next
        condition = (l.edge != start_from)
    return [(l.edge, l.calc_angle()) for l in loops]


def side_division(vert, prev_edge, next_edge):
    sorted_edges = radial_sorted_edges(vert, next_edge)
    edges = [se[0] for se in sorted_edges]
    angles = [se[1] for se in sorted_edges]
    left_edges, right_edges = edges[edges.index(prev_edge)+1:], edges[1:edges.index(prev_edge)]
    div_index = edges.index(prev_edge)
    angles.append(angles.pop(0))
    right_angle = sum([a for a in angles[:div_index]])
    left_angle = sum([a for a in angles[div_index:]])
    return {
        'right': {'angle': right_angle, 'edges': right_edges},
        'left': {'angle': left_angle, 'edges': left_edges}
    }


def find_mid_vert(end1, end2, edges):
    v1, v2 = end1, end2
    dist1 = dist2 = 0.

    def next_edge(v):
        for e in v.link_edges:
            if e in edges and not e.tag:
                return e
        return None

    e1, e2 = next_edge(v1), next_edge(v2)
    while v1 != v2:
        if dist1+e1.calc_length() < dist2+e2.calc_length():
            dist1 += e1.calc_length()
            e1.tag = True
            v1 = e1.other_vert(v1)
            e1 = next_edge(v1)
        else:
            dist2 += e2.calc_length()
            e2.tag = True
            v2 = e2.other_vert(v2)
            e2 = next_edge(v2)
    return v1


def bfs_select(queue, border_verts, border_edges, select=False):
    selected = {'verts': set(), 'edges': set(), 'faces': set()}
    selected['verts'].update(queue)
    selected['verts'].update(border_verts)

    for v in border_verts:
        for e in v.link_edges:
            if e.other_vert in border_verts and e not in border_edges:
                selected['edges'].add(e)
                for f in e.link_faces:
                    selected['faces'].add(f)

    while len(queue):
        v = queue.pop(0)
        selected['faces'].update(set([f for f in v.link_faces]))
        for e in v.link_edges:
            selected['edges'].add(e)
            vc = e.other_vert(v)
            if vc in selected['verts']:
                continue
            selected['verts'].add(vc)
            queue.append(vc)

    if select:
        for v in selected['verts']:
            v.select_set(True)
        for e in selected['edges']:
            e.select_set(True)
        for f in selected['faces']:
            f.select_set(True)

    return selected


def bm_inside(bm, border_edges):
    right_sum = left_sum = 0.
    right_verts, left_verts = set(), set()
    border_verts = []
    v = border_edges[0].verts[0]
    he = []
    for e in v.link_edges:
        if e in border_edges:
            he.append(e)
    if len(he) != 2:
        raise Exception("BM_INSIDE: INVALID-BORDER -- %s" % [e.index for e in border_edges])
    prev_edge, next_edge = he
    start_v = v

    while True:
        border_verts.append(v)
        sd = side_division(v, prev_edge, next_edge)
        left_sum += sd['left']['angle']
        left_verts.update([e.other_vert(v) for e in sd['left']['edges']])

        right_sum += sd['right']['angle']
        right_verts.update([e.other_vert(v) for e in sd['right']['edges']])

        v = next_edge.other_vert(v)
        prev_edge = next_edge
        for e in v.link_edges:
            if e in border_edges and e != prev_edge:
                next_edge = e
                break
        if prev_edge == next_edge:
            raise Exception("BM_INSIDE: INVALID-BORDER -- %s" % [e.index for e in border_edges])
        if v == start_v:
            break

    right_verts.difference_update(border_verts)
    left_verts.difference_update(border_verts)
    return bfs_select(list(right_verts) if right_sum < left_sum else list(left_verts), border_verts, border_edges)


def pull_string_single(end1, end2, co):
    return end1 + (end2 - end1)*((co-end1).length / ((co-end1).length + (co-end2).length))


def string_of_springs(bm, edges, separetly=False, calc_only=False):
    vc = vs = None
    edges_set = set(edges)

    for e in edges:
        for v in e.verts:
            if len([le for le in v.link_edges if le in edges_set]) == 1:
                vc = v
                break
        if vc:
            break

    sorted_edges = []
    visited = set()
    vs = vc
    vp = None
    len_sum = 0.
    len_sums = []
    while vp != vc:
        vp = vc
        visited.add(vc)
        for e in vc.link_edges:
            ov = e.other_vert(vc)
            if e in edges_set and ov not in visited:
                sorted_edges.append(e)
                len_sum += e.calc_length()
                len_sums.append(len_sum)
                vc = e.other_vert(vc)
                break
    curr_len_sum = sorted_edges[0].calc_length()
    ve = vc
    vec = ve.co - vs.co
    vc = sorted_edges[0].other_vert(vs)
    i = 1
    dest_co = {}
    while vc is not ve:
        dest_co[vc] = vs.co + (len_sums[i-1]/len_sum)*vec \
            if not separetly else pull_string_single(vs.co, ve.co, vc.co)
        vc = sorted_edges[i].other_vert(vc)
        i += 1
    if not calc_only:
        for v, co in dest_co.items():
            v.co = co
    return dest_co


def ctg(angle):
    return -math.tan(angle+math.pi/2.)


def wachspress_coordinates(xi, xj, xl, xr):
    return (ctg((xi-xj).angle(xl-xj)) + ctg((xi-xj).angle(xr-xj))) / ((xj-xi).length)**2


def discrete_harmonic_coordinates(xi, xj, xl, xr):
    return ctg((xi-xr).angle(xj-xr)) + ctg((xi-xl).angle(xj-xl))


def mean_value_coordinates(xi, xj, xl, xr):
    return (math.tan((xj-xi).angle(xr-xi)/2.) + math.tan((xj-xi).angle(xl-xi)/2.)) / (xj-xi).length


coordinate_methods = {
    'WACHSPRESS': wachspress_coordinates,
    'DISCRETE_HARMONIC': discrete_harmonic_coordinates,
    'MEAN_VALUE': mean_value_coordinates
}


class ConnectionMap(dict):
    def _rev_key(self, key):
        return (key[1], key[0])

    def __contains__(self, item):
        return any((
            super().__contains__(item),
            super().__contains__(self._rev_key(item))
        ))

    def __getitem__(self, key):
        if super().__contains__(key):
            return super().__getitem__(key)
        if super().__contains__(self._rev_key(key)):
            return super().__getitem__(self._rev_key(key))[::-1]    # reversed value for reverse key
        raise KeyError(key)


def bipartite_traingulation(bm, set1, set2, faces):
    def dist(v1, v2):
        if not v1 or not v2:
            return float('Inf')
        return (v1.co-v2.co).length

    to_conn = []

    class NoneVert:
        def index(self):
            return -1

        def __bool__(self):
            return False

    class LoopCut:
        def __init__(self, loop):
            self.nloop = loop.link_loop_next
            self.ploop = loop
            self.pair = None

        def pair_cuts(self, other):
            self.pair = other
            other.pair = self

        def verts1(self):
            return (self.nloop.vert,
                    self.ploop.link_loop_prev.vert if self.ploop != self.pair.nloop else NoneVert())

        def verts2(self):
            return (self.nloop.link_loop_next.vert if self.nloop != self.pair.ploop else NoneVert(),
                    self.ploop.vert)

        def cut_lenght(self):
            return min(dist(*self.verts1()), dist(*self.verts2()))

        def make_cut(self):
            if dist(*self.verts1()) < dist(*self.verts2()):
                to_conn.append(self.verts1())
                self.ploop = self.ploop.link_loop_prev
            else:
                to_conn.append(self.verts2())
                self.nloop = self.nloop.link_loop_next

        def collides(self):
            return (self.nloop == self.pair.ploop and self.ploop == self.pair.nloop.link_loop_next) or \
                   (self.ploop == self.pair.nloop and self.nloop == self.pair.ploop.link_loop_prev)

    added_faces = set()
    added_edges = set()

    def connects_frames(edge):
        return any((
            edge.verts[0] in set1 and edge.verts[1] in set2,
            edge.verts[0] in set2 and edge.verts[1] in set1,
        ))

    for face in faces:
        cut1 = cut2 = None
        for loop in face.loops:
            if connects_frames(loop.edge):
                if not cut1:
                    cut1 = LoopCut(loop)
                else:
                    cut2 = LoopCut(loop)
        if not cut1 or not cut2:
            continue
        cut1.pair_cuts(cut2)
        while not cut1.collides():
            if cut1.cut_lenght() < cut2.cut_lenght():
                cut1.make_cut()
            else:
                cut2.make_cut()

    for v1, v2 in to_conn:
        new_conn = bmesh.ops.connect_vert_pair(bm, verts=[v1, v2])
        added_edges.update(new_conn['edges'])
        if ENSURE_LOOKUP:
            bm.faces.ensure_lookup_table()
        added_faces.add(bm.faces[-1])

    return {
        'faces': added_faces,
        'edges': added_edges
    }


def triangulate_without_border_connections(bm, faces, boundary_verts=None):
    if not boundary_verts:
        boundary_verts = tuple(
            v
            for v in bm.verts
            if (any(((f not in faces) for f in v.link_faces)) or v.is_boundary)
        )
    connected_with_boundary = set()
    for bv in boundary_verts:
        for v in (e.other_vert(bv) for e in bv.link_edges):
            if v not in boundary_verts and all((f in faces) for f in v.link_faces):
                connected_with_boundary.add(v)

    new_elems = bmesh.ops.triangulate(bm, faces=faces)
    to_dissolve = []
    for e in new_elems['edges']:
        if all((v in boundary_verts) for v in e.verts):
            new_elems['edges'].remove(e)
            to_dissolve.append(e)
    bmesh.ops.dissolve_edges(bm, edges=to_dissolve)

    dead_faces = []
    for f in new_elems['faces']:
        if not f.is_valid:
            dead_faces.append(f)
    for df in dead_faces:
        new_elems['faces'].remove(df)
        del new_elems['face_map'][df]

    face_set = set(new_elems['faces'])

    for v in connected_with_boundary:
        for f in v.link_faces:
            if len(f.verts) > 3:
                to_conn = tuple(bv for bv in f.verts if bv in boundary_verts)
                for bv in to_conn:
                    new_edge = bmesh.ops.connect_vert_pair(bm, verts=[v, bv])['edges'][0]
                    new_elems['edges'].append(new_edge)
                    for f in new_edge.link_faces:
                        if f not in face_set:
                            face_set.add(f)
                            new_elems['faces'].append(f)

    return new_elems


def plane_barycentric_arrangement(bm, verts,
                                  boundary_verts,
                                  connections=None,   # ConnectionMap
                                  coord_method='MEAN_VALUE',
                                  calc_only=False):

    if not points_in_a_plane(tuple(co for v, co in boundary_verts.items())):
        raise Exception("boundary verts are not in same plane")

    if not connections:
        connections = ConnectionMap()
        for v in verts:
            for e in v.link_edges:
                if (len(e.link_loops) < 2) or (e.other_vert(v) not in verts):
                    continue
                loops = (e.link_loops[0], e.link_loops[1])
                ll, lr = loops if loops[0].vert == v else reversed(loops)
                connections[(v, e.other_vert(v))] = (ll.link_loop_prev.vert, lr.link_loop_prev.vert)

    interior_verts = tuple(v for v in verts if v not in boundary_verts)

    w = coordinate_methods[coord_method]

    connected_verts = {v: set() for v in verts}
    for v1, v2 in connections.keys():
        connected_verts[v1].add(v2)
        connected_verts[v2].add(v1)

    def get_co(vi, vj):
        vl, vr = connections[(vi, vj)]
        return (vi.co, vj.co, vl.co, vr.co)

    def lamb(vi, vj):
        suma = sum([w(*get_co(vi, vc)) for vc in connected_verts[vi]])
        w_ij = w(*get_co(vi, vj))
        return w_ij / suma

    def a(vi, vj):
        if vi == vj:
            return 1.
        if (vi, vj) in connections:
            return -lamb(vi, vj)
        return 0.

    dest_co = {}
    if interior_verts:
        A = [[a(vi, vj) for vj in interior_verts] for vi in interior_verts]
        Bx, By, Bz = [], [], []
        for vi in interior_verts:
            sum_x = sum_y = sum_z = 0.
            for vj in connected_verts[vi]:
                if vj in boundary_verts:
                    l = lamb(vi, vj)
                    vj_co = boundary_verts[vj]
                    sum_x += l*vj_co.x
                    sum_y += l*vj_co.y
                    sum_z += l*vj_co.z
            Bx.append(sum_x)
            By.append(sum_y)
            Bz.append(sum_z)
        x, y, z = solve(A, Bx), solve(A, By), solve(A, Bz)

        for v, co in zip(interior_verts, zip(x, y, z)):
            dest_co[v] = Vector(co)

    dest_co.update(boundary_verts)
    if not calc_only:
        for v, co in dest_co.items():
            v.co = co

    return dest_co


def edge_normal(e):
    return sum(
        (f.normal for f in e.link_faces),
        Vector((0, 0, 0))
    ).normalized()


def connect_vert_pair_on_direction(bm, v1, v2, dir_v, threshold=None):
    if not threshold:
        threshold = scaled_length(.05, 'mm')
    cut_plane_no = pl_normal(v1.co, v1.co+dir_v, v2.co)
    search_dir = v2.co-v1.co

    q = [v1, ]
    visited = {v1, v2}

    def lays_between(co):
        return ((co - v1.co) * search_dir) * ((co - v2.co) * search_dir) < 0.

    def face_crosses(f):
        crosses = []
        v_dist = []
        start = l = f.loops[0]

        v_dist.append(dist_pt_pl(l.link_loop_prev.vert.co, v1.co, cut_plane_no))
        if abs(v_dist[0]) < threshold and lays_between(l.link_loop_prev.vert.co):
            crosses.append(l.link_loop_prev.vert)

        while True:
            v_dist.append(dist_pt_pl(l.vert.co, v1.co, cut_plane_no))

            if abs(v_dist[-1]) < threshold and lays_between(l.vert.co):
                crosses.append(l.vert)
            elif v_dist[-1] * v_dist[-2] < 0.:
                edge_dir = l.link_loop_prev.vert.co - l.vert.co
                d1, d2 = abs(v_dist[-1]), abs(v_dist[-2])
                if lays_between(l.vert.co + edge_dir*(d1 / (d1+d2))):
                    crosses.append(l.link_loop_prev.edge)

            l = l.link_loop_next
            if l == start:
                break

        return crosses

    verts_on_plane = {v1, v2}

    while len(q):
        v = q.pop(0)
        visited.add(v)
        for f in v.link_faces:
            if f in visited:
                continue
            crosses = face_crosses(f)
            visited.add(f)
            for cross in crosses:
                cv = None
                if type(cross) is bmesh.types.BMEdge:
                    d1, d2 = tuple(abs(dist_pt_pl(v.co, v1.co, cut_plane_no)) for v in cross.verts)
                    if d1*d2 > threshold:
                        ne, cv = bmesh.utils.edge_split(cross, cross.verts[0], d1 / (d1+d2))
                else:
                    cv = cross
                if not cv:
                    continue
                if cv not in visited:
                    q.append(cv)
                    verts_on_plane.add(cv)

    verts_on_plane = [((v.co - v1.co) * search_dir, i, v) for i, v in enumerate(verts_on_plane)]
    verts_on_plane.sort()

    tv1 = verts_on_plane.pop(0)[2]
    edges_on_path = []
    while verts_on_plane:
        tv2 = verts_on_plane.pop(0)[2]
        alredy_connected = False
        for e in tv1.link_edges:
            if e.other_vert(tv1) is tv2:
                edges_on_path.append(e)
                alredy_connected = True
        if not alredy_connected:
            edges_on_path.extend(bmesh.ops.connect_vert_pair(bm, verts=[tv1, tv2])['edges'])
        tv1 = tv2

    return edges_on_path


def connect_vert_pair_on_simplification_projection(bm, v1, v2, v_map):
    pe = next(
        e
        for e in v_map[v1].link_edges
        if e.other_vert(v_map[v1]) == v_map[v2]
    )
    pe_no = edge_normal(pe)
    return connect_vert_pair_on_direction(bm, v1, v2, pe_no)


def ribbonate_mesh(bm, frame1, frame2,
                   select_inside=True,
                   edge_projection_method='STRING_TOGETHER',
                   face_projection_method='BARYCENTRIC',
                   barycentric_coord_method='MEAN_VALUE'):

    dupl_dict = bmesh.ops.duplicate(bm, geom=frame1['verts']+frame2['verts']+frame1['edges']+frame2['edges'])
    if ENSURE_LOOKUP:
        bm.verts.ensure_lookup_table()
    v_map = dupl_dict['vert_map']
    e_map = {edge: [mapped] for edge, mapped in dupl_dict['edge_map'].items()}
    frame1_set, frame2_set = set(frame1['verts']), set(frame2['verts'])
    border_edges = set([e for e in frame1['edges']]) | set([e for e in frame2['edges']])

    start_frame, finish_frame = choose_shorter(frame1['edges'], frame2['edges'])
    if start_frame is frame1['edges']:
        start_set, end_set = set(frame1['verts']), frame2['verts']
    else:
        start_set, end_set = set(frame2['verts']), frame1['verts']

    mapped_frame1, mapped_frame2 = set(v_map[v] for v in frame1_set), set(v_map[v] for v in frame2_set)
    fr1_end1, fr1_end2 = find_frame_ends(frame1)
    fr2_end1, fr2_end2 = find_frame_ends(frame2)

    # connecting extreme vertices of frames to create one loop if frames are not closed loops already
    simplified_edges = []

    # find extreme connection edges
    if fr1_end1 and fr2_end1:
        def find_paths(v, ends):
            paths = []
            start_v, prev_v = dijkstra(bm, [v], ends)
            for ev in ends:
                path = []
                vi = ev
                while vi is not v:
                    path.append(vi)
                    vi = prev_v[vi]
                path.append(v)
                paths.append(path)
            for v in bm.verts:
                v.tag = False
            return paths

        paths1 = find_paths(fr1_end1, [fr2_end1, fr2_end2])
        paths2 = find_paths(fr1_end2, [fr2_end1, fr2_end2])

        if not (set(paths1[0]) & set(paths2[1])):     # paths are not crossing
            conn = ((fr1_end1, fr2_end1), (fr1_end2, fr2_end2))
        else:
            conn = ((fr1_end1, fr2_end2), (fr1_end2, fr2_end1))

        for c in conn:
            new_edge = bm.edges.new([v_map[c[0]], v_map[c[1]]])
            simplified_edges.append(new_edge)
            extreme_edges = bmesh.ops.connect_vert_pair(bm, verts=[c[0], c[1]])['edges']
            e_map[new_edge] = extreme_edges
            direct_edge = next((e for e in extreme_edges if all(((v in e.verts) for v in c))), None)
            if len(extreme_edges) > 1 and direct_edge:
                extreme_edges.remove(direct_edge)
            for e in extreme_edges:
                border_edges.add(e)
            if c[0] in start_set:
                start_set.remove(c[0])
        if ENSURE_LOOKUP:
            bm.verts.ensure_lookup_table()
            bm.edges.ensure_lookup_table()

    reset_tags(bm, verts=True, edges=False, faces=False)
    for e in frame1['edges']:
        e.tag = False
    for e in frame2['edges']:
        e.tag = False

    traveled = set()

    # finding shortest, non crossing connections from one frame to another
    # and making new edges on duplicate mesh to make new form
    def on_each(v, sv, prev, dist):
        traveled.add(prev)

    def on_finish(v, sv, prev, dist):
        traveled.add(v)
        try:
            new_edge = bm.edges.new([v_map[v], v_map[sv]])
            if ENSURE_LOOKUP:
                bm.edges.ensure_lookup_table()
            simplified_edges.append(new_edge)
        except:
            pass
        if sv in start_set:
            start_set.remove(sv)

    start_v, prev_v = dijkstra(bm, start_set, end_set, on_each=on_each, on_finish=on_finish)

    for v in bm.verts:
        v.tag = False

    edges = [e for e in dupl_dict['geom'] if type(e) is bmesh.types.BMEdge]
    edges.extend(simplified_edges)

    simplified_faces = bmesh.ops.edgenet_fill(bm, edges=edges)['faces']

    if ENSURE_LOOKUP:
        bm.faces.ensure_lookup_table()
        
    # removing faces that are not in the ribbon
    frame_faces = []
    for face in simplified_faces:
        if all(v in mapped_frame1 for v in face.verts) or all(v in mapped_frame2 for v in face.verts):
            frame_faces.append(face)

    for face in frame_faces:
        simplified_faces.remove(face)
        bm.faces.remove(face)

    if ENSURE_LOOKUP:
        bm.faces.ensure_lookup_table()

    # form triangulation - connecting only vertices from opposite frames
    bt = bipartite_traingulation(bm, mapped_frame1, mapped_frame2, simplified_faces)
    simplified_edges.extend(bt['edges'])
    simplified_faces.extend(bt['faces'])

    # cutting simplified edges in original mesh
    div_i = 2 if fr1_end1 and fr2_end1 else 0

    dest_co = {
        v: v.co
        for v in frame1_set.union(frame2_set)
    }

    for se in simplified_edges[div_i:]:
        sv1, sv2 = se.verts
        to_map_edges = connect_vert_pair_on_simplification_projection(bm, v_map[sv1], v_map[sv2], v_map)
        e_map[se] = to_map_edges
        dest_co.update(
            string_of_springs(
                bm,
                to_map_edges,
                separetly=(edge_projection_method == 'STRING_SEPARATELY'),
                calc_only=True
            )
        )

    if ENSURE_LOOKUP:
        bm.verts.ensure_lookup_table()
        bm.edges.ensure_lookup_table()

    inside = {}

    if fr1_end1 and fr2_end1:
        inside.update(bm_inside(bm, list(border_edges)))
    else:
        def select_between_frames(v, sv, prev, dist):
            inside.update(bfs_select([prev], frame1_set.union(frame2_set), border_edges, select=select_inside))
        dijkstra(bm, frame1_set, {frame2['verts'][0]}, on_finish=select_between_frames)

    if face_projection_method == 'BARYCENTRIC':
        for f in simplified_faces:
            mapped_edges = [e.index for e in (sum((e_map[e] for e in f.edges), []))]
            face_inside = bm_inside(bm, sum((e_map[e] for e in f.edges), []))
            new_elems = triangulate_without_border_connections(bm, faces=list(face_inside['faces']))
            inside['faces'].union(new_elems['faces'])
            inside['edges'].union(new_elems['edges'])

            dest_co.update(
                plane_barycentric_arrangement(
                    bm,
                    verts=face_inside['verts'],
                    boundary_verts={
                        v: co
                        for v, co in dest_co.items()
                        if v in face_inside['verts']
                    },
                    coord_method=barycentric_coord_method,
                    calc_only=True
                )
            )

    elif face_projection_method == 'CORNER_DIST':
        for f in simplified_faces:
            mapped_edges = [e.index for e in (sum((e_map[e] for e in f.edges), []))]
            face_inside = bm_inside(bm, sum((e_map[e] for e in f.edges), []))

            connections = ConnectionMap()
            for v in face_inside['verts']:
                for cv in f.verts:
                    cl = next(l for l in cv.link_loops if l.face is f)
                    connections[(v, v_map[cv])] = (v_map[cl.link_loop_next.vert], v_map[cl.link_loop_prev.vert])

            dest_co.update(
                plane_barycentric_arrangement(
                    bm,
                    verts=face_inside['verts'],
                    boundary_verts={
                        v: co
                        for v, co in dest_co.items()
                        if v in face_inside['verts']
                    },
                    connections=connections,
                    coord_method=barycentric_coord_method,
                    calc_only=True
                )
            )

    face_test = None
    edge_test = None
    vert_test = None

    dead = set()
    for f in inside['faces']:
        if not f.is_valid:
            dead.add(f)
    inside['faces'].difference_update(dead)
    dead.clear()
    for e in inside['edges']:
        if not e.is_valid:
            dead.add(e)
    inside['edges'].difference_update(dead)

    for v, co in dest_co.items():
        if v in frame1_set or v in frame2_set:
            continue
        v.co = co

    bmesh.ops.delete(bm, geom=[v_map[v] for v in (frame1_set | frame2_set)], context=DEL_VERTS)
    bm.normal_update()

    inside['edges'].update(border_edges)
    if ENSURE_LOOKUP:
        bm.verts.ensure_lookup_table()
        bm.edges.ensure_lookup_table()
        bm.faces.ensure_lookup_table()
    return inside


def reduce_former(bm, diff):
    t = {}

    if ENSURE_LOOKUP:
        bm.faces.ensure_lookup_table()

    for v in bm.verts:
        le = []
        for e in v.link_edges:
            if e.is_boundary:
                le.append(e)
        if len(le) != 2:
            continue
        # translation vector
        if abs(math.pi - (le[0].other_vert(v).co - v.co).angle(le[1].other_vert(v).co - v.co)) > 0.01:
            tv = (
                (le[0].other_vert(v).co - v.co).normalized() +
                (le[1].other_vert(v).co - v.co).normalized()
            ).normalized()
            angle = 0.
            for l in v.link_loops:
                if l.is_convex:
                    angle += l.calc_angle()
                else:
                    angle += 2. * math.pi - l.calc_angle()
            angle /= 2.
            if angle > math.pi/2.:
                angle = -angle
            t[v.index] = (tv / math.sin(angle)) * diff
        else:
            l = [l for l in v.link_loops if l.edge in le][0]
            tv = (l.link_loop_prev.vert.co-v.co).normalized().cross(l.face.normal)
            t[v.index] = tv * diff

    for v in bm.verts:
        if v.index in t:
            v.co += t[v.index]


def face_line_intersection(face, ln_co, ln_no):
    in_co = insct_ln_pl(ln_co, ln_co+ln_no, face.calc_center_median(), face.normal)
    if in_co:
        dist = dist_pt_pl(ln_co, face.calc_center_median(), face.normal)
    else:
        return (None, None)
    return (in_co, dist) if insct_fc_pt(face, in_co) else (None, None)


def slice_mesh(bm, dir_v, thickness=None, single_side=False):
    if not thickness:
        thickness = scaled_length(1.125, 'mm')
    dir_v.normalize()
    sorted_verts = sorted([v for v in bm.verts], key=lambda v: (v.co*dir_v, v.index))
    co_min, co_max = sorted_verts[0].co, sorted_verts[-1].co
    d_min, d_max = co_min*dir_v, co_max*dir_v
    slice_count = round((d_max-d_min)/thickness)
    thickness = (d_max-d_min) / slice_count

    next_pl_co = co_min
    it = 0

    slices = []

    for i in range(slice_count):
        pl_co = next_pl_co
        pl_dist = pl_co * dir_v
        next_pl_co = co_min + dir_v*(thickness*(i+1))
        next_pl_dist = next_pl_co * dir_v

        verts = intersect_mesh_with_plane(bm, next_pl_co, dir_v)['verts']
        slices.append((verts, pl_co, pl_dist, next_pl_dist, next_pl_co))

    bm.faces.index_update()

    for s in slices:
        verts, pl_co, pl_dist, next_pl_dist, next_pl_co = s
        reset_tags(bm)
        for v in verts:
            for l in v.link_loops:
                if l.face.calc_center_median() * dir_v > next_pl_dist:
                    bmesh.utils.loop_separate(l)
        for v in verts:
            v.co -= dir_v * thickness

        def move_vert_and_connected(start):
            q = [start]
            start.tag = True
            while len(q):
                v = q.pop(0)
                v.co -= dir_v * dist_pt_pl(v.co, pl_co, dir_v)
                for e in v.link_edges:
                    cv = e.other_vert(v)
                    if cv.tag:
                        continue
                    cv.tag = True
                    q.append(cv)

        while it < len(sorted_verts) and sorted_verts[it].co * dir_v < next_pl_dist:
            v = sorted_verts[it]
            it += 1
            if v.tag:
                continue
            move_vert_and_connected(v)

        bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=0.0001)

    while it < len(sorted_verts):
        v = sorted_verts[it]
        it += 1
        if v.tag:
            continue
        move_vert_and_connected(v)
    bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=0.0001)


def project_verts_to_plane(verts, pl_co, pl_no, dir_ratio=0.5, only_calc=False):
    new_co_map = {}
    for v in verts:
        pr_no = v.normal*dir_ratio + pl_no*(1.-dir_ratio)
        new_co = insct_ln_pl(v.co, v.co+pr_no, pl_co, pl_no)
        new_co_map[v] = new_co
        if not only_calc:
            v.co = new_co

    return new_co_map


def project_faces_to_plane(faces, pl_co, pl_no, dir_ratio=0.5):
    for f in faces:
        project_verts_to_plane(f.verts, pl_co, pl_no, dir_ratio)


def radial_cuts(bm, base, axis_point, top_vert, cut_count):
    def find_boundary_conn(v1, v2):
        q = Queue()
        q.put(v1)
        verts, edges = [], []
        while not q.empty():
            v = q.get()
            verts.append(v)
            if v is top_vert:
                break
            for e in v.link_edges:
                if not e.is_boundary or e.select or e.tag:
                    continue
                e.tag = True
                edges.append(e)
                q.put(e.other_vert(v))
        return dict(verts=verts, edges=edges)

    cuts = []
    pre_base_len = sum([e.calc_length() for e in base['edges']])

    angle_step = 2.*math.pi / cut_count
    end1, end2 = find_frame_ends(base)
    cut_pt = copy(base['verts'][0].co)
    cut_normals = []
    base_cut_points = []
    if end1:    # end2 always exist with end1
        if (end1.co-axis_point).cross(end2.co-axis_point) * (top_vert.co-axis_point) < 0.:
            end1, end2 = end2, end1
        angle_step = (end1.co-axis_point).angle(end2.co-axis_point) / (cut_count+1)
        cut_pt = copy(end1.co)
        cuts.append(find_boundary_conn(end1, top_vert))
        end_cut = find_boundary_conn(end2, top_vert)

    axis = top_vert.co - axis_point

    for i in range(cut_count):
        cut_pt -= axis_point
        cut_pt.rotate(mathutils.Quaternion(axis, angle_step))
        cut_pt += axis_point

        cut_normals.append((cut_pt-axis_point).cross(top_vert.co-axis_point))
        cuts.append(intersect_mesh_with_plane(bm, axis_point, cut_normals[-1], cut_pt-axis_point))

    base_verts = set(base['verts'])
    base_edges = set(base['edges'])
    for e in base['edges']:
        base_verts.add(e.verts[0])
        base_verts.add(e.verts[1])
    for e in bm.edges:
        if e in base_edges:
            continue
        if e.verts[0] in base_verts and e.verts[1] in base_verts:
            base_edges.add(e)
    base['edges'] = base_edges
    base['verts'] = base_verts

    if end2:
        cuts.append(end_cut)
    else:
        cut_normals.append(cut_normals[0])
        cuts.append(cuts[0])

    cut_tops = []
    for cut in cuts:
        if top_vert not in cut['verts']:
            continue
        cut['verts'].remove(top_vert)
        for e in cut['edges']:
            if top_vert in e.verts:
                cut['edges'].remove(e)
                cut_tops.append(e.other_vert(top_vert))
                print("added", e.other_vert(top_vert).index)
                break

    if not end2:
        cut_tops.append(cut_tops[0])

    top_co = top_vert.co

    def face_is_between(f, pl_no1, pl_no2):
        f_vec = f.calc_center_median()-axis_point
        return f_vec*pl_no1 < 0. and f_vec*pl_no2 > 0.

    def is_inside(f, inside):
        for v in f.verts:
            if v not in inside['verts']:
                return False
        return True

    def connected_faces(f2proj):
        queue = [f2proj.pop()]
        curr_faces = set()
        is_top = False
        proj_normal, proj_co = None, None

        while len(queue):
            f = queue.pop(0)
            is_top |= top_vert in f.verts
            curr_faces.add(f)
            for e in f.edges:
                for lf in e.link_faces:
                    if lf is not f:
                        if lf in ready_faces:
                            proj_normal = lf.normal
                            proj_co = lf.verts[0].co
                        elif lf in f2proj:
                            queue.append(lf)
                            f2proj.remove(lf)
        return curr_faces, is_top, proj_co, proj_normal

    for i in range(len(cuts)-1):
        cut1, cut2 = cuts[i], cuts[i+1]
        inside = ribbonate_mesh(bm, cut1, cut2, threshold=0.4, select_inside=False)
        ready_faces = [f for f in bm.faces if is_inside(f, inside)]
        f2proj = [f for f in bm.faces if f not in ready_faces and face_is_between(f, cut_normals[i], cut_normals[i+1])]

        top_faces, bottom_faces = None, None
        proj_co, proj_normal = None, None

        for j in range(2):
            r = connected_faces(f2proj)
            if r[1]:    # is_top?
                top_faces = r[0]
            else:
                bottom_faces, _, proj_co, proj_normal = r

        project_faces_to_plane(top_faces, top_co, (cut_tops[i].co-top_co).cross(cut_tops[i+1].co-top_co))
        project_faces_to_plane(bottom_faces, proj_co, proj_normal)

    post_base_len = sum([e.calc_length() for e in base['edges']])
    scale_val = pre_base_len / post_base_len
    vec = top_vert.co - axis_point
    zv = Vector((0, 0, 1))
    mat = Matrix.Rotation(vec.angle(zv), 4, vec.cross(zv)) * Matrix.Translation(-axis_point)
    bmesh.ops.scale(bm, vec=Vector((scale_val, scale_val, 1.)), space=mat, verts=bm.verts)


def collapse_mesh(bm, cent, axis, cut_co=None, thickness=None, threshold=None):
    if not thickness:
        thickness = scaled_length(0.25, 'mm')
    if not threshold:
        threshold = scaled_length(0.05, 'mm')
    axis.normalize()

    rel = lambda co: co-cent
    edge_cent = lambda edge: rel(edge.verts[0].co + (edge.verts[1].co - edge.verts[0].co)/2.)

    todel_faces = [f for f in bm.faces if (1. - abs(f.normal*axis)) < threshold]
    bmesh.ops.delete(bm, geom=todel_faces, context=DEL_FACES)

    # separating 'rings' orthogonal to axis
    for v in bm.verts:
        if len(v.link_loops) > 2:
            new_verts = []
            for l in v.link_loops:
                if (l.face.calc_center_median()-v.co)*axis > 0:
                    new_verts.append(bmesh.utils.loop_separate(l))

            bmesh.ops.pointmerge(bm, verts=new_verts, merge_co=v.co)

    if ENSURE_LOOKUP:
        bm.verts.ensure_lookup_table()

    # counting the edge of the circuit
    n = 0
    pv = None
    v = bm.verts[0]
    v0s = rel(v.co)*axis
    while v is not pv:
        pv = v
        n += 1
        v.tag = True
        for l in v.link_edges:
            vc = l.other_vert(v)
            if all((
                not vc.tag,
                abs(rel(vc.co).length-rel(v.co).length) < threshold*2,
                v0s*(rel(vc.co)*axis) > 0.
            )):
                v = vc
                break

    def roll_fill(edge):
        r_step = thickness / n
        rot_matrix = Matrix.Rotation(2.*math.pi/n, 3, axis)
        e = edge
        while (insct_pt_ln(e.verts[0].co, cent, cent+axis)[0] - e.verts[0].co).length > thickness*2.-threshold:
            new_e = bmesh.ops.extrude_edge_only(bm, edges=[e])['geom'][2]

            anv0 = insct_pt_ln(rel(new_e.verts[0].co), Vector(), axis)[0]
            v0 = rel(new_e.verts[0].co) - anv0
            new_e.verts[0].co = anv0 + v0.normalized() * (v0.length - r_step)
            new_e.verts[0].co.rotate(rot_matrix)
            new_e.verts[0].co += cent

            anv1 = insct_pt_ln(rel(new_e.verts[1].co), Vector(), axis)[0]
            v1 = rel(new_e.verts[1].co) - anv1
            new_e.verts[1].co = anv1 + v1.normalized() * (v1.length - r_step)
            new_e.verts[1].co.rotate(rot_matrix)
            new_e.verts[1].co += cent

            e = new_e

        return e

    if not cut_co:
        cut_co = rel(bm.verts[0].co)
    cut_co = cut_co - insct_pt_ln(cut_co, Vector(), axis)[0]

    cut_pl_no = normal(Vector(), axis, cut_co)
    cut_edges = [e for e in bm.edges
                 if abs(dist_pt_pl(edge_cent(e), Vector(), cut_pl_no)) < threshold*2 and edge_cent(e)*cut_co > 0.]

    for ce in cut_edges:
        e_co = edge_cent(ce)
        ax_near = insct_pt_ln(e_co, Vector(), axis)[0]
        ax_rel = e_co-ax_near
        dist = round(ax_rel.length / thickness) * thickness

        for v in ce.verts:
            for l in v.link_loops:
                if dist_pt_pl(rel(l.face.calc_center_median()), Vector(), cut_pl_no) < 0.:
                    bmesh.utils.loop_separate(l)

        pe = None
        e = ce
        step = 0
        while e is not pe:
            diff = step*thickness/n
            pe = e
            e.tag = True
            for v in e.verts:
                v_an = insct_pt_ln(rel(v.co), Vector(), axis)[0]
                v.co = cent + v_an + ((rel(v.co)-v_an).normalized() * (dist-diff))
            for f in e.link_faces:
                for fe in f.edges:
                    if not fe.tag and abs((fe.verts[0].co-fe.verts[1].co)*axis) > threshold*math.pi:
                        e = fe
                        break
            step += 1

        roll_fill(e)

    bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=threshold)


def anchor_to_cursor_vector(context):
    return context.scene.cursor_location - context.active_object.location


class SliceMeshOperator(bpy.types.Operator):
    """ divides mesh into slices """
    bl_idname = "object.slice_mesh"
    bl_label = "Slice Mesh"
    bl_options = {'REGISTER', 'UNDO'}

    thickness = bpy.props.FloatProperty(name="Thickness",
                                        default=1.15,
                                        min=0.1,
                                        max=2.,
                                        step=0.1,
                                        subtype='FACTOR')

    single_side = bpy.props.BoolProperty(name="Single side", default=False)

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "thickness", text="Thickness (mm)")

    def execute(self, context):
        bm = bmesh.new()
        bm.from_mesh(context.active_object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        slice_mesh(bm, context.scene.cursor_location - context.active_object.location,
                   scaled_length(self.thickness, 'mm'),
                   self.single_side)

        bm.transform(context.active_object.matrix_world.inverted())
        bm.to_mesh(context.active_object.data)
        bm.free()
        return {'FINISHED'}


# class SetupCursorParallelOperator(bpy.types.Operator):
    # """ sets cursor position to create with object origin a parallel vector to selected vertices """
    # bl_idname = "edit_mesh.setup_cursor_parallel"
    # bl_label = "Setup Cursor Parallel"

    # @classmethod
    # def poll(cls, context):
        # return context.mode == 'EDIT_MESH'

    # def execute(self, context):
        # return {'FINISHED'}


class RollMeshOperator(bpy.types.Operator):
    """ makes mesh rolled """
    bl_idname = "object.roll_mesh"
    bl_label = "Roll Mesh"
    bl_options = {'REGISTER', 'UNDO'}

    thickness = bpy.props.FloatProperty(name="Thickness",
                                        default=0.13,
                                        min=0.05,
                                        step=0.01,
                                        max=1.,
                                        subtype='FACTOR')

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "thickness", text="Thickness (mm)")

    def execute(self, context):
        bm = bmesh.new()
        bm.from_mesh(context.active_object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        collapse_mesh(bm,
                      context.active_object.location,
                      context.scene.cursor_location - context.active_object.location,
                      thickness=scaled_length(self.thickness))

        bm.transform(context.active_object.matrix_world.inverted())
        bm.to_mesh(context.active_object.data)
        bm.free()
        return {'FINISHED'}


class SuggestRibPlacementOperator(bpy.types.Operator):
    """ creates planes orthogonal to direction vector in places where mesh collapses """
    bl_idname = "object.suggest_rib_placement"
    bl_label = "Suggest Rib Placement"
    bl_options = {'REGISTER', 'UNDO'}

    deviation = bpy.props.FloatProperty(name="Deviation",
                                        description="maximal angle",
                                        default=math.radians(10.0),
                                        min=0.0, max=math.radians(90.0), subtype='ANGLE')

    threshold = bpy.props.FloatProperty(name="Threshold",
                                        description="Minimal distance to another vertex to create new one",
                                        default=0.05, soft_min=0., subtype='FACTOR')

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "deviation", text="Deviation")
        layout.prop(self.properties, "threshold", text="Threshold (mm)")

    def execute(self, context):
        obj_loc = context.active_object.location
        cur_loc = context.scene.cursor_location
        dir_v = cur_loc - obj_loc
        collapses = find_surface_collapse(context.active_object, dir_v, self.deviation, scaled_length(self.threshold))
        for c in collapses:
            bpy.ops.mesh.primitive_plane_add(radius=2,
                                             location=obj_loc+dir_v*(c-dist_pt_pl(obj_loc, Vector((0, 0, 0)), dir_v)),
                                             rotation=Vector((0, 0, 1)).rotation_difference(dir_v).to_euler())
        return {'FINISHED'}


class MeshPlaneIntersectionOperator(bpy.types.Operator):
    """ Adds to mesh vertices and edges on intersection with plane """
    bl_idname = "object.intersect_mesh_plane"
    bl_label = "Intersect Mesh with Plane"
    bl_options = {'REGISTER', 'UNDO'}

    threshold = bpy.props.FloatProperty(name="Threshold",
                                        description="Minimal distance to another vertex to create new one",
                                        default=0.05, soft_min=0., subtype='FACTOR')

    half = bpy.props.BoolProperty(name="Half plane", default=False)

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT' and len(context.selected_objects) == 2

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "threshold", text="Threshold (mm)")
        layout.prop(self.properties, "half", text="Half plane")

    def execute(self, context):
        pl = context.selected_objects[0] \
             if context.selected_objects[0].name != context.active_object.name \
             else context.selected_objects[1]
        pl_bm = bmesh.new()
        pl_bm.from_mesh(pl.data)
        pl_bm.transform(pl.matrix_world)
        pl_bm.normal_update()
        if ENSURE_LOOKUP:
            pl_bm.faces.ensure_lookup_table()
        pl_no = copy(pl_bm.faces[0].normal)
        pl_bm.free()

        bm = bmesh.new()
        bm.from_mesh(context.active_object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()
        side = (context.scene.cursor_location - pl.location) if self.half else None
        intersection = intersect_mesh_with_plane(bm, pl.location, pl_no, side, threshold=scaled_length(self.threshold))
        for e in intersection['verts']:
            e.select_set(True)
        bm.transform(context.active_object.matrix_world.inverted())
        bm.to_mesh(context.active_object.data)
        bm.free()
        return {'FINISHED'}


class IntersectRibsWithStrut(bpy.types.Operator):
    """ Creates cuts on intersections of ribs and strut """
    bl_idname = "object.intersect_strut_ribs"
    bl_label = "Intersect Strut with Ribs"
    bl_options = {'REGISTER', 'UNDO'}

    thickness = bpy.props.FloatProperty(name="Thickness (mm)",
                                        description="Thickness of cardboard (width of cuts in ribs)",
                                        default=1.15, soft_min=0.5, soft_max=3., subtype='FACTOR')

    reverse = bpy.props.BoolProperty(name="Reversed", description="Reverse direction of cutting", default=False)

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT' and len(context.selected_objects) == 2

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "thickness", text="Thickness (mm)")
        layout.prop(self.properties, "reverse", text="Reverse cuts")

    def execute(self, context):
        ribs = []
        zero_is_active = context.selected_objects[0].name != context.active_object.name
        ribs_obj = context.selected_objects[0 if zero_is_active else 1]
        ribs_mesh = bmesh.new()
        ribs_mesh.from_mesh(ribs_obj.data)
        ribs_mesh.transform(ribs_obj.matrix_world)
        ribs_mesh.normal_update()
        rib_face = ribs_mesh.faces[0]
        ribs.append((rib_face.calc_center_bounds(), copy(rib_face.normal)))
        ribs_mesh.free()

        strut = bmesh.new()
        strut.from_mesh(context.active_object.data)
        strut.transform(context.active_object.matrix_world)
        strut.normal_update()
        if ENSURE_LOOKUP:
            strut.faces.ensure_lookup_table()
        strut_face = strut.faces[0]
        strut_co = strut_face.calc_center_bounds()
        strut_no = copy(strut_face.normal)

        strut_cuts(strut, ribs,
                   cut_direciton=-1. if self.reverse else 1.,
                   thickness=scaled_length(self.thickness, 'mm'))
        strut.transform(context.active_object.matrix_world.inverted())
        strut.to_mesh(context.active_object.data)
        strut.free()

        ribs_mesh = bmesh.new()
        ribs_mesh.from_mesh(ribs_obj.data)
        ribs_mesh.transform(ribs_obj.matrix_world)
        ribs_mesh.normal_update()
        strut_cuts(ribs_mesh, [(strut_co, strut_no)],
                   cut_direciton=1. if self.reverse else -1.,
                   thickness=scaled_length(self.thickness, 'mm'))
        ribs_mesh.transform(ribs_obj.matrix_world.inverted())
        ribs_mesh.to_mesh(ribs_obj.data)
        ribs_mesh.free()

        return {'FINISHED'}


class CreateRibsOperator(bpy.types.Operator):
    """ Creates ribs from intersections of mesh and planes """
    bl_idname = "object.create_ribs_operator"
    bl_label = "Create Ribs"
    bl_options = {'REGISTER', 'UNDO'}

    threshold = bpy.props.FloatProperty(name="Threshold",
                                        description="Minimal distance to another vertex to create new one",
                                        default=0.05, soft_min=0., subtype='FACTOR')

    reduce_on = bpy.props.BoolProperty(name="reduce",
                                       description="When option is selected created ribs will be reduced by thickness of covering paper",
                                       default=True)

    thickness = bpy.props.FloatProperty(name="Thickness",
                                        description="thickness of covering paper",
                                        default=0.25,
                                        min=0.05, max=0.5,
                                        subtype='FACTOR')

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT' and len(context.selected_objects) > 1

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "threshold", text="Threshold (mm)")
        layout.prop(self.properties, "reduce", text="Reduce")
        layout.prop(self.properties, "thickness", text="Thickness (mm)")

    def execute(self, context):
        bm = bmesh.new()
        bm.from_mesh(context.active_object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        planes = []
        for plane in context.selected_objects:
            if plane.name == context.active_object.name:
                continue
            pl_bm = bmesh.new()
            pl_bm.from_mesh(plane.data)
            pl_bm.transform(plane.matrix_world)
            pl_bm.normal_update()
            if ENSURE_LOOKUP:
                pl_bm.faces.ensure_lookup_table()
            planes.append((pl_bm.faces[0].calc_center_bounds(), Vector(pl_bm.faces[0].normal)))
            pl_bm.free()

        make_ribs(bm, planes, threshold=scaled_length(self.threshold))

        if self.reduce_on:
            reduce_former(bm, scaled_length(self.thickness))

        me = bpy.data.meshes.new(context.active_object.name+"ribs datablock")
        ribs = bpy.data.objects.new(name="Ribs of "+context.active_object.name, object_data=me)
        bpy.context.scene.objects.link(ribs)
        bm.to_mesh(ribs.data)
        bm.free()
        return {'FINISHED'}


plane_anchor_point_enum = [
    ('ACTIVE', 'Active element point', ''),
    ('SELECTION_MEDIAN', 'Median point of selection', ''),
    ('BOUNDARY_MEDIAN', 'Median point of boundary vertices', ''),
    ('CURSOR', '3D Cursor', ''),
    ('OBJECT_ANCHOR', 'Object anchor point', ''),
]

plane_normal_enum = [
    ('ACTIVE_NORMAL', 'Active element normal', ''),
    ('SELECTION_MEDIAN', 'Median of selection normals', ''),
    ('ANCHOR_CURSOR', 'Vector from object anchor to 3D cursor', ''),
]


coordinate_methods_enum = [
    ('WACHSPRESS', 'Wachspress', ''),
    ('DISCRETE_HARMONIC', 'Discrete harmonic', ''),
    ('MEAN_VALUE', 'Mean value', ''),
]


flatten_methods_enum = [
    ('BARYCENTRIC', 'Barycentric mapping', ''),
    ('PROJECTION', 'Projection', ''),
]


class FlattenSelectionOperator(bpy.types.Operator):
    """
    Transforms selected faces to common plane
    """
    bl_idname = "edit_mesh.flatten_selection"
    bl_label = "Transforms selected vertices to common plane"
    bl_options = {'REGISTER', 'UNDO'}

    plane_anchor_point = bpy.props.EnumProperty(
        name="PlaneAnchorPoint",
        description="Anchor point of target plane",
        items=plane_anchor_point_enum,
        default='ACTIVE'
    )

    plane_normal = bpy.props.EnumProperty(
        name="PlaneNormal",
        description="Normal of target plane",
        items=plane_normal_enum,
        default='ACTIVE_NORMAL'
    )

    dir_ratio = bpy.props.FloatProperty(
        name="Projection direction Ratio",
        description="Proportion of vertice normal to plane normal in projection direction",
        default=0.5, min=0., max=1.,
        subtype='FACTOR'
    )

    arrangement_method = bpy.props.EnumProperty(
        name="Arrangement method",
        description="Arrangement method for vertices inside selection",
        items=flatten_methods_enum,
        default='BARYCENTRIC'
    )

    coordinate_method = bpy.props.EnumProperty(
        name="CoordinateMethod",
        description="Homogenous coordinate calculation method",
        items=coordinate_methods_enum,
        default='MEAN_VALUE'
    )

    def draw(self, context):
        self.layout.prop(self.properties, "plane_anchor_point", text="Plane anchor point")
        self.layout.prop(self.properties, "plane_normal", text="Plane Normal")
        self.layout.prop(self.properties, "dir_ratio", text="Direction ratio")
        self.layout.prop(self.properties, "arrangement_method", text="Arrangement method")
        if self.arrangement_method == 'BARYCENTRIC':
            self.layout.prop(self.properties, "coordinate_method", text="Coordinate method")

    @classmethod
    def poll(cls, context):
        if context.mode != 'EDIT_MESH':
            return False
        bm = bmesh.from_edit_mesh(context.active_object.data)
        return len(tuple(selected_faces(bm))) > 1

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        verts = tuple(selected_verts(bm))
        faces = set(selected_faces(bm))
        boundary = tuple(
            v for v in verts
            if v.is_boundary or any((
                f not in faces
                for f in v.link_faces
            ))
        )
        active = active_element(bm)
        pl_co = None
        if self.plane_normal == 'ACTIVE_NORMAL':
            if type(active) in (bmesh.types.BMVert, bmesh.types.BMFace):
                pl_no = active.normal
            elif type(active) == bmesh.types.BMEdge:
                pl_no = sum((v.normal for v in active.verts), Vector()) / 2.
            else:
                raise Exception("UNKNOWN-TYPE-OF-ACTIVE-ELEMENT")
        elif self.plane_normal == 'ANCHOR_CURSOR':
            pl_no = anchor_to_cursor_vector(context)
        elif self.plane_normal == 'SELECTION_MEDIAN':
            pl_no = sum((v.normal for v in verts), Vector()).normalized()

        if self.plane_anchor_point == 'ACTIVE':
            if type(active) == bmesh.types.BMVert:
                pl_co = active.co
            elif type(active) == bmesh.types.BMEdge:
                pl_co = edge_middle(active)
            elif type(active) == bmesh.types.BMFace:
                pl_co = active.calc_center_median()
        elif self.plane_anchor_point == 'OBJECT_ANCHOR':
            pl_co = context.active_object.location
        elif self.plane_anchor_point == 'CURSOR':
            pl_co = context.scene.cursor_location
        elif self.plane_anchor_point == 'SELECTION_MEDIAN':
            pl_co = sum((v.co for v in verts), Vector()) / len(verts)
        elif self.plane_anchor_point == 'BOUNDARY_MEDIAN':
            pl_co = sum((v.co for v in boundary), Vector()) / len(boundary)

        if self.arrangement_method == 'BARYCENTRIC':
            boundary_new_co = project_verts_to_plane(
                boundary,
                pl_co, pl_no,
                self.dir_ratio,
                only_calc=True
            )
            triangulate_without_border_connections(
                bm,
                tuple(selected_faces(bm)),
                boundary
            )
            inside_new_co = plane_barycentric_arrangement(
                bm,
                verts,
                boundary_new_co,
                coord_method=self.coordinate_method
            )
        elif self.arrangement_method == 'PROJECTION':
            cos = project_verts_to_plane(
                verts,
                pl_co, pl_no,
                self.dir_ratio,
                only_calc=False
            )

        bm.transform(context.active_object.matrix_world.inverted())
        bmesh.update_edit_mesh(context.active_object.data)

        return {'FINISHED'}


edge_pull_methods = [
    ('STRING_SEPARATELY', 'Separetly', 'Each vertice distance is calculated separatly by its distanses from ends'),
    ('STRING_TOGETHER', 'Together', 'Veritice distanses are calculated by their distance on the path of mapped edges')
]

face_projection_methods = [
    ('BARYCENTRIC', 'Barycentric', 'Barycentric mapping algorithm'),
    ('CORNER_DIST', 'Corner distance', 'Similar to barycentric but calculated '
                                       'on virtual connections to face corners only'),
]


class RadialCutsOperator(bpy.types.Operator):
    """
    Cuts mesh by rotating mid-planes and simplify parts between cuts.
    """
    bl_idname = "edit_mesh.radial_cuts"
    bl_label = "Radial cuts"
    bl_options = {'REGISTER', 'UNDO'}

    cut_count = bpy.props.IntProperty(
        name="Number of Cuts",
        default=5
    )

    threshold = bpy.props.FloatProperty(
        name="Threshold",
        description="Maximal distance to move vertices",
        default=0.5,
        soft_min=0., soft_max=2.,
        subtype='FACTOR'
    )

    edge_projection_method = bpy.props.EnumProperty(
        name="EdgeProjectionMethod",
        description="Method of moving vertices to simplified edges",
        items=edge_pull_methods,
        default='STRING_TOGETHER'
    )

    face_projection_method = bpy.props.EnumProperty(
        name="FaceProjectionMethod",
        description="Method of moving vertices to simplified faces",
        items=face_projection_methods,
        default='BARYCENTRIC'
    )

    barycentric_coord_method = bpy.props.EnumProperty(
        name="CoordinateMethod",
        description="Homogenous coordinate calculation method",
        items=coordinate_methods_enum,
        default='MEAN_VALUE'
    )

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "cut_count", text="Number of cuts")
        layout.prop(self.properties, "threshold", text="Threshold (mm)")
        layout.prop(self.properties, "edge_projection_method", text="Edge Projection Method")
        layout.prop(self.properties, "face_projection_method", text="Face Projection Method")
        if self.face_projection_method == 'BARYCENTRIC':
            layout.prop(self.properties, "barycentric_coord_method", text="Coordinate method")

    @classmethod
    def poll(cls, context):
        return context.mode == 'EDIT_MESH'

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        reset_tags(bm)

        top = active_element(bm)
        verts = list(selected_verts(bm))
        verts.remove(top)
        base = {'verts': verts, 'edges': tuple(selected_edges(bm))}

        radial_cuts(bm, base, context.scene.cursor_location, top, self.cut_count)

        bm.transform(context.active_object.matrix_world.inverted())
        bmesh.update_edit_mesh(context.active_object.data)

        return {'FINISHED'}


class RibbonateMeshOperator(bpy.types.Operator):
    """
    Transformates segment for ribbon where folds are always
    connecting different ribs
    """
    bl_idname = "edit_mesh.simplify_segment_operator"
    bl_label = "Ribbonate mesh"
    bl_options = {'REGISTER', 'UNDO'}

    edge_projection_method = bpy.props.EnumProperty(
        name="EdgeProjectionMethod",
        description="Method of moving vertices to simplified edges",
        items=edge_pull_methods,
        default='STRING_TOGETHER'
    )

    face_projection_method = bpy.props.EnumProperty(
        name="FaceProjectionMethod",
        description="Method of moving vertices to simplified faces",
        items=face_projection_methods,
        default='BARYCENTRIC'
    )

    barycentric_coord_method = bpy.props.EnumProperty(
        name="CoordinateMethod",
        description="Homogenous coordinate calculation method",
        items=coordinate_methods_enum,
        default='MEAN_VALUE'
    )

    def draw(self, context):
        layout = self.layout
        layout.prop(self.properties, "edge_projection_method", text="Edge Projection Method")
        layout.prop(self.properties, "face_projection_method", text="Face Projection Method")
        layout.prop(self.properties, "barycentric_coord_method", text="Coordinate method")

    @classmethod
    def poll(cls, context):
        return context.mode == 'EDIT_MESH'

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        reset_tags(bm)

        verts = tuple(selected_verts(bm))

        q = Queue()
        q.put(verts[0])
        verts[0].tag = True
        frame1 = dict(verts=set(), edges=[])
        while not q.empty():
            v = q.get()
            frame1['verts'].add(v)
            for e in v.link_edges:
                if e.tag:
                    continue
                cv = e.other_vert(v)
                if e.select and not e.tag and not cv.tag:
                    cv.tag = True
                    e.tag = True
                    frame1['edges'].append(e)
                    q.put(cv)
        frame1['verts'] = list(frame1['verts'])

        frame2 = dict(verts=[], edges=[])
        for v in verts:
            if not v.tag:
                frame2['verts'].append(v)
            v.tag = False
        for e in selected_edges(bm):
            if not e.tag:
                frame2['edges'].append(e)
            e.tag = False

        if len(frame2['verts']) > 0:
            reset_tags(bm)
            inside = ribbonate_mesh(
                bm, frame1, frame2,
                edge_projection_method=self.edge_projection_method,
                face_projection_method=self.face_projection_method,
                barycentric_coord_method=self.barycentric_coord_method,
            )

        bm.transform(context.active_object.matrix_world.inverted())
        bmesh.update_edit_mesh(context.active_object.data)

        return {'FINISHED'}


class StringOfSpringsOperator(bpy.types.Operator):
    """ Moves interior vertices in edge string into line, with saving its length proportions """
    bl_idname = "edit_mesh.string_of_springs"
    bl_label = "String of springs"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        if context.mode != 'EDIT_MESH':
            return False
        bm = bmesh.from_edit_mesh(context.active_object.data)
        return len(tuple(selected_edges(bm))) > 1

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.active_object.data)
        try:
            string_of_springs(bm, tuple(selected_edges(bm)))
            return {'FINISHED'}
        except Exception as e:
            raise
            return {'CANCELLED'}


class ReduceFormerOperator(bpy.types.Operator):
    bl_idname = "object.reduce_former"
    bl_label = "Reduce former"
    bl_options = {'REGISTER', 'UNDO'}

    thickness = bpy.props.FloatProperty(
        name="Thickness (mm)",
        description="thickness of covering paper",
        default=0.25,
        min=0.05, max=0.5,
        subtype='FACTOR'
    )

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def execute(self, context):
        for obj in context.selected_objects:
            bm = bmesh.new()
            bm.from_mesh(obj.data)
            bm.transform(obj.matrix_world)
            bm.normal_update()

            reduce_former(bm, 0.001*self.thickness*context.scene.paper_model.scale)

            bm.transform(obj.matrix_world.inverted())
            bm.to_mesh(obj.data)
            bm.free()

        return {'FINISHED'}


def path_normal(bm, v1, v2):
    normals_map = {}

    def calc_edge_normal(v, sv, prev, dist):
        if prev:
            e = next(e for e in v.link_edges if e.other_vert(v) == prev)
            normals_map[e] = edge_normal(e)*e.calc_length()

    start_v, prev_v = dijkstra(
        bm,
        begin_set=set((v1, )), end_set=set((v2, )),
        on_each=calc_edge_normal
    )

    path_normals = []

    v = v2
    while v != v1:
        for e in v.link_edges:
            if prev_v[v] and e.other_vert(v) == prev_v[v]:
                path_normals.append(normals_map[e])
                v = prev_v[v]

    normal_sum = sum(path_normals, Vector((0, 0, 0)))
    normal_sum.normalize()
    pl_no = normal_sum.cross((v1.co-v2.co))
    return pl_no


class PathNormalTestOperator(bpy.types.Operator):
    bl_idname = "edit_mesh.path_normal"
    bl_label = "Path normal"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        if context.mode != 'EDIT_MESH':
            return False
        bm = bmesh.from_edit_mesh(context.active_object.data)
        return len(tuple(selected_verts(bm))) == 2

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.active_object.data)
        v1, v2 = selected_verts(bm)

        reset_tags(bm)

        pl_no = path_normal(bm, v1, v2)
        bmesh.ops.connect_vert_pair(bm, verts=[v1, v2])

        bpy.ops.mesh.primitive_plane_add(
            radius=3,
            location=context.active_object.location+v1.co,
            rotation=Vector((0, 0, 1)).rotation_difference(pl_no).to_euler()
        )

        return {'FINISHED'}


class StringConnectionOperator(bpy.types.Operator):
    bl_idname = "edit_mesh.string_connection"
    bl_label = "String Connection"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        if context.mode != 'EDIT_MESH':
            return False
        bm = bmesh.from_edit_mesh(context.active_object.data)
        return len(tuple(selected_verts(bm))) == 2

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.active_object.data)
        v1, v2 = tuple(selected_verts(bm))
        connection = bmesh.ops.connect_vert_pair(bm, verts=[v1, v2])['edges']
        return {'FINISHED'}


class ConnectOnDirectionOperator(bpy.types.Operator):
    bl_idname = "edit_mesh.connect_on_direction"
    bl_label = "Connect verts on direction"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        if context.mode != 'EDIT_MESH':
            return False
        bm = bmesh.from_edit_mesh(context.active_object.data)
        return len(tuple(selected_verts(bm))) == 2

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.active_object.data)
        v1, v2 = tuple(selected_verts(bm))
        dir_v = context.scene.cursor_location - v1.co
        edges = connect_vert_pair_on_direction(bm, v1, v2, dir_v)
        for e in edges:
            e.select_set(True)
        return {'FINISHED'}


class TriangulateSpecialOperator(bpy.types.Operator):
    bl_idname = "edit_mesh.special_triangulation"
    bl_label = "MyTriangulate"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        if context.mode != 'EDIT_MESH':
            return False
        bm = bmesh.from_edit_mesh(context.active_object.data)
        return len(tuple(selected_edges(bm))) > 0

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.active_object.data)
        triangulate_without_border_connections(bm, tuple(selected_faces(bm)))
        return {'FINISHED'}

# class SelectInsideOperator(bpy.types.Operator):
#     """ selects mesh part inside selected loop """
#     bl_idname = "edit_mesh.select_inside"
#     bl_label = "Select Inside"
#
#     @classmethod
#     def poll(cls, context):
#         return context.mode == 'EDIT_MESH'
#
#      def execute(self, context):
#         bm = bmesh.from_edit_mesh(context.object.data)
#         return {'FINISHED'}


def path_normal2(bm, v1, v2):
    normals = []

    def add_to_normal_sum(v, sv, prev, dist):
        if prev:
            e = next(e for e in v.link_edges if e.other_vert(v) == prev)
            normals.append(edge_normal(e)*e.calc_length())

    dijkstra(bm,
             begin_set=set((v1, )), end_set=set((v2, )),
             on_each=add_to_normal_sum)

    normal_sum = sum(normals, Vector((0, 0, 0)))
    normal_sum.normalize()
    pl_no = normal_sum.cross((v1.co-v2.co))
    return pl_no


class VIEW3D_PT_paper_model_preparing(bpy.types.Panel):
    bl_label = "Preparing"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "Paper Model"

    def draw(self, context):
        layout = self.layout
        box = layout.box()
        box.label("Ribs (Skeleton)")
        box.operator("object.suggest_rib_placement")
        box.operator("object.create_ribs_operator")
        box.operator("object.reduce_former")
        box.operator("object.intersect_strut_ribs")

        box = layout.box()
        box.label("Part Tools")
        box.operator("edit_mesh.simplify_segment_operator")
        box.operator("object.slice_mesh")
        box.operator("object.roll_mesh")
        box.operator("edit_mesh.radial_cuts")

        box = layout.box()
        box.label("Helpers")
        box.operator("object.intersect_mesh_plane")
        box.operator("edit_mesh.string_of_springs")
        box.operator("edit_mesh.flatten_selection")

        layout.operator("edit_mesh.special_triangulation")
        layout.operator("edit_mesh.string_connection")
        layout.operator("edit_mesh.path_normal")
        layout.operator("edit_mesh.connect_on_direction")


def check_face_angles_diff(faces):
    pre_angles = {}
    for f in faces:
        pre_angles[f.index] = {}
        for l in f.loops:
            pre_angles[f.index][l.vert.index] = l.calc_angle()
    yield None

    diff = {}
    for f in faces:
        if not f.is_valid:
            continue
        diff[f.index] = {}
        for l in f.loops:
            diff[f.index][l.vert.index] = (
                pre_angles[f.index][l.vert.index] - l.calc_angle(),
                l.calc_angle() / pre_angles[f.index][l.vert.index]
            )
    yield diff

    raise StopIteration()


def check_vert_angles_diff(verts):
    pre_angles = {}
    for v in verts:
        pre_angles[v.index] = {}
        v_angles = {}
        for l in v.link_loops:
            v_angles[l.face.index] = l.calc_angle()
        sum_angle = sum(v_angles.values())
        for l, angle in v_angles.items():
            pre_angles[v.index][l] = angle / sum_angle
    yield None

    diff = {}
    for v in verts:
        if not v.is_valid:
            continue
        v_angles = {}
        diff[v.index] = {}
        for l in v.link_loops:
            v_angles[l.face.index] = l.calc_angle()
        sum_angle = sum(v_angles.values())
        for l, angle in v_angles.items():
            diff[v.index][l] = (angle / sum_angle) / pre_angles[v.index][l]

    yield diff

    raise StopIteration()


def check_edges_diff(edges):
    pre_lenghts = {e.index: e.calc_length() for e in edges}
    yield None

    diff = {
        e.index: pre_lenghts[e.index] / e.calc_length()
        for e in edges
    }
    yield diff

    raise StopIteration()

#
# def register():
#     bpy.utils.register_class(RibbonateMeshOperator)
#     bpy.utils.register_class(SliceMeshOperator)
#     bpy.utils.register_class(RollMeshOperator)
#     bpy.utils.register_class(RadialCutsOperator)
#
#     bpy.utils.register_class(ReduceFormerOperator)
#     bpy.utils.register_class(CreateRibsOperator)
#     bpy.utils.register_class(IntersectRibsWithStrut)
#     bpy.utils.register_class(SuggestRibPlacementOperator)
#
#     bpy.utils.register_class(StringOfSpringsOperator)
#     bpy.utils.register_class(MeshPlaneIntersectionOperator)
#     bpy.utils.register_class(PathNormalTestOperator)
#
#     bpy.utils.register_class(TriangulateSpecialOperator)
#     bpy.utils.register_class(StringConnectionOperator)
#     bpy.utils.register_class(FlattenSelectionOperator)
#
#     bpy.utils.register_class(ConnectOnDirectionOperator)
#
#     bpy.utils.register_class(VIEW3D_PT_paper_model_preparing)
#
#
# def unregister():
#     bpy.utils.unregister_class(VIEW3D_PT_paper_model_preparing)
#
#     bpy.utils.unregister_class(ConnectOnDirectionOperator)
#
#     bpy.utils.unregister_class(FlattenSelectionOperator)
#     bpy.utils.unregister_class(TriangulateSpecialOperator)
#     bpy.utils.unregister_class(StringConnectionOperator)
#     bpy.utils.unregister_class(PathNormalTestOperator)
#     bpy.utils.unregister_class(MeshPlaneIntersectionOperator)
#     bpy.utils.unregister_class(StringOfSpringsOperator)
#
#     bpy.utils.unregister_class(SuggestRibPlacementOperator)
#     bpy.utils.unregister_class(IntersectRibsWithStrut)
#     bpy.utils.unregister_class(CreateRibsOperator)
#     bpy.utils.unregister_class(ReduceFormerOperator)
#
#     bpy.utils.unregister_class(RadialCutsOperator)
#     bpy.utils.unregister_class(RollMeshOperator)
#     bpy.utils.unregister_class(SliceMeshOperator)
#     bpy.utils.unregister_class(RibbonateMeshOperator)
#

#if __name__ == "__main__":
    #register()
