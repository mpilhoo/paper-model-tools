import bpy
import bmesh
from mathutils import Vector
from mathutils.geometry import intersect_point_tri
from queue import Queue
from .common.helpers import (
    ensure_lookup, 
    VERTS, EDGES, FACES, ALL_MESH,
    get_separated_object,
)
from .common.selection import (
    selected_verts,
    selected_edges,
    find_selected_cycles_with_bridge,
    find_selected_two_paths,
    WRONG_SELECTION,
)
from .common.algorithm import (
    find_shortest_paths,
    select_between_cycles,
    select_inside_cycle,
    side_division,
)
from .common.uv import (
    calculate_co_by_uv_base,
    clean_uv_map,
    connect_path_uv,
    get_base,
    fit_uv,
    setup_cut_uv,
    setup_straith_path_uv,
    setup_boundary_path_uv,
)
from .common.values import DEL_VERTS
from .common.baking import bake_textures
from .common.math import (
    edge_middle,
    co_middle
)


def choose_shorter(seq1, seq2):
    s1_len = s2_len = 0.
    for e in seq1:
        s1_len += e.calc_length()
    for e in seq2:
        s2_len += e.calc_length()

    return (seq1, seq2) if s1_len < s2_len else (seq2, seq1)


def bipartite_traingulation(bm, set1, set2, source_faces):
    def dist(v1, v2):
        if not v1 or not v2:
            return float('Inf')
        return (v1.co-v2.co).length

    to_conn = []

    class NoneVert:
        def index(self):
            return -1

        def __bool__(self):
            return False

    class LoopCut:
        def __init__(self, loop):
            self.nloop = loop.link_loop_next
            self.ploop = loop
            self.pair = None

        def pair_cuts(self, other):
            self.pair = other
            other.pair = self

        def verts1(self):
            return (self.nloop.vert,
                    self.ploop.link_loop_prev.vert if self.ploop != self.pair.nloop else NoneVert())

        def verts2(self):
            return (self.nloop.link_loop_next.vert if self.nloop != self.pair.ploop else NoneVert(),
                    self.ploop.vert)

        def cut_lenght(self):
            return min(dist(*self.verts1()), dist(*self.verts2()))

        def make_cut(self):
            if dist(*self.verts1()) < dist(*self.verts2()):
                to_conn.append(self.verts1())
                self.ploop = self.ploop.link_loop_prev
            else:
                to_conn.append(self.verts2())
                self.nloop = self.nloop.link_loop_next

        def collides(self):
            return (self.nloop == self.pair.ploop and self.ploop == self.pair.nloop.link_loop_next) or \
                   (self.ploop == self.pair.nloop and self.nloop == self.pair.ploop.link_loop_prev)

    if type(source_faces) != set:
        source_faces = set(source_faces)

    added_faces = set()
    added_edges = set()

    def connects_frames(edge):
        return any((
            edge.verts[0] in set1 and edge.verts[1] in set2,
            edge.verts[0] in set2 and edge.verts[1] in set1,
        ))

    for face in source_faces:
        cut1 = cut2 = None
        for loop in face.loops:
            if connects_frames(loop.edge):
                if not cut1:
                    cut1 = LoopCut(loop)
                else:
                    cut2 = LoopCut(loop)
        if not cut1 or not cut2:
            continue
        cut1.pair_cuts(cut2)
        while not cut1.collides():
            if cut1.cut_lenght() < cut2.cut_lenght():
                cut1.make_cut()
            else:
                cut2.make_cut()

    for v1, v2 in to_conn:
        new_edge = bmesh.ops.connect_vert_pair(bm, verts=[v1, v2])['edges'][0]
        added_edges.add(new_edge)
        ensure_lookup(FACES)
        added_faces.update(f for f in new_edge.link_faces if f not in source_faces)

    return {
        'faces': added_faces,
        'edges': added_edges
    }


def generate_ribbon_geometry(bm, frame1, frame2, predef_conn=tuple()):
    dupl_dict = bmesh.ops.duplicate(bm, geom=list(frame1 | frame2))
    ensure_lookup(VERTS | EDGES)
    edges = [e for e in dupl_dict['geom'] if type(e) is bmesh.types.BMEdge]
    v_map = dupl_dict['vert_map']
    e_map = dupl_dict['edge_map']

    for conn in predef_conn:
        new_edge = bm.edges.new([v_map[v] for v in conn])
        edges.append(new_edge)
    ensure_lookup(EDGES)

    start_frame, finish_frame = choose_shorter(frame1, frame2)

    def on_finish(v, sv, prev, dist):
        try:
            new_edge = bm.edges.new([v_map[v], v_map[sv]])
            ensure_lookup(EDGES)
            edges.append(new_edge)
        except:
            pass

    start_set, end_set  = set(), set()
    for e in frame1:
        for v in e.verts:
            start_set.add(v)
    for e in frame2:
        for v in e.verts:
            end_set.add(v)

    find_shortest_paths(start_set, end_set, on_finish=on_finish)
    faces = bmesh.ops.edgenet_fill(bm, edges=edges)['faces']
    ensure_lookup(EDGES | FACES)

    doubled_frame_vertices = []

    is_frame = lambda f: all(e_map.get(e) in frame1 for e in f.edges) \
                      or all(e_map.get(e) in frame2 for e in f.edges)

    frame_faces = [f for f in faces if is_frame(f)]
    faces[:] = [f for f in faces if not is_frame(f)]

    if len(frame_faces) == 2:
        for face in frame_faces:
            doubled_frame_vertices.append(set(v for v in face.verts))
            bm.faces.remove(face)
    else:
        for frame in (frame1, frame2):
            frame_verts = set()
            for e in frame:
                frame_verts.update(v_map[v] for v in e.verts)
            doubled_frame_vertices.append(frame_verts)

    bt = bipartite_traingulation(bm, doubled_frame_vertices[0], doubled_frame_vertices[1], faces)
    edges.extend(bt['edges'])
    faces.extend(bt['faces'])

    e_map = {e: e_map[e] for e in (frame1 | frame2)}
    geom = {
        'faces': faces,
        'edges': edges,
        'verts': [e for e in dupl_dict['geom'] if type(e) is bmesh.types.BMVert]
    }
    dupl_map = {
        'verts': v_map,
        'edges': e_map
    }

    return geom, dupl_map


def connect_paths_into_cycle(bm, frame1, frame2):
    def path(v, end, prev_v):
        if end not in prev_v:
            raise Exception("Path not found")
        p = []
        vi = end
        while vi is not v:
            p.insert(0, vi)
            vi = prev_v[vi]
        p.insert(0, v)
        return p

    def find_paths(v, ends):
        paths = []
        start_v, prev_v = find_shortest_paths([v], ends)
        for ev in ends:
            paths.append(path(v, ev, prev_v))
        return paths

    f1s, f1e = frame1['ends']
    f2s, f2e = frame2['ends']

    paths1 = find_paths(f1s, [f2s, f2e])
    paths2 = find_paths(f1e, [f2s, f2e])

    def distance(step, e): 
        return (step.distance+e.calc_length() if is_proper_edge(e) else float('inf'))

    def calc_target_bound_direction(v1, v2): 
        end_edges = {frame1['edges'][0], frame2['edges'][0], frame1['edges'][-1], frame2['edges'][-1]}
        find_b_vertice = lambda v: next(e for e in v.link_edges if (e in end_edges)).other_vert(v)
        v1b, v2b = find_b_vertice(v1), find_b_vertice(v2)
        return co_middle(v1b.co, v2b.co) - co_middle(v1.co, v2.co)

    def find_boundary_path(v1, v2):
        target_bound_direction = calc_target_bound_direction(v1, v2)

        def is_proper_edge(e):
            if not e.is_boundary:
                return False
            bound_direction = edge_middle(e) - e.link_faces[0].calc_center_median()
            return bound_direction.dot(target_bound_direction) < 0.

        if all(v.is_boundary for v in (v1,v2)):
            _, prev_v = find_shortest_paths(
                [v1], [v2], 
                dist_func=distance
            )
            try:
                path_verts = path(v1, v2, prev_v)
                path_edges = []
                for next_i, v in enumerate(path_verts[:-1], 1):
                    nv = path_verts[next_i]
                    path_edges.append(next(e for e in v.link_edges if e.other_vert(v) is nv))
                return path_edges
            except Exception:
                pass
        return None

    def find_congruent_bound(v1, v2):
        target_bound_direction = calc_target_bound_direction(v1, v2)
        congruent_dir = (v2.co - v1.co).normalized()
        edge_dir = lambda e: (e.verts[0].co - e.verts[1].co).normalized()

        def is_proper_edge(e):
            if not (e.is_boundary and e.seam):
                return False
            bound_direction = edge_middle(e) - e.link_faces[0].calc_center_median()
            return bound_direction.dot(target_bound_direction) > 0. and \
                abs(1.0 - abs(congruent_dir * edge_dir(e))) < 0.001

        if all(v.is_boundary for v in (v1,v2)):
            _, prev_v = find_shortest_paths(
                [v1], [v2],
                dist_func=distance,
            )
            return v2 in prev_v
        return False

    def path_description(start, end):
        path = find_boundary_path(start, end)
        is_boundary = path is not None
        has_congruent = find_congruent_bound(start, end)
        if not is_boundary:
            path = bmesh.ops.connect_vert_pair(bm, verts=[start, end])['edges']
            if not path:
                raise Exception("Failed to connect vert pair")

        return {
            'edges': path,
            'is_boundary': is_boundary,
            'has_congruent': has_congruent,
            'ends': (start, end)
        }

    if not (set(paths1[0]) & set(paths2[1])):     # paths are not crossing
        return (
            path_description(f1s, f2s),
            path_description(f1e, f2e),
        )
    else:
        return (
            path_description(f1s, f2e),
            path_description(f1e, f2s),
        )


def ribbon_faces_seq(ribbon_geom, ribbon_cut=None):
    seq = []

    visited = set()

    if ribbon_cut:
        begin, end = ribbon_cut.verts
        rss = (end.co - begin.co).cross(begin.normal)  # rigt side specifier
        right_face, left_face = next(iter(f for f in ribbon_cut.link_faces if (rss.dot(f.calc_center_median() - begin.co) < 0))), \
                                next(iter(f for f in ribbon_cut.link_faces if (rss.dot(f.calc_center_median() - begin.co) > 0)))
        start_face = right_face
        visited.add(left_face)
    else:
        start_face = next(f for f in ribbon_geom['faces'] if len(tuple(e for e in f.edges if e.is_boundary))==2)

    f = start_face
    def next_face(f):
        for e in f.edges:
            for ef in e.link_faces:
                if ef not in visited:
                    return ef
    prev = None
    while f is not prev:
        visited.add(f)
        seq.append(f)
        f = next_face(f)

    if ribbon_cut:
        seq.append(left_face)

    return seq


def make_separating_cut(bm, edges, leave_faces=None):
    if not leave_faces:
        leave_faces = set()
    selected_link_edges = lambda v: (e for e in v.link_edges if e in edges)

    ends = []
    for e in edges:
        e.seam = True
        for v in e.verts:
            if len(tuple(selected_link_edges(v))) == 1:
                ends.append(v)
    if len(ends) != 2:
        raise Exception("Wrong selection (ends: %s)" % str(ends))

    v1, v2 = ends

    ne = next(selected_link_edges(v1))
    visited = set((ne, ))
    v = ne.other_vert(v1)

    new_verts = []

    side = 'right'
    if leave_faces:
        e1 = ne
        e2 = next(e for e in selected_link_edges(v) if e not in visited)
        sd = side_division(v, e1, e2)
        side_face = next(l.face for l in sd[side]['loops'] if l.face is not None)
        if side_face in leave_faces:
            side = 'left'

    while v is not v2:
        pe = ne
        ne = next(e for e in selected_link_edges(v) if e not in visited)
        visited.add(ne)

        sd = side_division(v, pe, ne)
        for l in sd[side]['loops']:
            if l.face is None:
                continue
            new_verts.append(bmesh.utils.loop_separate(l))
            ensure_lookup(VERTS | EDGES)

        v = ne.other_vert(v)

    bmesh.ops.remove_doubles(bm, verts=new_verts, dist=0.001)
    ensure_lookup(VERTS | EDGES)


def ribbonate_mesh(bm, frame1, frame2):
    pass # TODO


def ribbonate_mesh(bm, source_geom, frame1, frame2, bridge=None, bounds=None):
    uv_layer = bm.loops.layers.uv.new('__ribbon__')
    clean_uv_map(bm, uv_layer)

    predef_conn = tuple()
    if bounds:
        bound1, bound2 = bounds
        predef_conn = [path['ends'] for path in paths]

    ribbon_geom, dupl_map = generate_ribbon_geometry(bm, frame1, frame2, predef_conn)

    uv_layer = bm.loops.layers.uv.remove(uv_layer)
    bmesh.ops.delete(bm, geom=ribbon_geom['verts'], context=DEL_VERTS)


def ribbonate_mesh(bm, source_geom, frame1, frame2, bridge=None, bounds=None):
    uv_layer = bm.loops.layers.uv.new('__ribbon__')
    clean_uv_map(bm, uv_layer)

    if bounds:
        bound1, bound2 = (set(bound['edges']) for bound in bounds)

    uv_layer = bm.loops.layers.uv.remove(uv_layer)


class RibbonateMeshOperator(bpy.types.Operator):
    """
    Transformates segment for ribbons where folds are always
    connecting different ribs
    """
    bl_idname = "edit_mesh.ribbonate_mesh_operator"
    bl_label = "Ribbonate mesh"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.mode == 'EDIT_MESH'

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.object.data)
        bm.transform(context.active_object.matrix_world)
        bm.normal_update()

        # uv_layer = bm.loops.layers.uv[uv_name]
        # clean_uv_map(bm, uv_layer)

        source_geom = None
        frame1, frame2, bridge, ends = find_selected_cycles_with_bridge(bm)
        paths = tuple()
        bound1, bound2 = set(), set()

        # predef_conn = tuple()
        if frame1:
            source_geom = select_between_cycles(frame1, frame2, bridge)
            for e in bridge:
                e.seam = True
        else:
            frames_with_ends = find_selected_two_paths(bm)
            if not frames_with_ends[0]:
                raise WRONG_SELECTION
            frame1, frame2 = (set(fwe['edges']) for fwe in frames_with_ends)
            paths = connect_paths_into_cycle(bm, *frames_with_ends)
            bound1, bound2 = (set(path['edges']) for path in paths)
            # predef_conn = [path['ends'] for path in paths]

            source_geom = select_inside_cycle(frame1 | frame2 | bound1 | bound2)

        for e in selected_edges(bm):
            e.select_set(False)
            e.seam = True

        # ribbon_geom, dupl_map = generate_ribbon_geometry(bm, frame1, frame2, predef_conn)

        ribbon_cut = None
        if ends:
            ends_set = set(ends)
            for e in ribbon_geom['edges']:
                if set(dupl_map['verts'][v] for v in e.verts) == ends_set:
                    ribbon_cut = e;
                    e.seam = True

        for f in ribbon_geom['faces']:
            f.select_set(True)
        bpy.ops.uv.unwrap()
        for f in ribbon_geom['faces']:
            f.select_set(False)

        if bridge:
            setup_cut_uv({'edges': bridge, 'ends': ends}, source_geom, ribbon_geom, dupl_map['verts'], uv_layer)
        else:
            for path in paths:
                if len(path['edges']) < 2:
                    continue
                if path['is_boundary'] and not path['has_congruent']:
                    setup_boundary_path_uv(path, source_geom, ribbon_geom, dupl_map['verts'], uv_layer)
                else:
                    setup_straith_path_uv(path, source_geom, ribbon_geom, dupl_map['verts'], uv_layer)
                    if not path['is_boundary']:
                        boundary_faces = set()
                        for e in path['edges']:
                            boundary_faces.update(f for f in e.link_faces if f in source_geom['faces'])
                        make_separating_cut(bm, path['edges'], leave_faces=boundary_faces)

        fit_uv(source_geom, ribbon_geom, dupl_map['verts'], uv_layer)

        visited = set()
        unmapped_edges = set(ribbon_geom['edges'])
        for e, mapped_e in dupl_map['edges'].items():
            unmapped_edges.remove(mapped_e)

        edge_count = len(bm.edges)
        face_count = len(bm.faces)
        for e in unmapped_edges:
            v1, v2 = e.verts
            path, verts = connect_path_uv(bm, dupl_map['verts'][v1], dupl_map['verts'][v2], uv_layer, source_geom['faces'])
            ensure_lookup(ALL_MESH)
            source_geom['faces'].update(bm.faces[face_count:])
            face_count = len(bm.faces)

            for l, v in verts[1:-1]:
                v.co = v1.co + (v2.co - v1.co) * (1.-l)
                for bound in (bound1, bound2):
                    if any((e in bound) for e in v.link_edges):
                        bound.update(e for e in v.link_edges if e.is_boundary)
            edge_count = len(bm.edges)
            visited.update(v[1] for v in verts)
            if all((set(e.verts) != set(dupl_map['verts'][v] for v in p['ends']) for p in paths)):
                visited.update(path)

        rfs = ribbon_faces_seq(ribbon_geom, ribbon_cut)
        start_face = rfs[0]

        queues = {f: Queue() for f in rfs}
        if bridge:
            e1, e2 = ends
            rss = (e2.co - e1.co).cross(e1.normal)
            for e in bridge:
                for f in e.link_faces:
                    visited.add(f)
                    if rss.dot(f.calc_center_median() - e1.co) > 0:
                        queues[start_face].put(f)
        else:
            v = paths[0]['ends'][0]
            mv = dupl_map['verts'][v]
            bound = bound2
            for f in mv.link_faces:
                if f is start_face:
                    bound = bound1
                    break

            for e in bound:
                f = next(f for f in e.link_faces)
                uv_center = sum((l[uv_layer].uv for l in f.loops), Vector((0.,0.))) / len(f.loops)
                done = False
                for rf in rfs:
                    if intersect_point_tri(uv_center, *(l[uv_layer].uv for l in rf.loops)):
                        queues[rf].put(f)
                        done = True
                        break
                if not done:
                    queues[start_face].put(f)

        debug_data = {}
        for i, rf in enumerate(rfs, 1):
            base, bv = get_base(rf, uv_layer)
            fv0, fv1, fv2 = bv

            q = queues[rf]
            if i < len(rfs):
                next_q = queues[rfs[i]]
            else:
                next_q = None

            while not q.empty():
                f = q.get()
                for v in f.verts:
                    if v not in visited:
                        visited.add(v)
                        a, b = calculate_co_by_uv_base(v, uv_layer, base)
                        debug_data[v.index] = (a, b)
                        v.co = fv0.co + a * (fv1.co-fv0.co) + b * (fv2.co - fv0.co)
                for e in f.edges:
                    try:
                        cf = next(iter(ff for ff in e.link_faces if ff is not f))
                    except StopIteration:
                        continue
                    if cf in visited or cf not in source_geom['faces']:
                        continue
                    visited.add(cf)
                    if e in visited:
                        next_q.put(cf)
                    else:
                        q.put(cf)

        bm.transform(context.active_object.matrix_world.inverted())
        bmesh.update_edit_mesh(context.active_object.data)

        return {'FINISHED'}
