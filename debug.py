import bpy
import bmesh
import mathutils



def connect_path_uv(bm, v1, v2, uv_layer, threshold=0.0001):
    # TODO recalculate threshold?
    def edge_uvs(e, uv_layer):
        for l in e.link_loops:
            v = l.vert
            ov = e.other_vert(v)
            f = l.face
            ol = next(ll for ll in ov.link_loops if ll.face is f)
            if v is e.verts[0]:
                yield (l[uv_layer].uv, ol[uv_layer].uv)
            else:
                yield (ol[uv_layer].uv, l[uv_layer].uv)

    old_edges = [e for e in bm.edges]
    p1, p2 = tuple(v.link_loops[0][uv_layer].uv for v in (v1, v2))
    verts = {}
    for e in old_edges:
        for ev1, ev2 in edge_uvs(e, uv_layer):
            intersection = mathutils.geometry.intersect_line_line_2d(ev1, ev2, p1, p2)
            if intersection:
                if (ev2-intersection).length < threshold:
                    verts[e.verts[1]] = intersection
                elif (ev1-intersection).length < threshold:
                    verts[e.verts[0]] = intersection
                else:
                    ne, nv = bmesh.utils.edge_split(e, e.verts[0], (ev1-intersection).length / (ev1-ev2).length)
                    verts[nv] = intersection
            break

    sorted_verts = sorted(verts.keys(), key=lambda v: (p2 - verts[v]).length)
    for i in range(len(sorted_verts)-1):
        bmesh.ops.connect_verts(bm, verts=sorted_verts[i:i+2])


class ConnectPathUVOperator(bpy.types.Operator):
    """
    Transformates segment for ribbons where folds are always
    connecting different ribs
    """
    bl_idname = "edit_mesh.connect_path_uv"
    bl_label = "Vertex connect path UV"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.mode == 'EDIT_MESH'

    def execute(self, context):
        bm = bmesh.from_edit_mesh(context.object.data)
        uv_name = context.object.data.uv_textures[0].name
        uv_layer = bm.loops.layers.uv[uv_name]

        v1, v2 = tuple(v for v in bm.verts if v.select)
        connect_path_uv(bm, v1, v2, uv_layer)

        bmesh.update_edit_mesh(context.active_object.data)

        return {'FINISHED'}


def register():
    bpy.utils.register_class(ConnectPathUVOperator)


def unregister():
    bpy.utils.unregister_class(ConnectPathUVOperator)


if __name__ == "__main__":
    register()
